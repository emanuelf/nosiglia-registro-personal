package nosigliaregistropersonal;

import com.csvreader.CsvReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Time;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.vistas.JFrmSistema;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Emanuel
 */
public class Main {

    static JFrmSistema jFrmSistema ;
    
    public static void main(String[] args) {
        try {
            Service.createAndSetSessionFactory();
  //          lanzarThreadActualizacionMarcas();
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            jFrmSistema = new JFrmSistema();
            jFrmSistema.pack();
            jFrmSistema.setVisible(true);
            jFrmSistema.setLocationRelativeTo(null);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void lanzarThreadActualizacionMarcas() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String pathExportFile = getPathExportFile();
                exitOnInvalidPath(pathExportFile);
                Set<Marca> marcas = leerMarcas(pathExportFile);
                guardarMarcas(marcas);
                try {
                    Thread.sleep(5000);
                    run();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        thread.start();
    }

    private static void guardarMarcas(Set<Marca> marcas) {
        for (Marca marca : marcas){
            Marca.saveOrUpdate(marca.toMarca());
        }
    }
            
    private static Set<Marca> leerMarcas(String pathExportFile) {
        try {
            Set<Marca> marcas = new HashSet<>();
            CsvReader csvReader = new CsvReader(new FileReader(URLDecoder.decode(pathExportFile)), ';');
            try {
                while (csvReader.readRecord()) {
                    String stringDia = csvReader.get(Marca.COLUMNA_DIA);
                    String stringHora = csvReader.get(Marca.COLUMNA_HORA);
                    String stringAccesoReal = csvReader.get(Marca.COLUMNA_ACCESO_REAL);
                    String stringLegajo = csvReader.get(Marca.COLUMNA_LEGAJO);
                    String stringId = csvReader.get(Marca.COLUMNA_ID);
                    Marca marca = marcaWith(stringDia, stringHora, stringAccesoReal, stringLegajo, stringId);
                    if (marca != null){
                        marcas.add(marca);
                    }
                }
                return marcas;
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private static void exitOnInvalidPath(String pathExportFile) {
        if (pathExportFile == null || pathExportFile.isEmpty()) {
            MessageUtils.showError(null, "No se puede leer el archivo de configuracion.");
            System.exit(0);
        }
    }

    private static String getPathExportFile() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(URLDecoder.decode(System.getProperty("user.dir") + "/config.properties")));
            return properties.getProperty("path_archivo_marcas");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private static Marca marcaWith(String stringDia, String stringHora, String stringAccesoReal, String stringLegajo, String stringId) {
        if (!stringAccesoReal.equals("T")){
            return null;
        }
        Marca marca = new Marca();
        int anio = Integer.parseInt(stringDia.substring(0, 4));
        int mes = Integer.parseInt(stringDia.substring(4, 6));
        int dia = Integer.parseInt(stringDia.substring(6));
        int hora = Integer.parseInt(stringHora.substring(0,2));
        int minutos = Integer.parseInt(stringHora.substring(2,4));
        int segundos = Integer.parseInt(stringHora.substring(4));
        marca.setHora(new Time(hora, minutos, segundos));
        marca.setFecha(new Date(anio - 1900, mes - 1, dia));
        marca.setLegajo(Integer.parseInt(stringLegajo));
        marca.setIdMarca(stringId);
        return marca;
    }

    private static class Marca {
        
        public static final int COLUMNA_DIA = 0, 
                COLUMNA_HORA = 1, 
                COLUMNA_LEGAJO =2, 
                COLUMNA_ACCESO_REAL = 4, 
                COLUMNA_ID = 5;

    static void saveOrUpdate(final nosigliaregistropersonal.modelo.Marca toMarca) {
        Service.executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                Object uniqueResult = session.createCriteria(nosigliaregistropersonal.modelo.Marca.class).add(Restrictions.eq("uid", toMarca.getUid())).uniqueResult();
                if (uniqueResult == null){
                    Service.save(toMarca);
                }
                return null;
            }
        });
    }
        
        private Date fecha;
        private long legajo;
        private String idMarca;
        private Time hora;

        public nosigliaregistropersonal.modelo.Marca toMarca(){
            final nosigliaregistropersonal.modelo.Marca marca = new nosigliaregistropersonal.modelo.Marca();
            marca.setEmpleado(Empleado.findByLegajo(legajo));
            GregorianCalendar fechaCalendar = new GregorianCalendar();
            fechaCalendar.setTime(this.fecha);
            marca.setFecha(fechaCalendar);
            marca.setHora(hora);
            marca.setUid(idMarca);
            return marca;
        }
        
        public Date getFecha() {
            return fecha;
        }

        public void setFecha(Date fecha) {
            this.fecha = fecha;
        }

        public long getLegajo() {
            return legajo;
        }

        public void setLegajo(long legajo) {
            this.legajo = legajo;
        }

        public String getIdMarca() {
            return idMarca;
        }

        public void setIdMarca(String idMarca) {
            this.idMarca = idMarca;
        }
        
        public Time getHora(){
            return hora;
        }
        
        public void setHora(Time time){
            hora = time;
        }
    }
}
