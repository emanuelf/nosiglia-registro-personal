package nosigliaregistropersonal.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import nosigliaregistropersonal.Main;
import nosigliaregistropersonal.modelo.Adelanto;
import nosigliaregistropersonal.modelo.Ajuste;
import nosigliaregistropersonal.modelo.DescuentoAdelanto;
import nosigliaregistropersonal.modelo.Liquidacion;
import nosigliaregistropersonal.modelo.OrdenDePago;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author #emas
 */
public class Impresor {
   
    public static void imprimir(Adelanto adelanto){
        try {
            File forPrint = new File(pathAdelantoForPrint());
            String content = FileUtils.readFileToString(new File(pathAdelantoTemplate()));
            content = content.replace(
                    "{{NRO_ANTICIPO}}", 
                    adelanto.getId().toString()).replace(
                    "{{FECHA}}", 
                    OutputUtils.formatShort(adelanto.getFecha().getTime())).replace(
                    "{{EMPLEADO}}", 
                    adelanto.getEmpleado().getNombreApellido()).replace(
                    "{{MONTO}}", 
                    OutputUtils.formatCurrency(adelanto.getMonto())).replace(
                    "{{OBSERVACIONES}}", 
                    adelanto.getObservaciones());
                    
            FileUtils.writeStringToFile(forPrint, content);
            print(forPrint);
        } catch (IOException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void imprimir(OrdenDePago ordenDePago) {
        try {
            File forPrint = new File(pathOrdenDePagoForPrint());
            String content = FileUtils.readFileToString(new File(pathOrdenDePagoTemplate()));
            content = content.replace(
                        "{{FECHA}}", 
                        OutputUtils.formatShort(ordenDePago.getFecha().getTime())).replace(
                        "{{EMPLEADO}}", 
                        ordenDePago.getEmpleado().getNombreApellido()).replace(
                        "{{NRO_ORDEN_DE_PAGO}}", 
                        ordenDePago.getId().toString()).replace(
                        "{{ADELANTADO}}", 
                        OutputUtils.formatCurrency(ordenDePago.totalAdelantos())).replace(
                        "{{A_PAGAR}}", 
                        OutputUtils.formatCurrency(ordenDePago.getTotalAPagar())).replace(
                        "{{TOTAL_LIQUIDACIONES}}", 
                        OutputUtils.formatCurrency(ordenDePago.getTotalLiquidaciones())).replace(
                        "{{TOTAL_AJUSTES}}", 
                        OutputUtils.formatCurrency(ordenDePago.totalAjustes())).replace(
                        "{{JUBILACION}}", OutputUtils.formatCurrency(ordenDePago.getJubilacion())).replace
                        ("{{RETENCIONES}}", OutputUtils.formatCurrency(ordenDePago.getRetenciones()));
            
            int i = 1;
            for (Liquidacion liquidacion : ordenDePago.getLiquidaciones()) {
                content = modifyContent(content, liquidacion, i);
                StringBuilder sb = new StringBuilder(content);
                i++;
                if (i > 3) {
                    break;
                }
            }
            i = 1;
            for (DescuentoAdelanto descuentoAdelanto : ordenDePago.getDescuentosAdelantos()) {
                content = modifyContent(content, descuentoAdelanto, i);
                i++;
                if (i > 3) {
                    break;
                }
            }
            i = 1;
            for (Ajuste ajuste : ordenDePago.getAjustes()) {
                content = modifyContent(content, ajuste, i);
                i++;
                if (i > 3) {
                    break;
                }
            }
            content = deleteUnusedParams(content, 
                    "{{LIQUIDACION1}}", "{{LIQUIDACION2}}", "{{LIQUIDACION3}}",
                    "{{ADELANTO1}}", "{{ADELANTO2}}", "{{ADELANTO3}}", 
                    "{{AJUSTE1}}", "{{AJUSTE2}}", "{{AJUSTE3}}");
            
            FileUtils.writeStringToFile(forPrint, content);
            
            print(forPrint);

        } catch (IOException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static String modifyContent(String content, DescuentoAdelanto descuentoAdelanto, int nroAdelanto){
        String retorno = content;
        switch (nroAdelanto) {
            case 1:
                retorno = modifyContent(
                        content,
                        descuentoAdelanto, "{{ADELANTO1}}");
                break;
            case 2:
                retorno = modifyContent(
                        content,
                        descuentoAdelanto, "{{ADELANTO2}}");
                break;
            case 3:
                retorno = modifyContent(
                        content,
                        descuentoAdelanto, "{{ADELANTO3}}");
                break;
            default:
                throw new IllegalArgumentException("");
        }
        return retorno;
    }
    
    private static String modifyContent(String content, Ajuste ajuste, int nroAjuste) {
        String retorno = content;
        switch (nroAjuste) {
            case 1:
                retorno = modifyContent(
                        content,
                        ajuste, "{{AJUSTE1}}");
                break;
            case 2:
                retorno = modifyContent(
                        content,
                        ajuste, "{{AJUSTE2}}");
                break;
            case 3:
                retorno = modifyContent(
                        content,
                        ajuste, "{{AJUSTE3}}");
                break;
            default:
                throw new IllegalArgumentException("");
        }
        return retorno;
    }
    
    private static String modifyContent(String content, Liquidacion liquidacion, int nroLiquidacion){
        String retorno = content;
        switch (nroLiquidacion) {
            case 1:
                retorno = modifyContent(
                            content,
                            liquidacion, "{{LIQUIDACION1}}");
                break;
            case 2:
                retorno = modifyContent(
                            content,
                            liquidacion, "{{LIQUIDACION2}}");
                break;
            case 3:
                retorno = modifyContent(
                            content,
                            liquidacion, "{{LIQUIDACION3}}");
                break;
            default:
                throw new IllegalArgumentException("");
        }
        return retorno;
    }

    private static String modifyContent(String content, Liquidacion liquidacion, String clave) {
        String periodoTrabajado = liquidacion.getPeriodoTrabajo().periodoToString();
        String horasTrabajadas = OutputUtils.formatHorasMinutos(liquidacion.minutosTrabajados());
        String precioHora = OutputUtils.formatCurrency(liquidacion.getPrecioHora());
        String total = OutputUtils.formatCurrency(liquidacion.totalLiquidacion()); 
        String renglon = periodoTrabajado   + 
                "        "                  +
                horasTrabajadas             + 
                "             "             + 
                precioHora                  + 
                "                "          +
                total;
        return content.replace(
                clave, renglon);
    }
    
    private static String modifyContent(String content, DescuentoAdelanto liquidacion, String claveLiquidacion) {
        String renglon = "";
        renglon =  OutputUtils.formatShort(liquidacion.getAdelanto().getFecha().getTime())  +
                "                                               "                           +
                OutputUtils.formatCurrency(liquidacion.getMonto());
        return content.replace(claveLiquidacion, renglon);
    }
    
    private static String modifyContent(String content, Ajuste ajuste, String claveLiquidacion) {
        String renglon = "";
        renglon = ajuste.getObservaciones()
                + "                                                    "
                + OutputUtils.formatCurrency(ajuste.getMonto());
        return content.replace(claveLiquidacion, renglon);
    }
    
    private static void print(File forPrint) {
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();
        FileInputStream in = null;
        try {
            in = new FileInputStream(forPrint);

            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            pras.add(new Copies(1));

            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            Doc doc = new SimpleDoc(in, flavor, null);

            DocPrintJob job = service.createPrintJob();
            job.print(doc, pras);
            in.close();

            // send FF to eject the page
            /*InputStream ff = new ByteArrayInputStream("\f".getBytes());
            Doc docff = new SimpleDoc(ff, flavor, null);
            DocPrintJob jobff = service.createPrintJob();
            jobff.print(docff, null);*/

        } catch (PrintException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }

    private static String deleteUnusedParams(String content, String... aReemplazar) {
        String retorno = content;
        for (String string : aReemplazar) {
            retorno = retorno.replace(string, "");
        }
        return retorno;
    }
    
    private static String getProperty(String property){
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(URLDecoder.decode(System.getProperty("user.dir") + "/config.properties")));
            return properties.getProperty(property);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    private static String pathAdelantoForPrint() {
        return getProperty("adelanto_for_print");
    }
    
    private static String pathAdelantoTemplate(){
        return getProperty("adelanto_template");
    }
    
    private static String pathOrdenDePagoForPrint(){
        return getProperty("orden_de_pago_for_print");
    }
    
    private static String pathOrdenDePagoTemplate(){
        return getProperty("orden_de_pago_template");
    }

    /*
     * Luego de probar la impresion en el cliente, ver si sigue siendo innecesario
     * 
    private static void probar(String fileName){
        try {
            FileInputStream textStream;
            textStream = new FileInputStream(fileName);
            
            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            Doc mydoc = new SimpleDoc(textStream, flavor, null);
            PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
            
            
            //print using default
            DocPrintJob job = defaultService.createPrintJob();
            
            PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
            aset.add(new Copies(1));           
            
            job.print(mydoc, aset);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PrintException ex) {
            Logger.getLogger(Impresor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
}