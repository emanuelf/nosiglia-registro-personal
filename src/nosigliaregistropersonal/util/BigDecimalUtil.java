package nosigliaregistropersonal.util;

import java.math.BigDecimal;

public class BigDecimalUtil {

    public static final BigDecimal CIEN = new BigDecimal(100);
    public static final BigDecimal IVA_GENERAL = new BigDecimal(".21");
    public static final BigDecimal IVA_DIFERENCIAL = new BigDecimal(".105");
    public static final BigDecimal IVA_DIFERENCIAL_27 = new BigDecimal(".27");
    public static final BigDecimal MULTIPLICADOR_IVA_GENERAL = new BigDecimal("1.21");
    public static final BigDecimal MULTIPLICADOR_IVA_DIFERENCIAL = new BigDecimal("1.105");
    public static final BigDecimal MULTIPLICADOR_IVA_DIFERENCIAL_27 = new BigDecimal("1.27");

    private BigDecimalUtil() {
        throw new UnsupportedOperationException("Non instantiable");
    }

    public static BigDecimal toBigDecimal(int entero) {
        return new BigDecimal(entero);
    }

    public static boolean menorQueCero(BigDecimal valor) {
        return BigDecimal.ZERO.compareTo(valor) != 0 && BigDecimal.ZERO.min(valor) == valor;
    }

    public static boolean mayorQueUno(BigDecimal valor) {
        return BigDecimal.ONE.compareTo(valor) != 0 && BigDecimal.ONE.max(valor) == valor;
    }

    public static BigDecimal toMultiplier(BigDecimal porcentaje) {
        return BigDecimal.ONE.add(porcentaje);
    }

    public static void checkPorcentaje(BigDecimal porcentaje) {
        if (BigDecimalUtil.menorQueCero(porcentaje) || BigDecimalUtil.mayorQueUno(porcentaje)) {
            throw new IllegalArgumentException("El porcentaje debe estar entre 0% y 100%");
        }
    }

    public static boolean mayorQueCero(BigDecimal valor) {
        return BigDecimal.ZERO.compareTo(valor) != 0 && BigDecimal.ZERO.max(valor) == valor;
    }

    public static boolean mayorOIgualQueCero(BigDecimal valor) {
        return mayorQueCero(valor) || BigDecimal.ZERO.compareTo(valor) == 0;
    }

    public static boolean menorOIgualQueCero(BigDecimal valor) {
        return menorQueCero(valor) || BigDecimal.ZERO.compareTo(valor) == 0;
    }

    public static boolean igualACero(BigDecimal numero) {
        return BigDecimal.ZERO.compareTo(numero) == 0;
    }
}