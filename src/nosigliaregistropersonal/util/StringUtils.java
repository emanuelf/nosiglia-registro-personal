package nosigliaregistropersonal.util;

import java.util.Collection;
import java.util.Objects;


public class StringUtils {

    private StringUtils() {
        throw new UnsupportedOperationException("non instantiable.");
    }

    public static String join(Collection aJuntar, String separator){
        String retorno = "";
        for (Object object : aJuntar) {
            if (retorno.isEmpty()){
                retorno = object.toString();
            } else {
                retorno = retorno + ", " + object.toString();            
            }
        }
        return retorno;
    }
    
    public static String trimLastChar(String string) {
        return string.substring(0, string.length() - 1);
    }

    public static String removeUnnecesarySpacings(String string) {
        if (string == null) {
            return "";
        }
        String aux = string;
        while (aux.contains("  ")) {
            aux = aux.replaceAll("  ", " ");
        }
        return aux.trim();
    }

    public static String capitalizeFirstLetter(String palabra) {
        if (palabra.isEmpty()) {
            return palabra;
        } else {
            String firstLetter = palabra.substring(0, 1).toUpperCase();
            String remainingLetters = palabra.substring(1);
            return firstLetter.concat(remainingLetters);
        }
    }

    public static String normalizeClassName(String className) {
        char[] chars = className.toCharArray();
        String palabraEspaciada = "";
        for (char currentChar : chars) {
            palabraEspaciada += Character.isLowerCase(currentChar) ? currentChar : " " + currentChar;
        }
        return palabraEspaciada.trim();
    }

    public static String toString(Object object) {
        return Objects.toString(object);
    }

    public static String toCsvLine(String[] data) {
        String line = "";
        for (String string : data) {
            line = line + string + ",";
        }
        return line.substring(0, line.length() - 1);
    }

    public static String fillWithSpaces(String string, int size) {
        return fillWithChar(string, size, string.length(), ' ');
    }

    public static String fillWithChar(String string, int size, char fillChar) {
        return fillWithChar(string, size, string.length(), fillChar);
    }

    public static String fillWithChar(String string, int size, int position, char fillChar) {
        int fillCount = size - string.length();
        String fillString = "";
        for (int i = 0; i < fillCount; i++) {
            fillString += fillChar;
        }
        return string.substring(0, position) + fillString + string.substring(position, string.length());
    }

    /**
     * @param string
     * @param size
     * @return a string of the specified size, blanks filled with spaces on the right side.
     */
    public static String fixedSize(String string, int size) {
        if (string.length() > size) {
            return string.substring(0, size);
        } else {
            return fillWithSpaces(string, size);
        }
    }
}
