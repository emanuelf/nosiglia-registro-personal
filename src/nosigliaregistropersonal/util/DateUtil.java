package nosigliaregistropersonal.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    
    public static boolean isSameDay(Date date1, Date date2){
        return date1.getDate() == date2.getDate() && date1.getMonth() == date2.getMonth() && date1.getYear() == date2.getYear();
    }

    public static Calendar toCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }
}