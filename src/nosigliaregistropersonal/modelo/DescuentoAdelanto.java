package nosigliaregistropersonal.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author #emas
 */

@Entity
@Table(name = "descuentos_de_adelantos")
public class DescuentoAdelanto {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private OrdenDePago ordenDePago;
    @ManyToOne
    private Adelanto adelanto;
    private double monto;

    public DescuentoAdelanto() {
    }

    public DescuentoAdelanto(Adelanto adelanto, OrdenDePago ordenDePago1) {
        this.adelanto = adelanto;
        this.monto = adelanto.disponible();
        this.ordenDePago = ordenDePago1;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrdenDePago getOrdenDePago() {
        return ordenDePago;
    }

    public void setOrdenDePago(OrdenDePago ordenDePago) {
        this.ordenDePago = ordenDePago;
    }

    public Adelanto getAdelanto() {
        return adelanto;
    }

    public void setAdelanto(Adelanto adelanto) {
        this.adelanto = adelanto;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}
