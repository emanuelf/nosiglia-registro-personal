package nosigliaregistropersonal.modelo;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author #emas
 */
@Entity
@Table(name = "adelantos")
public class Adelanto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    @ManyToOne
    private Empleado empleado;
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinTable(name = "adelantos_pagos")
    private Set<Pago> pagos;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fecha;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "adelanto")
    private Set<DescuentoAdelanto> descuentosAdelanto;
    private String observaciones;
    private double monto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Set<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(Set<Pago> pagos) {
        this.pagos = pagos;
    }

    public double disponible() {
        double ocupado = 0;
        for (DescuentoAdelanto pago : getDescuentosAdelanto()) {
            ocupado += pago.getMonto();
        }
        return getMonto() - ocupado;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public Set<DescuentoAdelanto> getDescuentosAdelanto() {
        return descuentosAdelanto;
    }

    public void setDescuentosAdelanto(Set<DescuentoAdelanto> descuentosAdelanto) {
        this.descuentosAdelanto = descuentosAdelanto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public static Collection<Adelanto> findByNroAdelanto(final long id) {
        return Service.executeQuery(new QueryRunner<List<Adelanto>>() {
            @Override
            public List<Adelanto> runQueryIn(Session session) {
                return session.createCriteria(Adelanto.class).add(Restrictions.eq("id", id)).list();
            }
        });
    }

    public static Collection<Adelanto> findByEmpleado(final String textBusqueda) {
        return Service.executeQuery(new QueryRunner<List<Adelanto>>() {
            @Override
            public List<Adelanto> runQueryIn(Session session) {
                return session.
                        createQuery("from Adelanto where lower(empleado.nombreApellido) like :nombre")
                        .setString("nombre", "%" + textBusqueda.toLowerCase() + "%").list();
            }
        });
    }

    public static Collection<Adelanto> findAdelantosDisponibles(final Empleado empleado) {
        return Service.executeQuery(new QueryRunner<Collection<Adelanto>>() {
            @Override
            public Collection<Adelanto> runQueryIn(Session session) {
                return session.createQuery("FROM Adelanto a WHERE a.monto - "
                        + "                     (SELECT COALESCE(sum(monto), 0) FROM DescuentoAdelanto d WHERE a.id = d.adelanto.id OR d.adelanto IS NULL) "
                        + "                     > 0 AND"
                        + "                      a.empleado = :empleado").
                        setEntity("empleado", empleado).
                        setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                        list();
            }
        });
    }

    public double totalDescontado() {
        double retorno = 0;
        for (DescuentoAdelanto descuentoAdelanto : descuentosAdelanto) {
            retorno += descuentoAdelanto.getMonto();
        }
        return retorno;
    }

    public static void guardar(final Adelanto element) {
        Service.executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                element.calcularMonto();
                session.saveOrUpdate(element);
                return null;
            }
        });
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adelanto other = (Adelanto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    private void calcularMonto() {
        monto = 0;
        for (Pago pago : getPagos()) {
            monto += pago.getMonto();
        }
    }
}