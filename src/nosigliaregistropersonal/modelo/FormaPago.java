package nosigliaregistropersonal.modelo;

import java.util.List;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

/**
 *
 * @author #emas
 */
@Entity
@Table(name = "formas_pago")
public class FormaPago {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private Integer codigo;
    @Column(unique = true)
    private String nombre;

    public FormaPago() {
    }

    
    public FormaPago(int nextCodigo) {
        this.codigo = nextCodigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    
    public static List<FormaPago> all(){
        return Service.executeQuery(new QueryRunner<List<FormaPago>>() {

            @Override
            public List<FormaPago> runQueryIn(Session session) {
                return session.createCriteria(FormaPago.class).addOrder(Order.asc("codigo")).list();
            }
        });
    }

    public void guardar() {
        Service.saveOrUpdate(this);
    }
    
    public static int nextCodigo() {
        return Service.executeQuery(new QueryRunner<Integer>() {
            @Override
            public Integer runQueryIn(Session session) {
                Object uniqueResult = session.createQuery("select max(codigo) from FormaPago").uniqueResult();
                if (uniqueResult == null){
                    return 1;
                } else {
                    return (Integer) uniqueResult + 1;
                }
            }
        });
    }

    @Override
    public String toString() {
        return nombre;
    }
    
    public static FormaPago first() {
        return Service.executeQuery(new QueryRunner<FormaPago>() {

            @Override
            public FormaPago runQueryIn(Session session) {
                return (FormaPago) session.createCriteria(FormaPago.class).addOrder(Order.asc("codigo")).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }
}
