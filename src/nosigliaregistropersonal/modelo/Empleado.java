package nosigliaregistropersonal.modelo;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.metamodel.ValidationException;

/**
 *
 * @author Emanuel
 */
@Entity
@Table(name = "empleados")
public class Empleado {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private Long legajo;
    @Column(nullable = false)
    private String nombreApellido;
    @OneToMany(mappedBy = "empleado")
    private Set<Marca> marcas;
    @Column(columnDefinition = "double precision default 0")
    private double precioHora;
    @Column(columnDefinition = "double precision default 0")
    private double jubilacion;
    @Column(columnDefinition = "double precision default 0")
    private double retenciones;
    @OneToMany(mappedBy = "empleado")
    private List<OrdenDePago> ordenesDePagos;
    @OneToMany(mappedBy = "empleado")
    private Set<Adelanto> adelantos;
    @OneToMany(mappedBy = "empleado")
    private List<Liquidacion> liquidaciones;

    public Set<Adelanto> getAdelantos() {
        return adelantos;
    }

    public void setAdelantos(Set<Adelanto> adelantos) {
        this.adelantos = adelantos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getLegajo() {
        return legajo;
    }

    public void setLegajo(Long legajo) {
        this.legajo = legajo;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public Set<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(Set<Marca> marcas) {
        this.marcas = marcas;
    }

    public double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(double precioHora) {
        this.precioHora = precioHora;
    }

    public double getJubilacion() {
        return jubilacion;
    }

    public void setJubilacion(double jubilacion) {
        this.jubilacion = jubilacion;
    }

    public double getRetenciones() {
        return retenciones;
    }

    public void setRetenciones(double retenciones) {
        this.retenciones = retenciones;
    }

    public void save() {
        if (isValid()) {
            Service.saveOrUpdate(this);
        } else {
            throw new ValidationException("Datos no validos");
        }
    }

    private boolean isValid() {
        return legajo != 0 && nombreApellido != null && !nombreApellido.isEmpty() && precioHora > 0;
    }

    public void delete() {
        Service.delete(this);
    }

    public void update() {
        Service.update(this);
    }

    public static List<Empleado> findAll() {
        return Service.executeQuery(new QueryRunner<List<Empleado>>() {
            @Override
            @SuppressWarnings("unchecked")
            public List<Empleado> runQueryIn(Session session) {
                Criteria criteria = session.createCriteria(Empleado.class);
                criteria = criteria.addOrder(Order.asc("nombreApellido"));
                criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                return criteria.list();
            }
        });
    }

    public static Empleado findByLegajo(final long legajo) {
        return Service.executeQuery(new QueryRunner<Empleado>() {
            @Override
            public Empleado runQueryIn(Session session) {
                return (Empleado) session.createCriteria(Empleado.class).add(Restrictions.eq("legajo", legajo)).uniqueResult();
            }
        });
    }

    public static List<Empleado> findNotIIn(final Collection<Empleado> empleados) {
        return Service.executeQuery(new QueryRunner<List<Empleado>>() {
            @Override
            public List<Empleado> runQueryIn(Session session) {
                if (empleados == null || empleados.isEmpty()) {
                    return Collections.emptyList();
                }
                Set<Long> legajos = new HashSet<>();
                for (Empleado empleado : empleados) {
                    legajos.add(empleado.getLegajo());
                }
                return session.createCriteria(Empleado.class).add(
                        Restrictions.not(Restrictions.in("legajo", legajos))).
                        setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                        list();
            }
        });
    }

    public static Collection<Empleado> findByNombreYApellido(final String textBusqueda) {
        return Service.executeQuery(new QueryRunner<Collection<Empleado>>() {
            @Override
            public Collection<Empleado> runQueryIn(Session session) {
                return session.createQuery("from Empleado where lower(nombreApellido) like :busqueda").
                        setString("busqueda", "%" + textBusqueda.toLowerCase() + "%").list();
            }
        });
    }

    @Override
    public String toString() {
        return nombreApellido;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.legajo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.legajo, other.legajo)) {
            return false;
        }
        return true;
    }
}
