package nosigliaregistropersonal.modelo;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Session;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author #emas
 */
@Entity
@Table(name = "ordenes_de_pago")
public class OrdenDePago {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fecha;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ordenDePago")
    private Set<Liquidacion> liquidaciones = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "ordenDePago")
    private Set<DescuentoAdelanto> descuentosAdelantos = new HashSet<>();
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "ordenes_de_pago_pagos")
    private Set<Pago> pagos = new HashSet<>();
    @ManyToOne
    @Fetch(FetchMode.SELECT)
    private Empleado empleado;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Ajuste> ajustes = new HashSet<>();
    private double jubilacion;
    private double retenciones;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(Collection<Pago> pagos) {
        this.pagos.clear();
        this.pagos.addAll(pagos);
    }

    public Set<DescuentoAdelanto> getDescuentosAdelantos() {
        return descuentosAdelantos;
    }

    public void setDescuentosAdelantos(Collection<DescuentoAdelanto> descuentosAdelantos) {
        this.descuentosAdelantos.clear();
        this.descuentosAdelantos.addAll(descuentosAdelantos);
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public Set<Liquidacion> getLiquidaciones() {
        return liquidaciones;
    }

    public void setLiquidaciones(Collection<Liquidacion> liquidaciones) {
        this.liquidaciones.clear();
        this.liquidaciones.addAll(liquidaciones);
    }

    public Set<Ajuste> getAjustes() {
        return ajustes;
    }

    public void setAjustes(Collection<Ajuste> ajustes) {
        this.ajustes.clear();
        this.ajustes.addAll(ajustes);
    }

    
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    
    
    public double totalAdelantos() {
        double retorno = 0;
        for (DescuentoAdelanto adelanto : getDescuentosAdelantos()) {
            retorno += adelanto.getMonto();
        }
        return retorno;
    }

    public double getTotalAPagar() {
        double total = 0;
        for (Pago pago : getPagos()) {
            total += pago.getMonto();
        }
        for (Ajuste ajuste : getAjustes()) {
            total += ajuste.getMonto();
        }
        return total + jubilacion - retenciones;
    }
    
    public static Collection<OrdenDePago> findById(final Long id) {
        return Service.executeQuery(new QueryRunner<Collection<OrdenDePago>>() {
            @Override
            public Collection<OrdenDePago> runQueryIn(Session session) {
                return session.createCriteria(OrdenDePago.class).add(Restrictions.eq("id", id)).list();
            }
        });
    }

    public static Collection<OrdenDePago> findByApyNomEmpleado(final String textBusqueda) {
        return Service.executeQuery(new QueryRunner<Collection<OrdenDePago>>() {
            @Override
            public Collection<OrdenDePago> runQueryIn(Session session) {
                return session.createQuery("from OrdenDePago where lower(empleado.nombreApellido) like(:text)").
                        setString("text", "%" + textBusqueda.toLowerCase()+ "%").
                        list();

            }
        });
    }
    
    public static void guardar(final OrdenDePago element) {
        Service.executeQuery(new QueryRunner<Void>() {
            @Override
            public Void runQueryIn(Session session) {
                for (Liquidacion liquidacion : element.liquidaciones) {
                    liquidacion.setOrdenDePago(element);
                    session.update(liquidacion);
                }
                session.saveOrUpdate(element);
                return null;
            }
        });
    }

    public BigDecimal getTotalLiquidaciones() {
        double retorno = 0;
        for (Liquidacion liquidacion : getLiquidaciones()) {
            retorno += liquidacion.totalLiquidacion();
        }
        return new BigDecimal(retorno);
    }

    public BigDecimal totalAjustes() {
        double retorno = 0;
        for (Ajuste ajuste : getAjustes()) {
            retorno += ajuste.getMonto();
        }
        return new BigDecimal(retorno);
    }

    public double getJubilacion() {
        return jubilacion;
    }

    public void setJubilacion(double jubilacion) {
        this.jubilacion = jubilacion;
    }

    public double getRetenciones() {
        return retenciones;
    }

    public void setRetenciones(double retenciones) {
        this.retenciones = retenciones;
    }
    
    
}