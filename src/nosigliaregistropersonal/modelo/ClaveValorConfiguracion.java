package nosigliaregistropersonal.modelo;

import java.sql.Time;
import javax.persistence.Id;
import javax.persistence.Table;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author #emas
 */
@Table(name = "valores_configuracion")
public class ClaveValorConfiguracion {

    @Id
    private String clave;
    private String valor;

    public static final String INGRESO_PRIMER_TURNO = "INGRESO_PRIMER_TURNO";
    public static final String INGRESO_SEGUNDO_TURNO = "INGRESO_SEGUNDO_TURNO";
    public static final String EGRESO_PRIMER_TURNO = "EGRESO_PRIMER_TURNO";
    public static final String EGRESO_SEGUNDO_TURNO = "EGRESO_SEGUNDO_TURNO";
    public static final String MINUTOS_ANTES_INGRESO_IGNORAR = "MINUTOS_ANTES_EGRESO_IGNORAR";
    public static final String MINUTOS_ANTES_EGRESO_IGNORAR = "MINUTOS_ANTES_EGRESO_IGNORAR";
    public static final String MINUTOS_DESCANSO_POR_DIA = "INGRESOS";
    public static final String HORAS_NORMALES_POR_QUINCENA = "HORAS_NORMALES_POR_QUINCENA";
    public static final String PORCENTAJE_EXTRA_POR_HORA_EXTRA = "PORCENTAJE_EXTRA_POR_HORA_EXTRA";

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private Time timeFor(String CLAVE){
        String ingresoPrimerTurnoString = findValor(CLAVE);
        final int INDICE_HORA = 0, INDICE_MINUTO = 1;
        String[] horaMinuto = ingresoPrimerTurnoString.split(":");
        return new Time(Integer.parseInt(horaMinuto[INDICE_HORA]), Integer.parseInt(horaMinuto[INDICE_MINUTO]), 0);
    }
    
    public Time ingresoPrimerTurno(){
        return timeFor(INGRESO_PRIMER_TURNO);
    }
    
    public Time ingresoSegundoTurno(){
        return timeFor(INGRESO_SEGUNDO_TURNO);
    }
    
    public Time egresoPrimerTurno(){
        return timeFor(EGRESO_PRIMER_TURNO);
    }
    
    public Time egresoSegundoTurno() {
        return timeFor(EGRESO_SEGUNDO_TURNO);
    }
    
    public String findValor(final String CLAVE){
        return Service.executeQuery(new QueryRunner<String>() {
            @Override
            public String runQueryIn(Session session) {
                return (String) session.
                        createCriteria(ClaveValorConfiguracion.class).
                        add(Restrictions.eq("clave", CLAVE)).
                        uniqueResult();
            }
        });
    } 
}