package nosigliaregistropersonal.modelo.helpers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Time;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import nosigliaregistropersonal.modelo.Marca;

public class MarcaHelper {

    public static boolean isMarcaFueraDeHorario(Marca marca) {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(URLDecoder.decode(System.getProperty("user.dir") + "/config.properties")));
            Time primerIngreso = primerIngreso(properties);
            Time segundoIngreso = segundoIngreso(properties);

            long diferenciaPrimerIngreso = marca.getHora().getTime() - primerIngreso.getTime();
            long diferenciaSegundoIngreso = marca.getHora().getTime() - segundoIngreso.getTime();

            long diferenciaPrimerIngresoEnMinutos = (diferenciaPrimerIngreso / 1000) / 60;
            long diferenciaSegundoIngresoEnMinutos = (diferenciaSegundoIngreso / 1000) / 60;

            if (diferenciaPrimerIngresoEnMinutos == 0 || diferenciaSegundoIngresoEnMinutos == 0) {
                return false;
            }
            if (diferenciaPrimerIngresoEnMinutos > 0) {
                if (-diferenciaSegundoIngresoEnMinutos > diferenciaPrimerIngresoEnMinutos) {
                    return true;
                }
            }
            if (diferenciaSegundoIngresoEnMinutos > 0) {
                if (diferenciaPrimerIngresoEnMinutos > diferenciaSegundoIngresoEnMinutos) {
                    return true;
                }
            }
            return false;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MarcaHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MarcaHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    private static Time primerIngreso(Properties properties) {
        String stringPrimerIngreso = properties.getProperty("ingresos").split(";")[0];
        Integer hora = Integer.parseInt(stringPrimerIngreso.split(":")[0]);
        Integer minuto = Integer.parseInt(stringPrimerIngreso.split(":")[1]);
        return new Time(hora, minuto, 0);
    }

    private static Time segundoIngreso(Properties properties) {
        String stringPrimerIngreso = properties.getProperty("ingresos").split(";")[1];
        Integer hora = Integer.parseInt(stringPrimerIngreso.split(":")[0]);
        Integer minuto = Integer.parseInt(stringPrimerIngreso.split(":")[1]);
        return new Time(hora, minuto, 0);
    }
}
