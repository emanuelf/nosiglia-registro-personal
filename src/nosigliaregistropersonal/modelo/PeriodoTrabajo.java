package nosigliaregistropersonal.modelo;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import org.hibernate.Session;
import org.hibernate.metamodel.ValidationException;

@Entity
@Table(name = "periodos_trabajo")
public class PeriodoTrabajo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String descripcion;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar inicio;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fin;
    private Integer horasLaborales = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Calendar getInicio() {
        return inicio;
    }

    public void setInicio(Calendar inicio) {
        this.inicio = inicio;
    }

    public Calendar getFin() {
        return fin;
    }

    public void setFin(Calendar fin) {
        this.fin = fin;
    }

    public Integer getHorasLaborales() {
        return horasLaborales;
    }

    public void setHorasLaborales(Integer horasLaborales) {
        this.horasLaborales = horasLaborales;
    }

    public void save() {
        if (horasLaborales < 1 || inicio == null || fin == null || descripcion.isEmpty()) {
            throw new ValidationException("");
        }
        Service.saveOrUpdate(this);
    }

    public void setFin(Date date) {
        GregorianCalendar aux = new GregorianCalendar();
        aux.setTime(date);
        this.fin = aux;
    }

    public void setInicio(Date date) {
        GregorianCalendar aux = new GregorianCalendar();
        aux.setTime(date);
        this.inicio = aux;
    }

    public boolean usado() {
            Long cantidadLiquidaciones = Service.executeQuery(new QueryRunner<Long>() {

            @Override
            public Long runQueryIn(Session session) {
                return (Long) session.createQuery(
                        "select count(*) "
                        + "from Liquidacion where periodoTrabajo = :periodoTrabajo").
                        setEntity("periodoTrabajo", PeriodoTrabajo.this).
                        uniqueResult();
            }
        });
        return cantidadLiquidaciones != 0;
    }

    public static Collection<PeriodoTrabajo> all(){
        return Service.findAll(PeriodoTrabajo.class, null);
    }
    public void delete() {
        Service.delete(this);
    }

    @Override
    public String toString() {
        return descripcion + ". Del " + OutputUtils.formatShort(inicio.getTime()) + " al " + OutputUtils.formatShort(fin.getTime());
    }
    
    public String periodoToString(){
        return OutputUtils.formatShort(inicio.getTime()) + "-" + OutputUtils.formatShort(fin.getTime());
    }
}