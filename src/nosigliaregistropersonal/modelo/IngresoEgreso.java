package nosigliaregistropersonal.modelo;

import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import nosigliaregistropersonal.modelo.helpers.MarcaHelper;

/**
 *
 * @author #emas
 */
@Entity
@Table(name = "ingresos_egresos")
public class IngresoEgreso {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Empleado empleado;
    @OneToOne
    private Marca ingreso;
    @OneToOne
    private Marca egreso;

    public IngresoEgreso() {
    }

    public IngresoEgreso(Marca ingreso, Marca egreso) {
        this.ingreso = ingreso;
        this.egreso = egreso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Marca getIngreso() {
        return ingreso;
    }

    public void setIngreso(Marca ingreso) {
        this.ingreso = ingreso;
    }

    public Marca getEgreso() {
        return egreso;
    }

    public void setEgreso(Marca egreso) {
        this.egreso = egreso;
    }
    
    public boolean isIngresoFueraDeHorario(){
        return MarcaHelper.isMarcaFueraDeHorario(ingreso);
    }

    public String horasTrabajadas() {
        if (minutosDiferencia() == -1) {
            return null;
        }
        int minutosDiferencia = minutosDiferencia();
        int horas = minutosDiferencia / 60;
        int minutos = minutosDiferencia % 60;

        return normalizarHorasOMinutos(horas) + ":" + normalizarHorasOMinutos(minutos);
    }

    public int minutosDiferencia() {
        if (egreso == null || ingreso == null) {
            return 0;
        }
        Calendar fechaIngreso = ingreso.getFecha();
        Calendar fechaEgreso = egreso.getFecha();
        fechaEgreso.set(Calendar.HOUR_OF_DAY, egreso.getHora().getHours());
        fechaEgreso.set(Calendar.MINUTE, egreso.getHora().getMinutes());
        fechaIngreso.set(Calendar.HOUR_OF_DAY, ingreso.getHora().getHours());
        fechaIngreso.set(Calendar.MINUTE, ingreso.getHora().getMinutes());
        return (int) ((egreso.getFecha().getTime().getTime() / 60000) - (ingreso.getFecha().getTime().getTime() / 60000));
//            int minutosDiferencia = 0;
//            if (DateUtil.isSameDay(egreso.getFecha().getTime(), ingreso.getFecha().getTime())) {
//                minutosDiferencia = egreso.getHora().getHours() * 60 + egreso.getHora().getMinutes();
//                minutosDiferencia -= (ingreso.getHora().getHours() * 60 + ingreso.getHora().getMinutes());
//            } else {
//                minutosDiferencia = -1;
//            }
//            return minutosDiferencia;
    }

    private String normalizarHorasOMinutos(int minutos) {
        String minutosString = "";
        if (minutos == 0) {
            minutosString = "00";
        } else if (minutos < 10) {
            minutosString = "0" + Integer.toString(minutos);
        } else {
            minutosString = Integer.toString(minutos);
        }
        return minutosString;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.ingreso);
        hash = 53 * hash + Objects.hashCode(this.egreso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IngresoEgreso other = (IngresoEgreso) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.ingreso, other.ingreso)) {
            return false;
        }
        if (!Objects.equals(this.egreso, other.egreso)) {
            return false;
        }
        return true;
    }

}
