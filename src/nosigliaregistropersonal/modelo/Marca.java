package nosigliaregistropersonal.modelo;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Emanuel
 */
@Entity
@Table(name = "marcas")
public class Marca {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String uid;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fecha;
    private Time hora;
    private Time horaReal;
    @ManyToOne
    private Empleado empleado;
    @Column(columnDefinition = "boolean default false")
    private boolean liquidado = false;
    private boolean activo = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public boolean isLiquidado() {
        return liquidado;
    }

    public void setLiquidado(boolean liquidado) {
        this.liquidado = liquidado;
    }

    public Time getHoraReal() {
        return horaReal;
    }

    public void setHoraReal(Time horaReal) {
        this.horaReal = horaReal;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date fechaYHora() {
        return new Date(
                fecha.get(Calendar.YEAR),
                fecha.get(Calendar.MONTH),
                fecha.get(Calendar.DATE),
                hora.getHours(),
                hora.getMinutes(),
                hora.getSeconds());
    }

    public static List<Marca> buscarDelDia() {
        return Service.executeQuery(new QueryRunner<List<Marca>>() {
            @Override
            public List<Marca> runQueryIn(Session session) {
                System.out.println(OutputUtils.formatFechaYHora(Calendar.getInstance().getTime()));
                Criteria criteria = session.createCriteria(Marca.class).add(Restrictions.eq("fecha", Calendar.getInstance()));
                criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                return criteria.list();
            }
        });
    }

    public static List<Marca> buscarMarcasNoLiquidadasEntre(Date desde, Date hasta, final Empleado empleado) {
        final GregorianCalendar gDesde = new GregorianCalendar();
        final GregorianCalendar gHasta = new GregorianCalendar();
        gDesde.setTime(desde);
        gHasta.setTime(hasta);
        return Service.executeQuery(new QueryRunner<List<Marca>>() {
            @Override
            public List<Marca> runQueryIn(Session session) {
                Criteria criteria = session.createCriteria(Marca.class).add(Restrictions.between("fecha", gDesde, gHasta));
                criteria.
                        addOrder(Order.asc("fecha")).
                        addOrder(Order.asc("hora"));
                if (empleado != null) {
                    criteria = criteria.add(Restrictions.eq("empleado", empleado));
                }
                criteria = criteria.add(Restrictions.eq("liquidado", false));
                criteria = criteria.add(Restrictions.eq("activo", true));
                return criteria.list();
            }
        });
    }

    public static void softDelete(final Marca marca){
        Service.executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                marca.activo = false;
                session.update(marca);
                return null;
            }
        });
    }
    
    public void guardar() {
        Service.executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                session.saveOrUpdate(Marca.this);
                return null;
            }
        });
    }
}
