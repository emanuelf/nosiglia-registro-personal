package nosigliaregistropersonal.modelo;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import nosigliaregistropersonal.modelo.persistencia.QueryRunner;
import nosigliaregistropersonal.modelo.persistencia.Service;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author #emas
 */
@Entity
@Table(name = "liquidaciones")
public class Liquidacion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    PeriodoTrabajo periodoTrabajo;
    @ManyToOne
    private Empleado empleado;
    @ManyToOne
    private OrdenDePago ordenDePago;
    private double precioHora;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<IngresoEgreso> ingresosEgresos = new HashSet<>();
    private int horasExtras = 0;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PeriodoTrabajo getPeriodoTrabajo() {
        return periodoTrabajo;
    }

    public void setPeriodoTrabajo(PeriodoTrabajo periodoTrabajo) {
        this.periodoTrabajo = periodoTrabajo;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public OrdenDePago getOrdenDePago() {
        return ordenDePago;
    }

    public void setOrdenDePago(OrdenDePago ordenDePago) {
        this.ordenDePago = ordenDePago;
    }

    public Set<IngresoEgreso> getIngresosEgresos() {
        return ingresosEgresos;
    }

    public void setIngresosEgresos(Set<IngresoEgreso> ingresosEgresos) {
        this.ingresosEgresos = ingresosEgresos;
    }

    public int minutosTrabajados() {
        int minutosTrabajados = 0;
        for (IngresoEgreso ingresoEgreso : getIngresosEgresos()) {
            if (ingresoEgreso.minutosDiferencia() != -1) {
                minutosTrabajados += ingresoEgreso.minutosDiferencia();
            }
        }
        return minutosTrabajados;
    }

    public double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(double precioHora) {
        this.precioHora = precioHora;
    }

    public int getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

    public static void guardar(final Liquidacion liquidacion) {
        Service.executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                for (IngresoEgreso ingresoEgreso : liquidacion.ingresosEgresos) {
                    ingresoEgreso.setEmpleado(liquidacion.empleado);
                    session.saveOrUpdate(ingresoEgreso);
                    ingresoEgreso.getIngreso().setLiquidado(true);
                    ingresoEgreso.getEgreso().setLiquidado(true);
                    session.update(ingresoEgreso.getIngreso());
                    session.update(ingresoEgreso.getEgreso());
                }
                session.saveOrUpdate(liquidacion);
                return null;
            }
        });
    }

    public double totalLiquidacion() {
        double precioMinuto = precioHora / 60;
        return precioMinuto * minutosTrabajados();
    }

    public Date getDesde() {
        return getPeriodoTrabajo().getInicio().getTime();
    }

    public Date getHasta() {
        return getPeriodoTrabajo().getFin().getTime();
    }
    
    public static Collection<Liquidacion> liquidacionesNoUsadas(final Empleado empleadoSeleccionado) {
        return Service.executeQuery(new QueryRunner<Collection<Liquidacion>>() {

            @Override
            public Collection<Liquidacion> runQueryIn(Session session) {
                return session.createCriteria(Liquidacion.class).
                        add(Restrictions.eq("empleado", empleadoSeleccionado)).
                        add(Restrictions.isNull("ordenDePago")).
                        setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                        list();
            }
        });
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Liquidacion other = (Liquidacion) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }   
}