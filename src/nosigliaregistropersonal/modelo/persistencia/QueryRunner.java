package nosigliaregistropersonal.modelo.persistencia;

import org.hibernate.Session;

public interface QueryRunner<T> {

    /**
     * ejecuta un metodo dentro de la session dada o lanza null pointer
     * exception si la session es null
     *
     * @param session
     * @return T el resultado de la ejecucion del metodo.
     */
    public abstract T runQueryIn(Session session);
}