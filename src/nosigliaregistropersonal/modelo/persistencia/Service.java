package nosigliaregistropersonal.modelo.persistencia;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.PersistenceException;
import nosigliaregistropersonal.util.StringUtils;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.StaleStateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;


public class Service {

    private static final AtomicInteger ATOMIC_INTEGER = new AtomicInteger();
    private static Session session;
    private static SessionFactory SESSION_FACTORY;
    private static final String ERROR_MESSAGE = "Error de persistencia. Los cambios pueden no haber sido guardados.";

    public static synchronized void createDB() {
        Configuration conf = new Configuration();
        //conf.setProperty("hibernate.hbm2ddl.auto", "update");
        conf.configure();
        SESSION_FACTORY = conf.buildSessionFactory();
    }

    private Service() {
        throw new UnsupportedOperationException("Non instantiable");
    }

    public static synchronized void createAndSetSessionFactory() throws ExceptionInInitializerError {
        SESSION_FACTORY = new Configuration().
                //setInterceptor(new ProductoInterceptor()).
                configure().
                buildSessionFactory();
    }

    public static void saveOrUpdate(final Collection<?> objetos) {
        if (objetos instanceof Set) {
            saveOrUpdate((Set) objetos);
        }
        if (objetos instanceof List) {
            saveOrUpdate((List) objetos);
        }
    }

    static void saveOrUpdate(final Set<?> objetos) {
        QueryRunner<Void> updateCollection = new QueryRunner<Void>() {

            @Override
            @SuppressWarnings("unchecked")
            public Void runQueryIn(Session session) {
                List list = new ArrayList<>(objetos);
                objetos.clear();
                saveOrUpdate(list);
                objetos.addAll(list);
                return null;
            }
        };
        executeQuery(updateCollection);
    }

    static void saveOrUpdate(final List<?> objetos) {
        QueryRunner<Void> updateCollection = new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                for (Object object : objetos) {
                    session.saveOrUpdate(object);
                }
                return null;
            }
        };
        executeQuery(updateCollection);
    }

    public static void saveOrUpdate(final Object objeto) {
        executeQuery(new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                session.saveOrUpdate(objeto);
                return null;
            }
        });
    }

    public static void save(final Object objeto) {
        QueryRunner<Void> saveSingle = new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                session.save(objeto);
                return null;
            }
        };
        executeQuery(saveSingle);
    }

    public static void delete(final Object objeto) {
        QueryRunner<Void> deleteSingle = new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                session.delete(objeto);
                return null;
            }
        };
        executeQuery(deleteSingle);
    }

    public static void update(final Object objeto) {
        QueryRunner<Void> updateSingle = new QueryRunner<Void>() {

            @Override
            public Void runQueryIn(Session session) {
                session.update(objeto);
                return null;
            }
        };
        executeQuery(updateSingle);
    }

    /**
     *
     * @param <T> el tipo de resultado esperado
     * @param query el queryRunner con el codigo a ejecutar dentro de una transaccion
     * @return el resultado de la transaccion (de tipo T), definido por el queryRunner.
     */
    public static synchronized <T> T executeQuery(QueryRunner<T> query) {
        if (ATOMIC_INTEGER.incrementAndGet() == 1) {
            if (SESSION_FACTORY == null) {
                createAndSetSessionFactory();
            }
            session = SESSION_FACTORY.openSession();
            ManagedSessionContext.bind(session);
            session.beginTransaction();
        }
        try {
            T result = query.runQueryIn(session);
            if (ATOMIC_INTEGER.decrementAndGet() == 0) {
                ManagedSessionContext.unbind(SESSION_FACTORY);
                session.getTransaction().commit();
                session.close();
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            session.getTransaction().rollback();
            session.close();
            ATOMIC_INTEGER.getAndSet(0);
            if (ex instanceof HibernateException && ex.getCause() instanceof BatchUpdateException) {
                throw new PersistenceException(((BatchUpdateException) ex.getCause()).
                        getNextException().getMessage());
            }
            if (ex instanceof HibernateException && ex.getCause() instanceof ConstraintViolationException) {
                throw new PersistenceException(((ConstraintViolationException) ex.getCause()).
                        getSQLException().getMessage());
            }
            if (ex instanceof StaleStateException | ex instanceof StaleObjectStateException) {
                throw ex;
            } else {
                throw new PersistenceException(ERROR_MESSAGE, ex);
            }
        }
    }

    public static <T> T load(final Class<T> clase, final Serializable id) {
        return executeQuery(new QueryRunner<T>() {

            @Override
            @SuppressWarnings("unchecked")
            public T runQueryIn(Session session) {
                T load = (T) session.get(clase, id);
                return load;
            }
        });
    }

    public static <T> T initializeProperty(final Class<?> clase, final Serializable id, final String propertyName) {
        return executeQuery(new QueryRunner<T>() {

            private final Class[] noArgs = new Class[0];
            private final Object[] emptyArgs = new Object[0];

            @Override
            public T runQueryIn(Session session) {
                Object load = session.load(clase, id);
                Method getter = getGetterFor(propertyName);
                return invokeGetter(getter, load);
            }

            private Method getGetterFor(String propertyName) throws PersistenceException {
                String nombreMetodo = "get" + StringUtils.capitalizeFirstLetter(propertyName);
                try {
                    return clase.getMethod(nombreMetodo, noArgs);
                } catch (NoSuchMethodException | SecurityException ex) {
                    try {
                        return clase.getDeclaredMethod(nombreMetodo, noArgs);
                    } catch (NoSuchMethodException | SecurityException ex1) {
                        final String notFoundMessage = "Metodo " + nombreMetodo + " no encontrado por ";
                        throw new PersistenceException(notFoundMessage + ex1.getClass(), ex1);
                    }
                }
            }

            @SuppressWarnings("unchecked")
            private T invokeGetter(Method getter, Object load) throws PersistenceException {
                try {
                    getter.setAccessible(true);
                    if (Collection.class.isAssignableFrom(getter.getReturnType())) {
                        throw new UnsupportedOperationException("No funciona con colecciones.");
                    }
                    return (T) getter.invoke(load, emptyArgs);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    final String ERROR_MESSAGE = "Error al invocar el getter. ";
                    throw new PersistenceException(ERROR_MESSAGE + ex.getClass(), ex);
                }
            }
        });
    }

    public static <E> E findFirst(final Class<E> searchedClass, final String propertyName, final Map<String, Object> where) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.allEq(where)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static <E> E findFirst(final Class<E> searchedClass, final String propertyName, final String neProperty, final Object neValue) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.ne(neProperty, neValue)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static <E> E findFirst(final Class<E> searchedClass, final String propertyName, final String nullProperty) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.isNull(nullProperty)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static <E> E findPrevious(final Class<E> searchedClass, final Object id, final String propertyName, final Map<String, Object> where) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.lt(propertyName, id)).
                        add(Restrictions.allEq(where)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findFirst(searchedClass, propertyName, where);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findPrevious(final Class<E> searchedClass, final Object id, final String propertyName, final String neProperty, final Object neValue) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.lt(propertyName, id)).
                        add(Restrictions.ne(neProperty, neValue)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findFirst(searchedClass, propertyName, neProperty, neValue);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findPrevious(final Class<E> searchedClass, final Object id, final String propertyName, final String notNullProperty) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.lt(propertyName, id)).
                        add(Restrictions.isNull(notNullProperty)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findFirst(searchedClass, propertyName, notNullProperty);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findNext(final Class<E> searchedClass, final Object id, final String propertyName, final Map<String, Object> where) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.gt(propertyName, id)).
                        add(Restrictions.allEq(where)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findLast(searchedClass, propertyName, where);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findNext(final Class<E> searchedClass, final Object id, final String propertyName, final String neProperty, final Object neValue) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.gt(propertyName, id)).
                        add(Restrictions.ne(neProperty, neValue)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findLast(searchedClass, propertyName, neProperty, neValue);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findNext(final Class<E> searchedClass, final Object id, final String propertyName, final String nullProperty) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                E anterior = (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.asc(propertyName)).
                        add(Restrictions.gt(propertyName, id)).
                        add(Restrictions.isNull(nullProperty)).
                        setMaxResults(1).
                        uniqueResult();
                if (anterior == null) {
                    return findLast(searchedClass, propertyName, nullProperty);
                } else {
                    return anterior;
                }
            }
        });
    }

    public static <E> E findLast(final Class<E> searchedClass, final String propertyName, final Map<String, Object> where) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.allEq(where)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static <E> E findLast(final Class<E> searchedClass, final String propertyName, final String neProperty, final Object neValue) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.ne(neProperty, neValue)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static <E> E findLast(final Class<E> searchedClass, final String propertyName, final String nullProperty) {
        return executeQuery(new QueryRunner<E>() {

            @Override
            @SuppressWarnings("unchecked")
            public E runQueryIn(Session session) {
                return (E) session.
                        createCriteria(searchedClass).
                        addOrder(Order.desc(propertyName)).
                        add(Restrictions.isNull(nullProperty)).
                        setMaxResults(1).
                        uniqueResult();
            }
        });
    }

    public static Map<String, Object> getActivoMap() {
        Map<String, Object> whereMap = new HashMap<>();
        whereMap.put("activo", true);
        return whereMap;
    }

    public static <T> List<T> findAll(final Class<T> clase, final String softDeleteProperty) {
        return Service.executeQuery(new QueryRunner<List<T>>() {

            @Override
            @SuppressWarnings("unchecked")
            public List<T> runQueryIn(Session session) {
                Criteria criteria = session.createCriteria(clase);
                if (softDeleteProperty != null) {
                    criteria = criteria.add(Restrictions.eq(softDeleteProperty, true));
                }
                criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                return criteria.list();
            }
        });
    }
    public static <T> List<T> findAll(final Class<T> clase, String orderBy, final String softDeleteProperty) {
        return Service.executeQuery(new QueryRunner<List<T>>() {
            @Override
            @SuppressWarnings("unchecked")
            public List<T> runQueryIn(Session session) {
                Criteria criteria = session.createCriteria(clase);
                if (softDeleteProperty != null) {
                    criteria = criteria.add(Restrictions.eq(softDeleteProperty, true));
                }
                criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
                return criteria.list();
            }
        });
    }
    public static <E> Collection<E> findBetween(final Class<E> clase, final String property, final int desde, final int hasta) {
        return Service.executeQuery(new QueryRunner<Collection<E>>() {

            @Override
            @SuppressWarnings("unchecked")
            public Collection<E> runQueryIn(Session session) {
                int min = Math.min(desde, hasta);
                int max = Math.max(desde, hasta);
                return session.
                        createCriteria(clase, "c").
                        add(Restrictions.between(property, min, max)).
                        list();
            }
        });

    }
}
