package nosigliaregistropersonal.vistas.framework;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.Beans;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

public class JPnlBusqueda extends javax.swing.JPanel {

    Buscador buscador;
    SingleSelectionTableAdapter tableAdapter;

    public JPnlBusqueda() {
        initComponents();
    }

    public JPnlBusqueda(Buscador buscador) {
        this();
        this.buscador = buscador;
        if (!Beans.isDesignTime()) {
            crearYAsignarTableAdapter(buscador);
            crearBuscarAction();
            crearSeleccionarAction();
            agregarListeners();
            
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jXTblResultados = new org.jdesktop.swingx.JXTable();
        jXSearch = new org.jdesktop.swingx.JXSearchField();
        jXTitledSeparator1 = new org.jdesktop.swingx.JXTitledSeparator();

        jXTblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTblResultados);

        jXSearch.setToolTipText("Ingrese lo que quiere buscar");
        jXSearch.setLayoutStyle(org.jdesktop.swingx.JXSearchField.LayoutStyle.MAC);
        jXSearch.setPrompt("");
        jXSearch.setSearchMode(org.jdesktop.swingx.JXSearchField.SearchMode.REGULAR);

        jXTitledSeparator1.setTitle("Resultados");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jXSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXSearchField jXSearch;
    private org.jdesktop.swingx.JXTable jXTblResultados;
    private org.jdesktop.swingx.JXTitledSeparator jXTitledSeparator1;
    // End of variables declaration//GEN-END:variables

    private void crearYAsignarTableAdapter(final Buscador buscador) {
        CustomTableModel ctm = new CustomTableModel(buscador.columnas());
        tableAdapter = new SingleSelectionTableAdapter(jXTblResultados, ctm) {
            @Override
            public Vectorizable toWrapper(Object element) {
                return buscador.wrap(element);
            }

            @Override
            public Object fromWrapper(Vectorizable wrapper) {
                return buscador.unWrap((WrapperNoEditable) wrapper);
            }
        };
    }

    private void agregarCambiarFocoAlPresionarAbajoKeyListener() {
        jXSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    jXTblResultados.requestFocusInWindow();
                }
            }
        });
    }

    private void agregarCambiarFocoAlJXSearchFieldAlPresionarArribaYEstarEnLaPrimeraFilaKeyListener() {
        jXTblResultados.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (jXTblResultados.getSelectedRow() == 0 && e.getKeyCode() == KeyEvent.VK_UP) {
                    jXSearch.requestFocusInWindow();
                }
            }
        });
    }

    private void agregarDoubleClickListener() {
        jXTblResultados.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    setSelectionAndDispose();
                }
            }
        });
    }

    public Object elementoSeleccionado() {
        return tableAdapter.getSelectedElement();
    }

    private void crearBuscarAction() {
        jXSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableAdapter.setElements(buscador.buscar(jXSearch.getText()));
            }
        });
    }

    private void crearSeleccionarAction() {
        jXTblResultados.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    setSelectionAndDispose();
                }
            }
        });
    }

    private void setSelectionAndDispose() {
        buscador.setSeleccion(tableAdapter.getSelectedElement());
        SwingUtilities.getWindowAncestor(jScrollPane1).dispose();
    }

    private void agregarListeners() {
        agregarCambiarFocoAlPresionarAbajoKeyListener();
        agregarCambiarFocoAlJXSearchFieldAlPresionarArribaYEstarEnLaPrimeraFilaKeyListener();
        agregarDoubleClickListener();
    }

    public void executeQuery(String textBusqueda) {
        jXSearch.setText(textBusqueda);
        tableAdapter.setElements(buscador.buscar(textBusqueda));
    }
}
