package nosigliaregistropersonal.vistas.framework;


import java.awt.Component;
import java.awt.Window;
import java.util.Collection;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

/**
 *
 * @author Emanuel
 */
public abstract class Buscador<O> {

    
    protected JPnlBusqueda panelBusqueda;
    protected JPanel oyente;
    protected O seleccion;
    
    public Buscador(JPanel oyente) {
        this.oyente = oyente;
        panelBusqueda = new JPnlBusqueda(this);
    }
    
    
    public O buscar(){
        mostrarPanelBusquedaEnModal();
        return seleccion;
    }
    
    public abstract Collection<O> buscar(String textBusqueda);
    
    public O buscarEnPanel(String textBusqueda){throw new AbstractMethodError("method para ser implementado");};
    
    public abstract String[] columnas() ;

    public abstract O unWrap(WrapperNoEditable<O> wrapper);

    public abstract WrapperNoEditable<O> wrap(O element);

    void setSeleccion(O selectedElement) {
        this.seleccion = selectedElement;
    }

    protected void mostrarPanelBusquedaEnModal() {
        Window padreBusqueda = SwingUtilities.getWindowAncestor((Component) oyente);
        PanelsUtil.showPanelOnModalDialog(padreBusqueda, panelBusqueda, null);
    }
}
