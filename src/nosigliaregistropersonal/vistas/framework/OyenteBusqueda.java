package nosigliaregistropersonal.vistas.framework;

import java.awt.Component;

/**
 *
 * @author VARIOS
 * @param <O>
 */
public interface OyenteBusqueda<O> {

    
    public abstract void busquedaFinalizada(O objetoEncontrado);
}