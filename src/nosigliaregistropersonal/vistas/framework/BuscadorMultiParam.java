package nosigliaregistropersonal.vistas.framework;

import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 *
 * @author Emanuel
 * @param <O>
 * @param <E>
 */
public abstract class BuscadorMultiParam<O, E extends Enum> extends Buscador<O>{

    public BuscadorMultiParam(JPanel oyente) {
        super(oyente);
    }
    
        
    public abstract List<O> buscar(Map<E, Object> values);
    
}
