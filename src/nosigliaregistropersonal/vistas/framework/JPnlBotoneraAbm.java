package nosigliaregistropersonal.vistas.framework;

import java.awt.event.ActionListener;
import java.beans.Beans;
import java.net.URL;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.abms.BotoneraAbm;

/**
 *
 * @author Emanuel
 */
public class JPnlBotoneraAbm extends JPanel implements BotoneraAbm {

    private Action cancelarAction;
    private Action editarAction;

    public JPnlBotoneraAbm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar = new JToolBar();
        jBtnAceptar = new JButton();
        jBtnAgregar = new JButton();
        jBtnEditarCancelar = new JButton();
        jBtnEliminar = new JButton();
        jBtnBuscar = new JButton();
        jBtnImprimir = new JButton();

        jToolBar.setFloatable(false);
        jToolBar.setOrientation(SwingConstants.VERTICAL);
        jToolBar.setRollover(true);
        jToolBar.setBorderPainted(false);

        jBtnAceptar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/ok.png"))); // NOI18N
        jBtnAceptar.setFocusable(false);
        jBtnAceptar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnAceptar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnAceptar);

        jBtnAgregar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/agregar.png"))); // NOI18N
        jBtnAgregar.setEnabled(false);
        jBtnAgregar.setFocusable(false);
        jBtnAgregar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnAgregar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnAgregar);

        jBtnEditarCancelar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/editar.png"))); // NOI18N
        jBtnEditarCancelar.setEnabled(false);
        jBtnEditarCancelar.setFocusable(false);
        jBtnEditarCancelar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEditarCancelar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnEditarCancelar);

        jBtnEliminar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/eliminar.png"))); // NOI18N
        jBtnEliminar.setEnabled(false);
        jBtnEliminar.setFocusable(false);
        jBtnEliminar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEliminar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnEliminar);

        jBtnBuscar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/buscar.png"))); // NOI18N
        jBtnBuscar.setEnabled(false);
        jBtnBuscar.setFocusable(false);
        jBtnBuscar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnBuscar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnBuscar);

        jBtnImprimir.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/imprimir.png"))); // NOI18N
        jBtnImprimir.setEnabled(false);
        jBtnImprimir.setFocusable(false);
        jBtnImprimir.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnImprimir.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar.add(jBtnImprimir);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar, GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnAceptar;
    private JButton jBtnAgregar;
    private JButton jBtnBuscar;
    private JButton jBtnEditarCancelar;
    private JButton jBtnEliminar;
    private JButton jBtnImprimir;
    private JToolBar jToolBar;
    // End of variables declaration//GEN-END:variables

    @Override
    public void agregar() {
        jBtnAgregar.doClick();
    }

    @Override
    public void buscar() {
        jBtnBuscar.doClick();
    }

    @Override
    public void cancelar() {
        jBtnEditarCancelar.setIcon(Iconos.EDITAR_ABM);
    }

    @Override
    public void editar() {
        jBtnEditarCancelar.setIcon(Iconos.CANCELAR_ABM);
    }

    @Override
    public void eliminar() {
        doClick(jBtnEliminar);
    }

    @Override
    public void guardar() {
        doClick(jBtnAceptar);
    }

    private void doClick(JButton button) {
        button.doClick();
    }

    @Override
    public void setAgregarAction(Action action) {
        jBtnAgregar.addActionListener(action);
    }

    @Override
    public void setBuscarAction(Action action) {
        jBtnBuscar.addActionListener(action);
    }

    @Override
    public void setCancelarAction(Action action) {
        this.cancelarAction = action;
    }

    @Override
    public void setEditarAction(Action action) {
        this.editarAction = action;
    }

    @Override
    public void setEliminarAction(Action action) {
        jBtnEliminar.addActionListener(action);
    }

    @Override
    public void setGuardarAction(Action action) {
        jBtnAceptar.addActionListener(action);
    }

    @Override
    public void setImprimirAction(Action imprimirAction) {
        jBtnImprimir.addActionListener(imprimirAction);
    }
    
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(new JPnlBotoneraAbm());
        jFrame.pack();
        jFrame.setVisible(true);

    }

    @Override
    public void modoInicial() {
        jBtnAceptar.setEnabled(false);
        jBtnAgregar.setEnabled(true);
        jBtnBuscar.setEnabled(true);
        jBtnEditarCancelar.setEnabled(false);
        jBtnEliminar.setEnabled(false);
    }

    @Override
    public void modoEdicion() {
        jBtnAceptar.setEnabled(true);
        jBtnEditarCancelar.setEnabled(true);
        
        jBtnAgregar.setEnabled(false);
        jBtnBuscar.setEnabled(false);
        jBtnEliminar.setEnabled(false);
        jBtnImprimir.setEnabled(false);
        
        jBtnEditarCancelar.setIcon(Iconos.CANCELAR_ABM);
        for (ActionListener actionListener : jBtnEditarCancelar.getActionListeners()) {
            jBtnEditarCancelar.removeActionListener(actionListener);
        }
        jBtnEditarCancelar.addActionListener(cancelarAction);
    }

    @Override
    public void modoVisualizacion() {
        jBtnAgregar.setEnabled(true);
        jBtnBuscar.setEnabled(true);
        jBtnEditarCancelar.setEnabled(true);
        jBtnEliminar.setEnabled(true);
        jBtnImprimir.setEnabled(true);
        jBtnAceptar.setEnabled(false);
        jBtnEditarCancelar.setIcon(Iconos.EDITAR_ABM);
        for (ActionListener actionListener : jBtnEditarCancelar.getActionListeners()) {
            jBtnEditarCancelar.removeActionListener(actionListener);
        }
        jBtnEditarCancelar.addActionListener(editarAction);    }
}
