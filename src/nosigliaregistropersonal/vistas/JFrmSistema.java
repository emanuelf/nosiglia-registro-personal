package nosigliaregistropersonal.vistas;

public class JFrmSistema extends javax.swing.JFrame {

    public JFrmSistema() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPnlBrowseTrabajadores1 = new nosigliaregistropersonal.vistas.abms.JPnlBrowseTrabajadores();
        jPnlBrowsePeriodosTrabajo1 = new nosigliaregistropersonal.vistas.abms.JPnlBrowsePeriodosTrabajo();
        jPnlFichadasDelDia1 = new nosigliaregistropersonal.vistas.otros.JPnlFichadasDelDia();
        jPnlFichadasPorFecha1 = new nosigliaregistropersonal.vistas.otros.JPnlFichadasPorFecha();
        jPnlHorasTrabajadasPorDia1 = new nosigliaregistropersonal.vistas.otros.JPnlHorasTrabajadasPorDia();
        jPnlBrowseFormasDePago1 = new nosigliaregistropersonal.vistas.abms.JPnlBrowseFormasDePago();
        jPnlAbmAdelanto1 = new nosigliaregistropersonal.vistas.abms.JPnlAbmAdelanto();
        jPnlAbmOrdenDePago1 = new nosigliaregistropersonal.vistas.abms.JPnlAbmOrdenDePago();
        jXHeader1 = new org.jdesktop.swingx.JXHeader();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de control de ingreso de personal");
        setBackground(new java.awt.Color(79, 156, 106));

        jTabbedPane1.addTab("Trabajadores", jPnlBrowseTrabajadores1);
        jTabbedPane1.addTab("Periodos de trabajo", jPnlBrowsePeriodosTrabajo1);
        jTabbedPane1.addTab("Fichajes del dia", jPnlFichadasDelDia1);
        jTabbedPane1.addTab("Fichajes por fecha", jPnlFichadasPorFecha1);
        jTabbedPane1.addTab("Horas trabajadas por dia", jPnlHorasTrabajadasPorDia1);
        jTabbedPane1.addTab("Formas de pago", jPnlBrowseFormasDePago1);
        jTabbedPane1.addTab("Adelantos", jPnlAbmAdelanto1);
        jTabbedPane1.addTab("Ordenes de pago", jPnlAbmOrdenDePago1);

        jXHeader1.setBackground(new java.awt.Color(142, 204, 82));
        jXHeader1.setDescription("Este sistema le ayudara en la tarea de controlar ingresos y egresos de personal, y generar las ordenes de pago correspondientes");
        jXHeader1.setTitle("Bienvenido al sistema de control de ingreso de personal");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jXHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jXHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 388, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private nosigliaregistropersonal.vistas.abms.JPnlAbmAdelanto jPnlAbmAdelanto1;
    private nosigliaregistropersonal.vistas.abms.JPnlAbmOrdenDePago jPnlAbmOrdenDePago1;
    private nosigliaregistropersonal.vistas.abms.JPnlBrowseFormasDePago jPnlBrowseFormasDePago1;
    private nosigliaregistropersonal.vistas.abms.JPnlBrowsePeriodosTrabajo jPnlBrowsePeriodosTrabajo1;
    private nosigliaregistropersonal.vistas.abms.JPnlBrowseTrabajadores jPnlBrowseTrabajadores1;
    private nosigliaregistropersonal.vistas.otros.JPnlFichadasDelDia jPnlFichadasDelDia1;
    private nosigliaregistropersonal.vistas.otros.JPnlFichadasPorFecha jPnlFichadasPorFecha1;
    private nosigliaregistropersonal.vistas.otros.JPnlHorasTrabajadasPorDia jPnlHorasTrabajadasPorDia1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    // End of variables declaration//GEN-END:variables
}
