package nosigliaregistropersonal.vistas.otros;

import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.Marca;
import nosigliaregistropersonal.vistas.abms.JPnlEdicionMarca;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

/**
 *
 * @author #emas
 */
public class JPnlFichadasPorFecha extends javax.swing.JPanel {

    private SingleSelectionTableAdapter<Marca, MarcaWrapper> tableAdapter;

    public JPnlFichadasPorFecha() {
        initComponents();
        if (!Beans.isDesignTime()) {
            crearYAsignarTableAdapter();
            asignarListeners();
            agregarJPopupMenuEliminar();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXDPDesde = new org.jdesktop.swingx.JXDatePicker();
        jXDPHasta = new org.jdesktop.swingx.JXDatePicker();
        jLblDesde = new javax.swing.JLabel();
        jLblHasta = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTblResultados = new org.jdesktop.swingx.JXTable();
        jXTitledSeparator1 = new org.jdesktop.swingx.JXTitledSeparator();
        jBtnBuscar = new javax.swing.JButton();

        jXDPDesde.setToolTipText("");

        jLblDesde.setText("Desde");

        jLblHasta.setText("Hasta");

        jXTblResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTblResultados);

        jXTitledSeparator1.setTitle("Resultados");

        jBtnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/15x15/search.png"))); // NOI18N
        jBtnBuscar.setText("Buscar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLblDesde, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jXDPDesde, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblHasta)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jXDPHasta, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnBuscar)))
                        .addGap(0, 438, Short.MAX_VALUE))
                    .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLblDesde)
                    .addComponent(jLblHasta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jXDPDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDPHasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBtnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jXTitledSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnBuscar;
    private javax.swing.JLabel jLblDesde;
    private javax.swing.JLabel jLblHasta;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXDatePicker jXDPDesde;
    private org.jdesktop.swingx.JXDatePicker jXDPHasta;
    private org.jdesktop.swingx.JXTable jXTblResultados;
    private org.jdesktop.swingx.JXTitledSeparator jXTitledSeparator1;
    // End of variables declaration//GEN-END:variables

    private JPnlEdicionMarca panelEdicion = new JPnlEdicionMarca();
    
    private void crearYAsignarTableAdapter() {
        CustomTableModel<MarcaWrapper> ctm = new CustomTableModel<>(MarcaWrapper.COLUMNAS);
        tableAdapter = new SingleSelectionTableAdapter<Marca, MarcaWrapper>(jXTblResultados, ctm) {
            @Override
            public MarcaWrapper toWrapper(Marca element) {
                return new MarcaWrapper(element);
            }
            
            @Override
            public Marca fromWrapper(MarcaWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
        
    }

    private void asignarListeners() {
        jBtnBuscar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Date desde = jXDPDesde.getDate();
                Date hasta = jXDPHasta.getDate();
                if (desde == null || hasta == null){
                    MessageUtils.showWarning(null, "Ingrese la fecha desde y hasta");
                    return;
                }
                Collection<Marca> marcas = Marca.buscarMarcasNoLiquidadasEntre(desde, hasta, null);
                tableAdapter.setElements(marcas);
            }
        });
    }

    private void agregarJPopupMenuEliminar() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        jPopupMenu.add(makePopupEliminar());
        jPopupMenu.add(makePopupEditar());
        jXTblResultados.setComponentPopupMenu(jPopupMenu);
        
    }

    private JMenuItem makePopupEliminar() {
        JMenuItem jMnuItem = new JMenuItem("Eliminar marca", Iconos.ELIMINAR_16X16);
        jMnuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter.hasSelection()) {
                    if (MessageUtils.areYouSure("Esta seguro de eliminar esta marca?")) {
                        Marca.softDelete(tableAdapter.getSelectedElement());
                        tableAdapter.removeSelectedElement();
                    }
                } else {
                    MessageUtils.showInfo(null, "Seleccione la marca a borrar");
                }
            }
        });
        return jMnuItem;
        
    }

    private JMenuItem makePopupEditar() {
        JMenuItem jMnuItem = new JMenuItem("Editar", Iconos.EDITAR_16X16);
        jMnuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter.hasSelection()) {
                    panelEdicion.setMarca(tableAdapter.getSelectedElement());
                    PanelsUtil.showPanelOnModalDialog(
                            SwingUtilities.getWindowAncestor(jBtnBuscar), 
                            panelEdicion, 
                            "Editar marca");
                } else {
                    MessageUtils.showInfo(null, "Debe seleccionar una marca");
                }
            }
        });
        return jMnuItem;
    }

    private static class MarcaWrapper extends WrapperNoEditable<Marca> {

        static final String[] COLUMNAS = "Empleado, Fecha, Hora, Hora real".split(", ");
        public MarcaWrapper(Marca marca) {
            super(marca);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                getElemento().getEmpleado(), 
                OutputUtils.formatFechaYHora(getElemento().getFecha().getTime()), 
                OutputUtils.formatTime(getElemento().getHora()),
                OutputUtils.formatTime(getElemento().getHoraReal())
            });
        }
    }
}
