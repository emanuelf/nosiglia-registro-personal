package nosigliaregistropersonal.vistas.otros;

import java.beans.Beans;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.Marca;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.MultipleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.TableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;

/**
 *
 * @author Emanuel
 */
public class JPnlFichadasDelDia extends javax.swing.JPanel {

    private TableAdapter<Marca, MarcaWrapper> tableAdapter;

    public JPnlFichadasDelDia() {
        initComponents();
        if (!Beans.isDesignTime()) {
            crearYAsiganarTableAdapter();
            iniciarThreadActualizacion();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new JScrollPane();
        JPnlMarcasDiarias = new JXTable();
        jScrollPane2 = new JScrollPane();
        jLstEmpleadosRegistrados = new JList();
        jXTitledSeparator1 = new JXTitledSeparator();
        jXTitledSeparator2 = new JXTitledSeparator();
        jXTitledSeparator3 = new JXTitledSeparator();
        jScrollPane3 = new JScrollPane();
        jLstEmpleadosFaltantes = new JList();
        jLabel1 = new JLabel();
        jSpnHora = new JSpinner();
        jSpnMinuto = new JSpinner();
        jLabel2 = new JLabel();
        jCheckFiltrar = new JCheckBox();

        JPnlMarcasDiarias.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(JPnlMarcasDiarias);

        jScrollPane2.setViewportView(jLstEmpleadosRegistrados);

        jXTitledSeparator1.setTitle("Marcas registradas");

        jXTitledSeparator2.setTitle("Empleados registrados");

        jXTitledSeparator3.setTitle("Empleados faltantes");

        jScrollPane3.setViewportView(jLstEmpleadosFaltantes);

        jLabel1.setText("Ver posteriores a");

        jLabel2.setText(":");

        jCheckFiltrar.setText("Filtrar");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
                    .addComponent(jXTitledSeparator1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSpnHora, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSpnMinuto, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckFiltrar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                    .addComponent(jXTitledSeparator2, GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                    .addComponent(jXTitledSeparator3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel2)
                            .addComponent(jSpnMinuto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpnHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckFiltrar))
                        .addGap(11, 11, 11)
                        .addComponent(jXTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jXTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jXTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JXTable JPnlMarcasDiarias;
    private JCheckBox jCheckFiltrar;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JList jLstEmpleadosFaltantes;
    private JList jLstEmpleadosRegistrados;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JSpinner jSpnHora;
    private JSpinner jSpnMinuto;
    private JXTitledSeparator jXTitledSeparator1;
    private JXTitledSeparator jXTitledSeparator2;
    private JXTitledSeparator jXTitledSeparator3;
    // End of variables declaration//GEN-END:variables

    private void crearYAsiganarTableAdapter() {
        CustomTableModel<MarcaWrapper> ctm = new CustomTableModel<>(MarcaWrapper.COLUMNAS);
        tableAdapter = new MultipleSelectionTableAdapter<Marca, MarcaWrapper>(JPnlMarcasDiarias, ctm) {
            @Override
            public MarcaWrapper toWrapper(Marca element) {
                return new MarcaWrapper(element);
            }

            @Override
            public Marca fromWrapper(MarcaWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
    }

    private void iniciarThreadActualizacion() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                actualizarMarcas();
                actualizarEmpleados();
                try {
                    Thread.sleep(5000);
                    run();
                } catch (InterruptedException ex) {
                    Logger.getLogger(JPnlFichadasDelDia.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            private void actualizarMarcas() {
                List<Marca> marcasFiltradas = filtrar(Marca.buscarDelDia());
                ordenarMarcas(marcasFiltradas);
                tableAdapter.setElements(marcasFiltradas);
            }

            private void actualizarEmpleados() {
                List<Empleado> empleadosConMarca = empleadosConMarca();
                List<Empleado> empleadosSinMarca = Empleado.findNotIIn(empleadosConMarca);
                ordenarEmpleados(empleadosConMarca);
                ordenarEmpleados(empleadosSinMarca);
                DefaultListModel<Empleado> modelEmpleadosSinMarca = new DefaultListModel<>();
                for (Empleado empleado : empleadosSinMarca) {
                    modelEmpleadosSinMarca.addElement(empleado);
                }
                DefaultListModel<Object> modelEmpleadosConMarca = new DefaultListModel<>();
                for (Empleado empleado : empleadosConMarca) {
                    modelEmpleadosConMarca.addElement(empleado);
                }
                jLstEmpleadosRegistrados.setModel(modelEmpleadosConMarca);
                jLstEmpleadosFaltantes.setModel(modelEmpleadosSinMarca);

            }

            private List<Marca> filtrarDespues(List<Marca> marcas) {
                List<Marca> retorno = new ArrayList<>();
                for (Marca marca : marcas) {
                    Time filtro = new Time(
                            InputUtils.parseInt(jSpnHora.getValue()),
                            InputUtils.parseInt(jSpnMinuto.getValue()),
                            0);
                    if (marca.getHora().after(filtro)) {
                        retorno.add(marca);
                    }
                }
                return retorno;
            }

            private void ordenarMarcas(List<Marca> marcasFiltradas) {
                Collections.sort(marcasFiltradas, new Comparator<Marca>() {

                    @Override
                    public int compare(Marca o1, Marca o2) {
                        int compareTo = o1.getFecha().compareTo(o2.getFecha());
                        if (compareTo == 0) {
                            return o1.getHora().compareTo(o2.getHora());
                        } else {
                            return compareTo;
                        }
                    }
                });
            }

            private List<Marca> filtrar(List<Marca> marcas) {
                List<Marca> marcasFiltradas;
                if (jCheckFiltrar.isSelected()) {
                    marcasFiltradas = filtrarDespues(marcas);
                    marcasFiltradas.addAll(filtrarDespues(tableAdapter.getAllElements()));
                } else {
                    marcasFiltradas = marcas;
                }
                return marcasFiltradas;
            }

            private List<Empleado> empleadosConMarca() {
                List<Marca> marcas = tableAdapter.getAllElements();
                Set<Empleado> empleadosConMarca = new HashSet<>();
                for (Marca marca : marcas) {
                    if (!empleadosConMarca.contains(marca.getEmpleado().getLegajo())) {
                        empleadosConMarca.add(marca.getEmpleado());
                    }
                }
                return new ArrayList<>(empleadosConMarca);
            }

            private void ordenarEmpleados(List<Empleado> empleadosConMarca) {
                Collections.sort(empleadosConMarca, new Comparator<Empleado>() {

                    @Override
                    public int compare(Empleado o1, Empleado o2) {
                        return o1.getNombreApellido().compareTo(o2.getNombreApellido());
                    }
                });
            }
        });
        thread.start();
    }

    private static class MarcaWrapper extends WrapperNoEditable<Marca> {

        static final int COLUMNA_LEGAJO = 0, COLUMNA_APYNOM = 1, COLUMNA_FECHAHORA = 2;
        static final String[] COLUMNAS = "Legajo, Empleado, Fecha, Hora, Hora real ".split(", ");

        public MarcaWrapper(Marca elemento) {
            super(elemento);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                getElemento().getEmpleado().getLegajo(),
                getElemento().getEmpleado().getNombreApellido(),
                OutputUtils.formatFechaYHora(getElemento().getFecha().getTime()),
                OutputUtils.formatTime(getElemento().getHora()),
                OutputUtils.formatTime(getElemento().getHoraReal())
            });
        }
    }
}