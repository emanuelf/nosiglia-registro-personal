package nosigliaregistropersonal.vistas.otros;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.IngresoEgreso;
import nosigliaregistropersonal.modelo.Liquidacion;
import nosigliaregistropersonal.modelo.Marca;
import nosigliaregistropersonal.modelo.PeriodoTrabajo;
import nosigliaregistropersonal.util.StringUtils;
import nosigliaregistropersonal.vistas.abms.JPnlEdicionMarca;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.MultipleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;
import nosigliaregistropersonal.vistas.lib.otrosmodelos.CustomComboBoxModel;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

/**
 *
 * @author #emas
 */
public class JPnlHorasTrabajadasPorDia extends javax.swing.JPanel {

    MultipleSelectionTableAdapter<IngresoEgreso, IngresoEgresoWrapper> tableAdapter;
    private Liquidacion liquidacion;

    public JPnlHorasTrabajadasPorDia() {
        initComponents();
        if (!Beans.isDesignTime()) {
            crearYAsignarTableAdapter();
            agregarListeners();
            cargarCombos();
            agregarPopupMenu();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jBtnVerHorasTrabajadas = new JButton();
        jScrollPane1 = new JScrollPane();
        jXTblResultados = new JXTable();
        jXDPHasta = new JXDatePicker();
        jXDPDesde = new JXDatePicker();
        jLblDesde = new JLabel();
        jLblHasta = new JLabel();
        jLblEmpleado = new JLabel();
        jCmbEmpleados = new JComboBox();
        jXTSepResultados = new JXTitledSeparator();
        jLblHorasTrabajadas = new JLabel();
        jLbllHorasTrabajadasValor = new JLabel();
        jLblTotalHoras = new JLabel();
        jLblTotalALiquidarValor = new JLabel();
        jBtnLiquidar = new JButton();
        jLabel1 = new JLabel();
        jCmbTipoConsulta = new JComboBox();
        jLabel2 = new JLabel();
        jCmbPeriodoTrabajo = new JComboBox();
        jLblTotalHoras1 = new JLabel();
        jLblHorasExtrasValor = new JLabel();

        jBtnVerHorasTrabajadas.setText("Ver horas trabajadas ");

        jXTblResultados.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTblResultados);

        jXDPDesde.setToolTipText("");

        jLblDesde.setText("Desde");

        jLblHasta.setText("Hasta");

        jLblEmpleado.setText("Empleado");

        jXTSepResultados.setTitle("Resultados");

        jLblHorasTrabajadas.setFont(new Font("Tahoma", 0, 14)); // NOI18N
        jLblHorasTrabajadas.setText("Total horas:");

        jLbllHorasTrabajadasValor.setFont(new Font("Tahoma", 1, 18)); // NOI18N

        jLblTotalHoras.setFont(new Font("Tahoma", 0, 14)); // NOI18N
        jLblTotalHoras.setText("Total a liquidar:");

        jLblTotalALiquidarValor.setFont(new Font("Tahoma", 1, 18)); // NOI18N

        jBtnLiquidar.setText("Liquidar estas horas");

        jLabel1.setText("Tipo de consulta");

        jCmbTipoConsulta.setModel(new DefaultComboBoxModel(new String[] { "Consulta simple", "Consulta para liquidacion" }));
        jCmbTipoConsulta.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jCmbTipoConsultaActionPerformed(evt);
            }
        });

        jLabel2.setText("Periodo de trabajo");

        jLblTotalHoras1.setFont(new Font("Tahoma", 0, 14)); // NOI18N
        jLblTotalHoras1.setText("Horas extras:");

        jLblHorasExtrasValor.setFont(new Font("Tahoma", 1, 18)); // NOI18N

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jXTSepResultados, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, GroupLayout.Alignment.TRAILING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLblHorasTrabajadas)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLbllHorasTrabajadasValor, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(104, 104, 104)
                                .addComponent(jLblTotalALiquidarValor, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLblTotalHoras)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLblTotalHoras1)
                        .addGap(16, 16, 16)
                        .addComponent(jLblHorasExtrasValor, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnLiquidar))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLblEmpleado))
                            .addComponent(jCmbEmpleados, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jBtnVerHorasTrabajadas))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jCmbTipoConsulta, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jCmbPeriodoTrabajo, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(jLblDesde, GroupLayout.Alignment.LEADING)
                            .addComponent(jXDPDesde, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLblHasta)
                            .addComponent(jXDPHasta, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLblDesde)
                    .addComponent(jLblHasta)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(jXDPHasta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDPDesde, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCmbTipoConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCmbPeriodoTrabajo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblEmpleado)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(jBtnVerHorasTrabajadas)
                    .addComponent(jCmbEmpleados, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXTSepResultados, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jBtnLiquidar)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLblTotalHoras1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLblHorasExtrasValor, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLblHorasTrabajadas, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLbllHorasTrabajadasValor, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLblTotalHoras, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLblTotalALiquidarValor, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jCmbTipoConsultaActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jCmbTipoConsultaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCmbTipoConsultaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnLiquidar;
    private JButton jBtnVerHorasTrabajadas;
    private JComboBox jCmbEmpleados;
    private JComboBox jCmbPeriodoTrabajo;
    private JComboBox jCmbTipoConsulta;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLblDesde;
    private JLabel jLblEmpleado;
    private JLabel jLblHasta;
    private JLabel jLblHorasExtrasValor;
    private JLabel jLblHorasTrabajadas;
    private JLabel jLblTotalALiquidarValor;
    private JLabel jLblTotalHoras;
    private JLabel jLblTotalHoras1;
    private JLabel jLbllHorasTrabajadasValor;
    private JScrollPane jScrollPane1;
    private JXDatePicker jXDPDesde;
    private JXDatePicker jXDPHasta;
    private JXTitledSeparator jXTSepResultados;
    private JXTable jXTblResultados;
    // End of variables declaration//GEN-END:variables

    private void crearYAsignarTableAdapter() {
        CustomTableModel<IngresoEgresoWrapper> ctm = new CustomTableModel<>(IngresoEgresoWrapper.COLUMNS);
        tableAdapter = new MultipleSelectionTableAdapter<IngresoEgreso, IngresoEgresoWrapper>(jXTblResultados, ctm) {
            @Override
            public IngresoEgresoWrapper toWrapper(IngresoEgreso element) {
                return new IngresoEgresoWrapper(element);
            }

            @Override
            public IngresoEgreso fromWrapper(IngresoEgresoWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
        jXTblResultados.setHighlighters(new ColorHighlighter(new HighlightPredicate() {

            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                IngresoEgreso elementAt = tableAdapter.getElementAt(adapter.row);
                return elementAt.minutosDiferencia() > 720 || elementAt.isIngresoFueraDeHorario();
            }
        }, Color.RED, Color.BLACK));
        
    }

    private void agregarListeners() {
        agregarVerHorasTrabajadasActionListener();
        agregarLiquidarHorasActionListener();
        agregarPeriodoTrabajoActionListener();
        agregarTipoConsultaActionListener();
    }

    private Empleado empleadoSeleccionado() {
        return (Empleado) jCmbEmpleados.getSelectedItem();
    }

    private void cargarCombos() {
        cargarComboEmpleados();
        cargarComboTipoConsulta();
        cargarComboPeriodosTrabajo();
        if (isConsultaSimple()) {
            jCmbPeriodoTrabajo.setEnabled(false);
        }
    }

    private void agregarVerHorasTrabajadasActionListener() {
        jBtnVerHorasTrabajadas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isConsultaAvailable()) {
                    return;
                }
                PeriodoTrabajo periodo = cargarPeriodo();
                cargarTabla(periodo);
                liquidacion = new Liquidacion();
                liquidacion.setEmpleado(empleadoSeleccionado());
                liquidacion.setPrecioHora(empleadoSeleccionado().getPrecioHora());
                liquidacion.setIngresosEgresos(new HashSet<>(tableAdapter.getAllElements()));
                jLblTotalALiquidarValor.setText(OutputUtils.formatCurrency(new BigDecimal(liquidacion.totalLiquidacion())));
                jLbllHorasTrabajadasValor.setText(OutputUtils.formatHorasMinutos(liquidacion.minutosTrabajados()));
                if (consultaParaLiquidacion()) {
                    Integer horasLaborales = periodoTrabajoSeleccionado().getHorasLaborales();
                    int horasTrabajadas = liquidacion.minutosTrabajados() / 60;
                    final int HORAS_EXTRAS = horasTrabajadas - horasLaborales;
                    if (HORAS_EXTRAS > 0) {
                        liquidacion.setHorasExtras(HORAS_EXTRAS);
                    }
                    jLblHorasExtrasValor.setText(Integer.toString(liquidacion.getHorasExtras()));
                }
                habilitarLiquidacionSegunCorresponda();
            }

            private void cargarTabla(PeriodoTrabajo periodo) {
                List<Marca> marcasOrdenadasPorFechaYHora = Marca.
                        buscarMarcasNoLiquidadasEntre(
                            periodo.getInicio().getTime(),
                            periodo.getFin().getTime(),
                            empleadoSeleccionado());
                List<IngresoEgreso> ingresosEgresos = new ArrayList<>();

                for (int i = 0; i < marcasOrdenadasPorFechaYHora.size(); i++) {
                    Marca marca = marcasOrdenadasPorFechaYHora.get(i);
                    final Marca ingreso = marca;
                    try {
                        final Marca egreso = marcasOrdenadasPorFechaYHora.get(i + 1);
                        ingresosEgresos.add(new IngresoEgreso(ingreso, egreso));
                        i = i + 1;
                    } catch (IndexOutOfBoundsException ex) {
                        // no hay egreso aun
                    }
                }
                tableAdapter.setElements(ingresosEgresos);
            }

            private boolean isConsultaAvailable() {
                if (jXDPDesde.getDate() == null || jXDPHasta.getDate() == null) {
                    MessageUtils.showWarning(null, "Ingrese las fechas desde y hasta");
                    return false;
                }
                if (empleadoSeleccionado() == null) {
                    MessageUtils.showInfo(null, "Debe seleccionar un empleado");
                    return false;
                }
                return true;
            }

            private PeriodoTrabajo cargarPeriodo() {
                PeriodoTrabajo periodo = null;
                if (consultaParaLiquidacion()) {
                    periodo = periodoTrabajoSeleccionado();
                } else {
                    periodo = new PeriodoTrabajo();
                    periodo.setInicio(jXDPDesde.getDate());
                    periodo.setFin(jXDPHasta.getDate());
                }
                return periodo;
            }

            private boolean consultaParaLiquidacion() {
                return TIPO_CONSULTA.PARA_LIQUIDACION.equals(getTipoConsultaSeleccionada());
            }
        });
    }

    private void habilitarLiquidacionSegunCorresponda() {
        if (isConsultaSimple()) {
            jBtnLiquidar.setEnabled(false);
        } else {
            if (!tableAdapter.isEmpty()) {
                jBtnLiquidar.setEnabled(true);
            }
        }
    }

    private boolean isConsultaSimple() {
        if (TIPO_CONSULTA.CONSULTA_SIMPLE.equals(getTipoConsultaSeleccionada())) {
            return true;
        } else {
            return false;
        }
    }

    private TIPO_CONSULTA getTipoConsultaSeleccionada() {
        return (TIPO_CONSULTA) jCmbTipoConsulta.getSelectedItem();
    }

    private PeriodoTrabajo periodoTrabajoSeleccionado() {
        return (PeriodoTrabajo) jCmbPeriodoTrabajo.getSelectedItem();
    }

    private void agregarLiquidarHorasActionListener() {
        jBtnLiquidar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter.isEmpty()) {
                    MessageUtils.showWarning(null, "Nada que liquidar");
                    return;
                }
                if (MessageUtils.areYouSure("Esta seguro de generar la liquidacion para \n este empleado por el periodo requerido?")) {
                    liquidacion.setPeriodoTrabajo(periodoTrabajoSeleccionado());
                    Liquidacion.guardar(liquidacion);
                    tableAdapter.clear();
                }
            }
        });
    }

    private void cargarComboEmpleados() {
        DefaultComboBoxModel<Empleado> defaultComboBoxModel = new DefaultComboBoxModel<>();
        for (Empleado empleado : Empleado.findAll()) {
            defaultComboBoxModel.addElement(empleado);
        }
        jCmbEmpleados.setModel(defaultComboBoxModel);
    }

    private void agregarPeriodoTrabajoActionListener() {
        jCmbPeriodoTrabajo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PeriodoTrabajo periodoTrabajoSeleccionado = periodoTrabajoSeleccionado();
                jXDPDesde.setDate(periodoTrabajoSeleccionado.getInicio().getTime());
                jXDPHasta.setDate(periodoTrabajoSeleccionado.getFin().getTime());
            }
        });
    }

    private void cargarComboTipoConsulta() {
        jCmbTipoConsulta.setModel(new CustomComboBoxModel<>(TIPO_CONSULTA.values()));
    }

    private void agregarTipoConsultaActionListener() {
        jCmbTipoConsulta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isConsultaSimple()) {
                    jCmbPeriodoTrabajo.setEnabled(false);
                } else {
                    jCmbPeriodoTrabajo.setEnabled(true);
                    cargarComboPeriodosTrabajo();
                }
            }
        });
    }
    
    private void cargarComboPeriodosTrabajo() {
        jCmbPeriodoTrabajo.setModel(new CustomComboBoxModel<>(PeriodoTrabajo.all()));
    }

    private void agregarPopupMenu() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        JMenuItem jMnuItemAgregar = new JMenuItem("Agregar marca", Iconos.AGREGAR_16X16);
        jMnuItemAgregar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                final JPnlEdicionMarca jPnlEdicioMarca = new JPnlEdicionMarca();
                jPnlEdicioMarca.setEmpleado(empleadoSeleccionado());
                PanelsUtil.showPanelOnModalDialog(
                            SwingUtilities.getWindowAncestor(jLabel1), 
                            jPnlEdicioMarca, 
                            "Nueva marca");
            }
        });
        jPopupMenu.add(jMnuItemAgregar);
        jXTblResultados.setComponentPopupMenu(jPopupMenu);
    }

    private static class IngresoEgresoWrapper extends WrapperNoEditable<IngresoEgreso> {

        static final String[] COLUMNS = "Ingreso, Egreso, Horas trabajadas".split(", ");

        public IngresoEgresoWrapper(IngresoEgreso elemento) {
            super(elemento);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                OutputUtils.formatFechaYHora(getElemento().getIngreso().fechaYHora()),
                OutputUtils.formatFechaYHora(getElemento().getEgreso() == null ? null : getElemento().getEgreso().fechaYHora()),
                getElemento().horasTrabajadas()
            });
        }
    }

    private enum TIPO_CONSULTA {

        CONSULTA_SIMPLE,
        PARA_LIQUIDACION;

        @Override
        public String toString() {
            return StringUtils.capitalizeFirstLetter(name().toLowerCase()).replace("_", " ");
        }
    }
}