package nosigliaregistropersonal.vistas.abms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.FormaPago;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;

public class JPnlEdicionFormaPago extends javax.swing.JPanel {

    private FormaPago formaPago;

    public JPnlEdicionFormaPago() {
        initComponents();
        if (!Beans.isDesignTime()) {
            crearYAsignarActions();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLblLegajo = new javax.swing.JLabel();
        jTxtCodigo = new javax.swing.JTextField();
        jLblApynom = new javax.swing.JLabel();
        jTxtNombre = new javax.swing.JTextField();
        jBtnCancelar = new javax.swing.JButton();
        jBtnGuardar = new javax.swing.JButton();

        jLblLegajo.setText("Codigo");

        jLblApynom.setText("Nombre");

        jBtnCancelar.setText("Cancelar");

        jBtnGuardar.setText("Guardar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblLegajo)
                    .addComponent(jTxtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLblApynom)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jBtnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnGuardar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLblLegajo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblApynom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnGuardar)
                    .addComponent(jBtnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCancelar;
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JLabel jLblApynom;
    private javax.swing.JLabel jLblLegajo;
    private javax.swing.JTextField jTxtCodigo;
    private javax.swing.JTextField jTxtNombre;
    // End of variables declaration//GEN-END:variables

    void setFormaPago(FormaPago formaPago) {
        if (formaPago == null) {
            this.formaPago = new FormaPago();
        } else {
            this.formaPago = formaPago;
            jTxtCodigo.setText(formaPago.getCodigo().toString());
            jTxtNombre.setText(formaPago.getNombre());

        }
    }

    private void crearYAsignarActions() {
        crearYAsignarGuardarAction();
        crearYAsignarCancelarAction();

    }

    private void crearYAsignarGuardarAction() {
        jBtnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarDatosDesdeVista();
                if (isFormValid()) {
                    cargarDatosDesdeVista();
                    formaPago.guardar();
                    cerrarVentana();

                } else {
                    MessageUtils.showWarning(null, "Algunos de los datos son incorrectos");
                }
            }

            private void cargarDatosDesdeVista() {
                formaPago.setCodigo(InputUtils.parseInt(jTxtCodigo.getText()));
                formaPago.setNombre(jTxtNombre.getText());
            }
        });
    }

    private boolean isFormValid() {
        return !jTxtCodigo.getText().isEmpty() && !jTxtNombre.getText().isEmpty();
    }

    private void crearYAsignarCancelarAction() {
        jBtnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cerrarVentana();
            }
        });
    }

    private void cerrarVentana() {
        SwingUtilities.getWindowAncestor(jLblApynom).dispose();
    }
}
