package nosigliaregistropersonal.vistas.abms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.sql.Time;
import java.util.GregorianCalendar;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.Marca;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.Util;

public class JPnlEdicionMarca extends javax.swing.JPanel {

    private Empleado empleado;
    private Marca marca;

    public JPnlEdicionMarca() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
        }
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
        jLblEmpleado.setText(empleado.getNombreApellido());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jXDPFecha = new org.jdesktop.swingx.JXDatePicker();
        jLabel2 = new javax.swing.JLabel();
        jTxtHora = new javax.swing.JTextField();
        jTxtMinuto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLblEmpleado = new javax.swing.JLabel();
        jBtnGuardar = new javax.swing.JButton();
        jBtnCancelar = new javax.swing.JButton();

        jLabel1.setText("Fecha");

        jLabel2.setText("Hora");

        jTxtHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtHoraActionPerformed(evt);
            }
        });

        jTxtMinuto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtMinutoActionPerformed(evt);
            }
        });

        jLabel3.setText(":");

        jLabel4.setText("Empleado");

        jBtnGuardar.setText("Guardar");

        jBtnCancelar.setText("Cancelar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXDPFecha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTxtHora, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTxtMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel4))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLblEmpleado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 143, Short.MAX_VALUE)
                        .addComponent(jBtnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtnGuardar)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXDPFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtMinuto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLblEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnGuardar)
                    .addComponent(jBtnCancelar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTxtHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtHoraActionPerformed
    }//GEN-LAST:event_jTxtHoraActionPerformed

    private void jTxtMinutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtMinutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtMinutoActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCancelar;
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLblEmpleado;
    private javax.swing.JTextField jTxtHora;
    private javax.swing.JTextField jTxtMinuto;
    private org.jdesktop.swingx.JXDatePicker jXDPFecha;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarCancelarAction();
        agregarGuardarAction();
    }

    private void agregarCancelarAction() {
        jBtnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cerrarPanel();
            }
        });
    }

    private void cerrarPanel() {
        SwingUtilities.getWindowAncestor(jLabel1).dispose();
    }

    private void agregarGuardarAction() {
        jBtnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isValidForm()) {
                    if (marca == null) {
                        marca = new Marca();
                        marca.setEmpleado(empleado);
                    }
                    GregorianCalendar fecha = new GregorianCalendar();
                    fecha.setTime(jXDPFecha.getDate());
                    marca.setFecha(fecha);
                    
                    marca.setHora(new Time(
                            InputUtils.parseInt(jTxtHora.getText()),
                            InputUtils.parseInt(jTxtMinuto.getText()),
                            0));
                    marca.guardar();
                    cerrarPanel();                    
                } else {
                    MessageUtils.showWarning(null, "Faltan datos o son erroneos");
                }
            }
        });
    }
    
    private boolean isValidForm() {
        return  jXDPFecha.getDate() != null && 
                !jTxtHora.getText().isEmpty() && 
                !jTxtMinuto.getText().isEmpty() && 
                empleado != null;
    }

    public void setMarca(Marca marca) {
        if (marca == null) {
            limpiar();
            marca = new Marca();
        } else {
            this.marca = marca;
            this.empleado = marca.getEmpleado();
            mostrar();
        }
    }

    private void mostrar() {
        jTxtHora.setText(Integer.toString(marca.getHora().getHours()));
        jTxtMinuto.setText(Integer.toString(marca.getHora().getMinutes()));
        jLblEmpleado.setText(marca.getEmpleado().getNombreApellido());
        jXDPFecha.setDate(marca.getFecha().getTime());
    }

    private void limpiar() {
        Util.limpiarComponentes(jTxtHora, jTxtMinuto, jLblEmpleado);
    }
}
