package nosigliaregistropersonal.vistas.abms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.PeriodoTrabajo;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.Util;
import org.hibernate.metamodel.ValidationException;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Emanuel
 */
public class JPnlEdicionPeriodoTrabajo extends javax.swing.JPanel {

    private PeriodoTrabajo periodoTrabajo;

    public JPnlEdicionPeriodoTrabajo() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTxtHorasLaborales = new JTextField();
        jBtnCancelar = new JButton();
        jLblJubilacion = new JLabel();
        jBtnGuardar = new JButton();
        jLblPrecioHora = new JLabel();
        jLblLegajo = new JLabel();
        jTxtDescripcion = new JTextField();
        jLblApynom = new JLabel();
        jXDPFechaInicio = new JXDatePicker();
        jXDPFechaFin = new JXDatePicker();

        jBtnCancelar.setText("Cancelar");

        jLblJubilacion.setText("Horas laborales");

        jBtnGuardar.setText("Guardar");

        jLblPrecioHora.setText("Fecha fin");

        jLblLegajo.setText("Descripcion");

        jLblApynom.setText("Fecha inicio");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBtnCancelar)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBtnGuardar)
                .addGap(10, 10, 10))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtHorasLaborales, GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                    .addComponent(jTxtDescripcion, GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                    .addComponent(jXDPFechaInicio, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLblApynom)
                            .addComponent(jLblLegajo)
                            .addComponent(jLblPrecioHora)
                            .addComponent(jLblJubilacion))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jXDPFechaFin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLblLegajo)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtDescripcion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblApynom)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXDPFechaInicio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jLblPrecioHora)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXDPFechaFin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblJubilacion)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtHorasLaborales, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnGuardar)
                    .addComponent(jBtnCancelar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnCancelar;
    private JButton jBtnGuardar;
    private JLabel jLblApynom;
    private JLabel jLblJubilacion;
    private JLabel jLblLegajo;
    private JLabel jLblPrecioHora;
    private JTextField jTxtDescripcion;
    private JTextField jTxtHorasLaborales;
    private JXDatePicker jXDPFechaFin;
    private JXDatePicker jXDPFechaInicio;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarGuardarAction();
        agregarCancelarAction();
        InputUtils.addIntegerListeners(jTxtHorasLaborales, false);

    }

    public void setPeriodo(PeriodoTrabajo periodoTrabajo) {
        if (periodoTrabajo == null) {
            this.periodoTrabajo = new PeriodoTrabajo();
            limpiar();
        } else {
            this.periodoTrabajo = periodoTrabajo;
            jTxtDescripcion.setText(periodoTrabajo.getDescripcion());
            jTxtHorasLaborales.setText(periodoTrabajo.getHorasLaborales().toString());
            if (periodoTrabajo.getInicio() != null){
                jXDPFechaInicio.setDate(periodoTrabajo.getInicio().getTime());
            }
            if (periodoTrabajo.getFin() != null){
                jXDPFechaFin.setDate(periodoTrabajo.getFin().getTime());
            }
        }
    }

    private void agregarGuardarAction() {
        jBtnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isValidForm()) {
                    MessageUtils.showWarning(null, "Ingrese TODOS LOS DATOS");
                    return;
                }
                try {
                    periodoTrabajo.setDescripcion(jTxtDescripcion.getText());
                    periodoTrabajo.setInicio(jXDPFechaInicio.getDate());
                    periodoTrabajo.setFin(jXDPFechaFin.getDate());
                    periodoTrabajo.setHorasLaborales(InputUtils.parseInt(jTxtHorasLaborales.getText()));
                    periodoTrabajo.save();
                    dispose();
                } catch (ValidationException ex) {
                    MessageUtils.showError(JPnlEdicionPeriodoTrabajo.this, ex.getMessage());
                }
            }
        });
    }

    private void dispose() {
        SwingUtilities.getWindowAncestor(jLblApynom).dispose();
    }

    private boolean isValidForm() {
        return !jTxtDescripcion.getText().isEmpty()
                && !jTxtHorasLaborales.getText().isEmpty()
                && jXDPFechaFin.getDate() != null
                && jXDPFechaInicio.getDate() != null;
    }

    private void agregarCancelarAction() {
        jBtnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void limpiar() {
        Util.limpiar(jTxtDescripcion, jTxtDescripcion);
        Util.limpiarComponentes(jXDPFechaFin, jXDPFechaInicio);
    }
}
