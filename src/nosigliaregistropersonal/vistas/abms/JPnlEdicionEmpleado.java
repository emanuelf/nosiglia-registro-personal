package nosigliaregistropersonal.vistas.abms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.math.BigDecimal;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import org.hibernate.metamodel.ValidationException;

/**
 *
 * @author Emanuel
 */
public class JPnlEdicionEmpleado extends javax.swing.JPanel {

    private Empleado empleado;

    public JPnlEdicionEmpleado() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
        }
    }

    public void setEmpleado(Empleado empleado) {
        if (empleado == null) {
            this.empleado = new Empleado();
            limpiar();
        } else {
            this.empleado = empleado;
            jTxtApellidoNombre.setText(empleado.getNombreApellido());
            jTxtLegajo.setText(empleado.getLegajo().toString());
            jTxtPrecioHora.setText(OutputUtils.formatCurrency(new BigDecimal(empleado.getPrecioHora())));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLblLegajo = new javax.swing.JLabel();
        jTxtLegajo = new javax.swing.JTextField();
        jLblApynom = new javax.swing.JLabel();
        jTxtApellidoNombre = new javax.swing.JTextField();
        jLblPrecioHora = new javax.swing.JLabel();
        jTxtPrecioHora = new javax.swing.JTextField();
        jBtnGuardar = new javax.swing.JButton();
        jBtnCancelar = new javax.swing.JButton();
        jLblJubilacion = new javax.swing.JLabel();
        jTxtJubilacion = new javax.swing.JTextField();
        jLblRetenciones = new javax.swing.JLabel();
        jTxtRetenciones = new javax.swing.JTextField();

        jLblLegajo.setText("Legajo");

        jLblApynom.setText("Apellido y nombre");

        jLblPrecioHora.setText("Precio/Hora");

        jBtnGuardar.setText("Guardar");

        jBtnCancelar.setText("Cancelar");

        jLblJubilacion.setText("Jubilacion");

        jLblRetenciones.setText("Retenciones");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblLegajo)
                            .addComponent(jTxtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLblJubilacion)
                            .addComponent(jTxtJubilacion, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLblApynom)
                                .addComponent(jTxtApellidoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLblPrecioHora)
                                .addComponent(jTxtPrecioHora, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jBtnCancelar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnGuardar))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLblRetenciones)
                                .addComponent(jTxtRetenciones, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLblLegajo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblApynom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtApellidoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblPrecioHora)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtPrecioHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLblJubilacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtJubilacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLblRetenciones)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTxtRetenciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnGuardar)
                    .addComponent(jBtnCancelar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCancelar;
    private javax.swing.JButton jBtnGuardar;
    private javax.swing.JLabel jLblApynom;
    private javax.swing.JLabel jLblJubilacion;
    private javax.swing.JLabel jLblLegajo;
    private javax.swing.JLabel jLblPrecioHora;
    private javax.swing.JLabel jLblRetenciones;
    private javax.swing.JTextField jTxtApellidoNombre;
    private javax.swing.JTextField jTxtJubilacion;
    private javax.swing.JTextField jTxtLegajo;
    private javax.swing.JTextField jTxtPrecioHora;
    private javax.swing.JTextField jTxtRetenciones;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarGuardarAction();
        agregarCancelarAction();
        InputUtils.addCurrencyListeners(jTxtPrecioHora, false);
        InputUtils.addIntegerListeners(jTxtLegajo, false);
    }

    private void agregarGuardarAction() {
        jBtnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTxtLegajo.getText().isEmpty() || jTxtApellidoNombre.getText().isEmpty() || jTxtPrecioHora.getText().isEmpty()){
                    MessageUtils.showWarning(null, "Ingrese TODOS LOS DATOS");
                    return;
                }
                empleado.setLegajo(InputUtils.parseLong(jTxtLegajo.getText()));
                empleado.setNombreApellido(jTxtApellidoNombre.getText());
                empleado.setPrecioHora(InputUtils.parseCurrency(jTxtPrecioHora.getText()).doubleValue());
                empleado.setJubilacion(InputUtils.parseCurrency(jTxtJubilacion.getText()).doubleValue());
                empleado.setRetenciones(InputUtils.parseCurrency(jTxtRetenciones.getText()).doubleValue());
                try {
                    empleado.save();
                    SwingUtilities.getWindowAncestor(jLblApynom).dispose();
                } catch (ValidationException ex) {
                    MessageUtils.showError(jLblApynom, ex.getMessage());
                }
            }
        });
    }

    private void agregarCancelarAction() {
        jBtnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.getWindowAncestor(jLblApynom).dispose();
            }
        });
    }

    private void limpiar() {
        jTxtApellidoNombre.setText("");
        jTxtLegajo.setText("");
        jTxtPrecioHora.setText("");
        jTxtJubilacion.setText("");
        jTxtRetenciones.setText("");
    }
}
