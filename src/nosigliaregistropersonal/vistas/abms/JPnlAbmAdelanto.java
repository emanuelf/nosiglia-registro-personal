package nosigliaregistropersonal.vistas.abms;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.beans.Beans;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.Adelanto;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.FormaPago;
import nosigliaregistropersonal.modelo.Pago;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.util.Impresor;
import nosigliaregistropersonal.vistas.abms.buscadores.BuscadorAdelanto;
import nosigliaregistropersonal.vistas.abms.buscadores.BuscadorEmpleado;
import nosigliaregistropersonal.vistas.framework.Buscador;
import nosigliaregistropersonal.vistas.framework.JPnlBotoneraAbm;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.Util;
import nosigliaregistropersonal.vistas.lib.abms.BotoneraAbm;
import nosigliaregistropersonal.vistas.lib.abms.ControladorAbm;
import nosigliaregistropersonal.vistas.lib.abms.Detalle;
import nosigliaregistropersonal.vistas.lib.abms.JPnlAbm;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;

/**
 *
 * @author Emanuel
 */
public class JPnlAbmAdelanto extends JPnlAbm<Adelanto> {

    private SingleSelectionTableAdapter<Pago, PagoWrapper> tableAdapter;
    private BuscadorAdelanto buscadorAdelanto;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new JLabel();
        jTxtNroAdelanto = new JTextField();
        jLabel2 = new JLabel();
        jXDPFecha = new JXDatePicker();
        jPanel1 = new JPanel();
        jLabel3 = new JLabel();
        jTxtLegajoEmpleado = new JTextField();
        jTxtNombreApellido = new JTextField();
        jLabel4 = new JLabel();
        jPanel2 = new JPanel();
        jScrollPane1 = new JScrollPane();
        jXTblAdelantosPagos = new JXTable();
        jLabel5 = new JLabel();
        jLblTotal = new JLabel();
        jPnlBotoneraAbm = new JPnlBotoneraAbm();
        jLabel6 = new JLabel();
        jScrollPane2 = new JScrollPane();
        jTxtAreaObservaciones = new JTextArea();

        jLabel1.setText("Nro adelanto");

        jTxtNroAdelanto.setEditable(false);

        jLabel2.setText("Fecha");

        jPanel1.setBorder(BorderFactory.createTitledBorder("Empleado"));

        jLabel3.setText("Legajo");

        jLabel4.setText("Nombre y apellido");

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtLegajoEmpleado, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTxtNombreApellido))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtLegajoEmpleado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtNombreApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(BorderFactory.createTitledBorder("Conceptos"));

        jXTblAdelantosPagos.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTblAdelantosPagos);

        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 631, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel5.setText("Total: ");

        jLblTotal.setFont(new Font("Tahoma", 1, 18)); // NOI18N
        jLblTotal.setHorizontalAlignment(SwingConstants.RIGHT);

        jLabel6.setText("Observaciones");

        jTxtAreaObservaciones.setColumns(20);
        jTxtAreaObservaciones.setRows(5);
        jScrollPane2.setViewportView(jTxtAreaObservaciones);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPnlBotoneraAbm, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jTxtNroAdelanto, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jXDPFecha, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jScrollPane2))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLblTotal, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtNroAdelanto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDPFecha, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLblTotal, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jPnlBotoneraAbm, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLblTotal;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPnlBotoneraAbm jPnlBotoneraAbm;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JTextArea jTxtAreaObservaciones;
    private JTextField jTxtLegajoEmpleado;
    private JTextField jTxtNombreApellido;
    private JTextField jTxtNroAdelanto;
    private JXDatePicker jXDPFecha;
    private JXTable jXTblAdelantosPagos;
    // End of variables declaration//GEN-END:variables
    private Empleado empleadoSeleccionado;
    private BuscadorEmpleado buscadorEmpleado;

    public JPnlAbmAdelanto() {
        super();
        initComponents();
        if (!Beans.isDesignTime()) {
            configurarInputListeners();
            configurarActions();
            modoInicial();
            configurarTabla();
            buscadorAdelanto = new BuscadorAdelanto(this);
            buscadorEmpleado = new BuscadorEmpleado(this);
        }
    }

    @Override
    protected void configurarActions() {
        super.configurarActions();
        configurarBuscarEmpleadoPorLegajoAction();
        configurarBuscarEmpleadoPorNombreYApellidoAction();
        configurarJXTableActions();
    }

    private void configurarTabla() {
        CustomTableModel<PagoWrapper> ctm = new CustomTableModel<>(COLUMNAS);
        tableAdapter = new SingleSelectionTableAdapter<Pago, PagoWrapper>(jXTblAdelantosPagos, ctm) {
            @Override
            public PagoWrapper toWrapper(Pago element) {
                return new PagoWrapper(element);
            }

            @Override
            public Pago fromWrapper(PagoWrapper wrapper) {
                return wrapper.pago;
            }
        };
        cargarFormasPago();
    }
    
    
    @Override
    protected Buscador<Adelanto> getBuscador() {
        return buscadorAdelanto;
    }

    @Override
    protected ControladorAbm<Adelanto> getControladorAbm() {
        return new ControladorAbmPago();
    }

    @Override
    protected Detalle<Adelanto> getDetalle() {
        return new DetallePago();
    }

    @Override
    protected BotoneraAbm getBotonera() {
        return jPnlBotoneraAbm;
    }

    @Override
    protected boolean isValid(Adelanto object) {
        return object.getFecha() != null && object.getEmpleado() != null && !object.getPagos().isEmpty();
    }

    private void configurarBuscarEmpleadoPorLegajoAction() {
        jTxtLegajoEmpleado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Empleado aux = Empleado.findByLegajo(InputUtils.parseLong(jTxtLegajoEmpleado.getText()));
                if (aux == null) {
                    if (empleadoSeleccionado == null) {
                        Util.limpiar(jTxtLegajoEmpleado, jTxtNombreApellido);
                    } else {
                        mostrarEmpleado();
                    }
                } else {
                    empleadoSeleccionado = aux;
                    mostrarEmpleado();
                }
            }
        });
    }

    private void mostrarEmpleado() {
        jTxtLegajoEmpleado.setText(Long.toString(empleadoSeleccionado.getLegajo()));
        jTxtNombreApellido.setText(empleadoSeleccionado.getNombreApellido());
    }

    private void configurarBuscarEmpleadoPorNombreYApellidoAction() {
        jTxtNombreApellido.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                empleadoSeleccionado = buscadorEmpleado.buscarEnPanel(jTxtNombreApellido.getText());
                mostrarEmpleado();
            }
        });
    }

    private void configurarInputListeners() {
        InputUtils.addIntegerListeners(jTxtLegajoEmpleado, false);
    }

    private void configurarJXTableActions() {
        configurarDeleteAndAddActions();
    }

    private void calcularTotal() {
        double total = 0;
        for (Pago pago : tableAdapter.getAllElements()) {
            total += pago.getMonto();
        }
        jLblTotal.setText(OutputUtils.formatCurrency(new BigDecimal(total)));
    }

    private void configurarDeleteAndAddActions() {
        jXTblAdelantosPagos.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                manipularDelete(e);
                manipularInsert(e);
            }

            private void manipularDelete(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    tableAdapter.removeSelectedElement();
                    calcularTotal();
                }
            }

            private void manipularInsert(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_INSERT) {
                    tableAdapter.addElement(new Pago());
                }
            }
        });
        final JPopupMenu jPopupMenu = new JPopupMenu();
        jPopupMenu.add(new JMenuItem(new AbstractAction("agregar", Iconos.AGREGAR_ABM) {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableAdapter.addElement(new Pago());
            }
        }));
        jPopupMenu.add(new JMenuItem(new AbstractAction("borrar", Iconos.ELIMINAR_ABM) {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableAdapter.removeSelectedElement();
                calcularTotal();
            }
        }));
        jXTblAdelantosPagos.setComponentPopupMenu(jPopupMenu);
    }
    final String[] COLUMNAS = "Forma pago, Monto".split(", ");
    final int COLUMNA_FORMA_PAGO = 0,
            COLUMNA_MONTO = 1;

    private void cargarFormasPago() {
        List<FormaPago> all = FormaPago.all();
        ComboBoxCellEditor comboBoxCellEditor = new ComboBoxCellEditor(new JComboBox(all.toArray()));
        jXTblAdelantosPagos.getColumn(COLUMNA_FORMA_PAGO).setCellEditor(comboBoxCellEditor);
    }

    @Override
    protected void nuevo() {
        super.nuevo(); 
        cargarFormasPago();
    }

    private class PagoWrapper implements Vectorizable {

        private Pago pago;

        public PagoWrapper(Pago pago) {
            this.pago = pago;
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                pago.getFormaPago(),
                pago.getMonto()
            });
        }

        @Override
        public void setValueAt(int column, Object aValue) {
            switch (column) {
                case COLUMNA_FORMA_PAGO:
                    pago.setFormaPago((FormaPago) aValue);
                    break;
                case COLUMNA_MONTO:
                    pago.setMonto(InputUtils.parseCurrency(aValue).doubleValue());
                    calcularTotal();
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        public boolean isCellEditable(int column) {
            return true;
        }
    }

    private class ControladorAbmPago implements ControladorAbm<Adelanto> {

        public ControladorAbmPago() {
        }

        @Override
        public void anular(Adelanto element) {
        }

        @Override
        public Adelanto crear() {
            return new Adelanto();
        }

        @Override
        public void eliminar(Adelanto element) {
            Service.delete(element);
        }

        @Override
        public void guardar(final Adelanto element) {
            Adelanto.guardar(element);
        }

        @Override
        public void imprimir(Adelanto element) throws PrinterException {
            Impresor.imprimir(element);
        }

        @Override
        public void vistaPreliminar(Adelanto element) throws PrinterException {
        }

        @Override
        public Adelanto reload(Adelanto element) {
            if (element == null) {
                return null;
            }
            return Service.load(Adelanto.class, element.getId());
        }

        @Override
        public Object getId(Adelanto element) {
            return element.getId();
        }
    }

    private class DetallePago implements Detalle<Adelanto> {

        public DetallePago() {
        }

        @Override
        public void mostrarDatos(Adelanto element) {
            jTxtLegajoEmpleado.setText(element.getEmpleado().getLegajo().toString());
            jTxtNombreApellido.setText(element.getEmpleado().getNombreApellido());
            jTxtNroAdelanto.setText(element.getId().toString());
            jXDPFecha.setDate(element.getFecha().getTime());
            tableAdapter.setElements(element.getPagos());
            jTxtAreaObservaciones.setText(element.getObservaciones());
        }

        @Override
        public void guardarDatos(Adelanto element) {
            element.setEmpleado(empleadoSeleccionado);
            final Date date = jXDPFecha.getDate();
            if (date != null) {
                Calendar fecha = new GregorianCalendar();
                fecha.setTime(date);
                element.setFecha(fecha);
            }
            element.setObservaciones(jTxtAreaObservaciones.getText());
            element.setPagos(new HashSet<>(tableAdapter.getAllElements()));
        }

        @Override
        public void edicion() {
            Util.setEnabled(
                    true, 
                    jTxtLegajoEmpleado, 
                    jTxtNombreApellido, 
                    jTxtNroAdelanto, 
                    jXTblAdelantosPagos, 
                    jXDPFecha, 
                    jTxtAreaObservaciones);
        }

        @Override
        public void visualizacion() {
            Util.setEnabled(
                    false, 
                    jTxtLegajoEmpleado, 
                    jTxtNombreApellido, 
                    jTxtNroAdelanto, 
                    jXTblAdelantosPagos, 
                    jXDPFecha, 
                    jTxtAreaObservaciones);
        }

        @Override
        public void limpiar() {
            Util.limpiarComponentes(
                    jTxtLegajoEmpleado, 
                    jTxtNombreApellido, 
                    jTxtNroAdelanto, 
                    jXDPFecha);
            tableAdapter.removeAllElements();
        }
    }
}
