package nosigliaregistropersonal.vistas.abms;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.beans.Beans;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.Adelanto;
import nosigliaregistropersonal.modelo.Ajuste;
import nosigliaregistropersonal.modelo.DescuentoAdelanto;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.FormaPago;
import nosigliaregistropersonal.modelo.Liquidacion;
import nosigliaregistropersonal.modelo.OrdenDePago;
import nosigliaregistropersonal.modelo.Pago;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.util.Impresor;
import nosigliaregistropersonal.vistas.abms.buscadores.BuscadorEmpleado;
import nosigliaregistropersonal.vistas.abms.buscadores.BuscadorOrdenDePago;
import nosigliaregistropersonal.vistas.framework.Buscador;
import nosigliaregistropersonal.vistas.framework.JPnlBotoneraAbm;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.Util;
import nosigliaregistropersonal.vistas.lib.abms.BotoneraAbm;
import nosigliaregistropersonal.vistas.lib.abms.ControladorAbm;
import nosigliaregistropersonal.vistas.lib.abms.Detalle;
import nosigliaregistropersonal.vistas.lib.abms.JPnlAbm;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.MultipleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

/**
 *
 * @author Emanuel
 */
public class JPnlAbmOrdenDePago extends JPnlAbm<OrdenDePago> {

    public JPnlAbmOrdenDePago() {
        initComponents();
        if (!Beans.isDesignTime()) {
            configurarTableAdapters();
            configurarActions();
            modoInicial();
            configurarBuscarEmpleadoPorLegajoAction();
            configurarBuscarEmpleadoPorNombreYApellidoAction();
        }
    }
    private MultipleSelectionTableAdapter<Liquidacion, LiquidacionWrapper> tableAdapterLiquidaciones;
    private MultipleSelectionTableAdapter<DescuentoAdelanto, DescuentoAdelantoWrapper> tableAdapterDescuentoAdelanto;
    private MultipleSelectionTableAdapter<Pago, PagoWrapper> tableAdapterPago;
    private MultipleSelectionTableAdapter<Ajuste, AjusteWrapper> tableAdapterAjustes;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlBotoneraAbm = new JPnlBotoneraAbm();
        jLabel1 = new JLabel();
        jTxtNroAdelanto = new JTextField();
        jLabel2 = new JLabel();
        jXDPFecha = new JXDatePicker();
        jPanel1 = new JPanel();
        jLabel3 = new JLabel();
        jTxtLegajoEmpleado = new JTextField();
        jTxtNombreApellido = new JTextField();
        jLabel4 = new JLabel();
        jTabbedPane1 = new JTabbedPane();
        jScrollPane1 = new JScrollPane();
        jXTblLiquidaciones = new JXTable();
        jScrollPane2 = new JScrollPane();
        jXTblDescuentosAdelantos = new JXTable();
        jScrollPane4 = new JScrollPane();
        jXTblAjustes = new JXTable();
        jScrollPane3 = new JScrollPane();
        jXTblFormasDePago = new JXTable();
        jPanel2 = new JPanel();
        jTxtJubilacion = new JTextField();
        jLabel6 = new JLabel();
        jLabel8 = new JLabel();
        jTxtRetenciones = new JTextField();
        jLabel5 = new JLabel();
        jLblAPagar = new JLabel();
        jLabel7 = new JLabel();
        jLblAdelantos = new JLabel();
        jLblALiquidar = new JLabel();
        jLabel9 = new JLabel();
        jlslb = new JLabel();
        jLblAjustes = new JLabel();
        jLblJubilacion = new JLabel();
        jLabel10 = new JLabel();
        jLblRetenciones = new JLabel();
        jLabel11 = new JLabel();

        jLabel1.setText("Nro orden de pago");

        jTxtNroAdelanto.setEditable(false);

        jLabel2.setText("Fecha");

        jPanel1.setBorder(BorderFactory.createTitledBorder("Empleado"));

        jLabel3.setText("Legajo");

        jLabel4.setText("Nombre y apellido");

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtLegajoEmpleado, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTxtNombreApellido))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(jTxtLegajoEmpleado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTxtNombreApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jXTblLiquidaciones.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTblLiquidaciones);

        jTabbedPane1.addTab("Liquidaciones a pagar", jScrollPane1);

        jXTblDescuentosAdelantos.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jXTblDescuentosAdelantos);

        jTabbedPane1.addTab("Adelantos a descontar", jScrollPane2);

        jXTblAjustes.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jXTblAjustes);

        jTabbedPane1.addTab("Ajustes", jScrollPane4);

        jXTblFormasDePago.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jXTblFormasDePago);

        jTabbedPane1.addTab("Formas de pago", jScrollPane3);

        jLabel6.setText("Jubilacion");

        jLabel8.setText("Retenciones");

        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtJubilacion, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jTxtRetenciones, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addContainerGap(489, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtRetenciones, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTxtJubilacion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(325, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Jubilacion y retenciones", jPanel2);

        jLabel5.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("A pagar");

        jLblAPagar.setFont(new Font("Tahoma", 1, 14)); // NOI18N

        jLabel7.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Adelantos descontados");

        jLblAdelantos.setFont(new Font("Tahoma", 1, 14)); // NOI18N

        jLblALiquidar.setFont(new Font("Tahoma", 1, 14)); // NOI18N
        jLblALiquidar.setText("$ 125555.54");

        jLabel9.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("A liquidar");

        jlslb.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jlslb.setText("Ajustes");

        jLblAjustes.setFont(new Font("Tahoma", 1, 14)); // NOI18N

        jLblJubilacion.setFont(new Font("Tahoma", 1, 14)); // NOI18N

        jLabel10.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Jubilacion");

        jLblRetenciones.setFont(new Font("Tahoma", 1, 14)); // NOI18N

        jLabel11.setFont(new Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Retenciones");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPnlBotoneraAbm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jTxtNroAdelanto, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jXDPFecha, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(jTabbedPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLblALiquidar, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jlslb)
                                    .addComponent(jLblAjustes, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLblAdelantos, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLblJubilacion, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addComponent(jLblRetenciones, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLblAPagar, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );

        layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jLblALiquidar, jLblAPagar, jLblAdelantos, jLblAjustes});

        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPnlBotoneraAbm, GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(jTxtNroAdelanto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXDPFecha, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLblALiquidar, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jlslb)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLblAjustes, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jLabel10)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLblJubilacion, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLblAdelantos, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLblRetenciones, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLblAPagar, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JLabel jLblALiquidar;
    private JLabel jLblAPagar;
    private JLabel jLblAdelantos;
    private JLabel jLblAjustes;
    private JLabel jLblJubilacion;
    private JLabel jLblRetenciones;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPnlBotoneraAbm jPnlBotoneraAbm;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JScrollPane jScrollPane4;
    private JTabbedPane jTabbedPane1;
    private JTextField jTxtJubilacion;
    private JTextField jTxtLegajoEmpleado;
    private JTextField jTxtNombreApellido;
    private JTextField jTxtNroAdelanto;
    private JTextField jTxtRetenciones;
    private JXDatePicker jXDPFecha;
    private JXTable jXTblAjustes;
    private JXTable jXTblDescuentosAdelantos;
    private JXTable jXTblFormasDePago;
    private JXTable jXTblLiquidaciones;
    private JLabel jlslb;
    // End of variables declaration//GEN-END:variables
    BuscadorOrdenDePago buscador = new BuscadorOrdenDePago(this);
    BuscadorEmpleado buscadorEmpleado = new BuscadorEmpleado(this);
    Empleado empleadoSeleccionado = null;
    Detalle detalle = new DetalleOrdenDePago();

    @Override
    protected void configurarActions() {
        super.configurarActions();
        configurarMenusContextuales();
        configurarInputOutputListeners();
    }

    private void configurarBuscarEmpleadoPorLegajoAction() {
        jTxtLegajoEmpleado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Empleado aux = null;
                if (jTxtLegajoEmpleado.getText().isEmpty()) {
                    aux = buscadorEmpleado.buscar();
                } else {
                    aux = Empleado.findByLegajo(InputUtils.parseLong(jTxtLegajoEmpleado.getText()));
                }
                if (aux == null) {
                    if (empleadoSeleccionado == null) {
                        Util.limpiar(jTxtLegajoEmpleado, jTxtNombreApellido);
                    } else {
                        mostrarEmpleado();
                    }
                } else {
                    empleadoSeleccionado = aux;
                    mostrarEmpleado();
                }
            }
        });
    }

    private void mostrarEmpleado() {
        jTxtLegajoEmpleado.setText(Long.toString(empleadoSeleccionado.getLegajo()));
        jTxtNombreApellido.setText(empleadoSeleccionado.getNombreApellido());
        cargarLiquidacionesPendientes();
        cargarAdelantosDisponibles();
        cargarTotalAPagarEnEfectivo();
        actualizarTotales();
    }

    private void configurarBuscarEmpleadoPorNombreYApellidoAction() {
        jTxtNombreApellido.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                empleadoSeleccionado = buscadorEmpleado.buscarEnPanel(jTxtNombreApellido.getText());
                mostrarEmpleado();
            }
        });
    }

    @Override
    protected Buscador<OrdenDePago> getBuscador() {
        return buscador;
    }

    @Override
    protected ControladorAbm<OrdenDePago> getControladorAbm() {
        return new ControladorAbmOrdenDePago();
    }

    @Override
    protected Detalle<OrdenDePago> getDetalle() {
        return detalle;
    }

    @Override
    protected BotoneraAbm getBotonera() {
        return jPnlBotoneraAbm;
    }

    @Override
    protected boolean isValid(OrdenDePago object) {
        return jXDPFecha.getDate() != null && !tableAdapterLiquidaciones.isEmpty() && !tableAdapterPago.isEmpty() && empleadoSeleccionado != null;
    }

    private void configurarTableAdapters() {
        configurarTableAdapterLiquidaciones();
        configurarTableAdapterAdelantos();
        configuraTableAdapterPagos();
        configurarTableAdapterAjustes();
    }

    private void configurarTableAdapterLiquidaciones() {
        CustomTableModel<LiquidacionWrapper> ctm = new CustomTableModel<>(COLUMNAS_LIQUIDACIONES);
        tableAdapterLiquidaciones = new MultipleSelectionTableAdapter<Liquidacion, LiquidacionWrapper>(jXTblLiquidaciones, ctm) {
            @Override
            public LiquidacionWrapper toWrapper(Liquidacion element) {
                return new LiquidacionWrapper(element);
            }

            @Override
            public Liquidacion fromWrapper(LiquidacionWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
    }

    private void configurarTableAdapterAdelantos() {
        CustomTableModel<DescuentoAdelantoWrapper> ctm = new CustomTableModel<>(COLUMNAS_DESCUENTO_ADELANTOS);
        tableAdapterDescuentoAdelanto = new MultipleSelectionTableAdapter<DescuentoAdelanto, DescuentoAdelantoWrapper>(jXTblDescuentosAdelantos, ctm) {
            @Override
            public DescuentoAdelantoWrapper toWrapper(DescuentoAdelanto element) {
                return new DescuentoAdelantoWrapper(element);
            }

            @Override
            public DescuentoAdelanto fromWrapper(DescuentoAdelantoWrapper wrapper) {
                return wrapper.descuentoAdelanto;
            }
        };
    }

    private void configuraTableAdapterPagos() {
        CustomTableModel<PagoWrapper> ctm = new CustomTableModel<>(COLUMNAS_PAGOS);
        tableAdapterPago = new MultipleSelectionTableAdapter<Pago, PagoWrapper>(jXTblFormasDePago, ctm) {
            @Override
            public PagoWrapper toWrapper(Pago element) {
                return new PagoWrapper(element);
            }

            @Override
            public Pago fromWrapper(PagoWrapper wrapper) {
                return wrapper.pago;
            }
        };
    }

    private void configurarTableAdapterAjustes() {
        CustomTableModel<AjusteWrapper> ctm = new CustomTableModel<>(COLUMNAS_AJUSTES);
        tableAdapterAjustes = new MultipleSelectionTableAdapter<Ajuste, AjusteWrapper>(jXTblAjustes, ctm) {
            @Override
            public AjusteWrapper toWrapper(Ajuste element) {
                return new AjusteWrapper(element);
            }

            @Override
            public Ajuste fromWrapper(AjusteWrapper wrapper) {
                return wrapper.ajuste;
            }
        };
    }

    private void cargarLiquidacionesPendientes() {
        tableAdapterLiquidaciones.setElements(Liquidacion.liquidacionesNoUsadas(empleadoSeleccionado));
        actualizarTotales();
    }

    private void cargarAdelantosDisponibles() {
        Collection<Adelanto> adelantosDisponibles = Adelanto.findAdelantosDisponibles(empleadoSeleccionado);
        tableAdapterDescuentoAdelanto.removeAllElements();
        for (Adelanto adelanto : adelantosDisponibles) {
            tableAdapterDescuentoAdelanto.addElement(new DescuentoAdelanto(adelanto, getWorkingCopy()));
        }
        actualizarTotales();
    }

    private double totalDescontado() {
        double retorno = 0;
        for (DescuentoAdelanto descuentoAdelanto : tableAdapterDescuentoAdelanto.getAllElements()) {
            retorno += descuentoAdelanto.getMonto();
        }
        return retorno;
    }

    private double totalALiquidar() {
        double retorno = 0;
        for (Liquidacion liquidacion : tableAdapterLiquidaciones.getAllElements()) {
            retorno += liquidacion.totalLiquidacion();
        }
        return retorno;
    }

    private double totalPagado() {
        double retorno = 0;
        for (Pago pago : tableAdapterPago.getAllElements()) {
            retorno += pago.getMonto();
        }
        return retorno;
    }

    private double totalAjustes() {
        double retorno = 0;
        for (Ajuste ajuste : tableAdapterAjustes.getAllElements()) {
            retorno += ajuste.getMonto();
        }
        return retorno;
    }

    private void cargarTotalAPagarEnEfectivo() {
        tableAdapterPago.clear();
        Pago pago = new Pago();
        pago.setFormaPago(FormaPago.first());
        pago.setMonto(totalAPagar());
        tableAdapterPago.addElement(pago);
        actualizarTotales();
    }

    private double totalAPagar() {
        return totalALiquidar() - totalDescontado() + totalAjustes() + jubilacion() - retenciones();
    }

    private void configurarMenusContextuales() {
        cargarMenuContextual(jXTblDescuentosAdelantos);
        cargarMenuContextual(jXTblLiquidaciones);
        cargarMenuContextual(jXTblFormasDePago);
        cargarMenuContextual(jXTblAjustes);
    }

    private void cargarMenuContextual(JXTable jXTbl) {
        final MultipleSelectionTableAdapter[] aUsar = new MultipleSelectionTableAdapter[]{null};
        final Runnable[] runnable = new Runnable[1];
        if (jXTbl == jXTblDescuentosAdelantos) {
            aUsar[0] = tableAdapterDescuentoAdelanto;
            runnable[0] = new Runnable() {
                @Override
                public void run() {
                    cargarAdelantosDisponibles();
                }
            };
        } else if (jXTbl == jXTblFormasDePago) {
            aUsar[0] = tableAdapterPago;
            runnable[0] = new Runnable() {
                @Override
                public void run() {
                    cargarTotalAPagarEnEfectivo();
                }
            };

        } else if (jXTbl == jXTblAjustes) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            JMenuItem jMnuEliminar = new JMenuItem("Eliminar", Iconos.ELIMINAR_16X16);
            jMnuEliminar.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    tableAdapterAjustes.removeSelectedElements();
                }
            });
            JMenuItem jMnuAgregar = new JMenuItem("Agregar", Iconos.EDITAR_16X16);
            jMnuAgregar.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    tableAdapterAjustes.addElement(new Ajuste());
                }
            });
            jPopupMenu.add(jMnuAgregar);
            jPopupMenu.add(jMnuEliminar);
            jXTblAjustes.setComponentPopupMenu(jPopupMenu);
            return;
        } else {
            aUsar[0] = tableAdapterLiquidaciones;
            runnable[0] = new Runnable() {
                @Override
                public void run() {
                    cargarLiquidacionesPendientes();
                }
            };
        }

        final JPopupMenu jPopupMenu = new JPopupMenu();

        final JMenuItem jMnuItemEliminar = new JMenuItem("Eliminar", Iconos.ELIMINAR_16X16);
        jMnuItemEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aUsar[0].removeSelectedElements();
                actualizarTotales();
            }
        });
        jPopupMenu.add(jMnuItemEliminar);
        final JMenuItem jMenuItemRecargarDisponibles = new JMenuItem("Recargar disponibles", Iconos.EDITAR_16X16);
        jMenuItemRecargarDisponibles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                runnable[0].run();
            }
        });
        jPopupMenu.add(jMenuItemRecargarDisponibles);
        jXTbl.setComponentPopupMenu(jPopupMenu);
    }

    private double retenciones() {
        return InputUtils.parseCurrency(jTxtRetenciones.getText()).doubleValue();
    }

    private double jubilacion() {
        return InputUtils.parseCurrency(jTxtJubilacion.getText()).byteValue();
    }

    private void configurarInputOutputListeners() {
        InputUtils.addCurrencyListeners(jTxtRetenciones, true);
        InputUtils.addCurrencyListeners(jTxtJubilacion, true);
    }

    private static class ControladorAbmOrdenDePago implements ControladorAbm<OrdenDePago> {

        public ControladorAbmOrdenDePago() {
        }

        @Override
        public void anular(OrdenDePago element) {
        }

        @Override
        public OrdenDePago crear() {
            return new OrdenDePago();
        }

        @Override
        public void eliminar(OrdenDePago element) {
            Service.delete(element);
        }

        @Override
        public void guardar(OrdenDePago element) {
            OrdenDePago.guardar(element);
        }

        @Override
        public void imprimir(OrdenDePago element) throws PrinterException {
            Impresor.imprimir(element);
        }

        @Override
        public void vistaPreliminar(OrdenDePago element) throws PrinterException {
        }

        @Override
        public OrdenDePago reload(OrdenDePago element) {
            return Service.load(OrdenDePago.class, element.getId());
        }

        @Override
        public Object getId(OrdenDePago element) {
            return element.getId();
        }
    }
    final String[] COLUMNAS_AJUSTES = "Observaciones, Monto".split(", ");

    private class AjusteWrapper implements Vectorizable {

        final int COLUMNA_OBSERVACIONES = 0, COLUMNA_MONTO = 1;
        Ajuste ajuste;

        public AjusteWrapper() {
            ajuste = new Ajuste();
        }

        public AjusteWrapper(Ajuste ajuste) {
            this.ajuste = ajuste;
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                ajuste.getObservaciones(),
                ajuste.getMonto()
            });
        }

        @Override
        public void setValueAt(int column, Object aValue) {
            switch (column) {
                case COLUMNA_MONTO:
                    ajuste.setMonto(InputUtils.parseCurrency(aValue).doubleValue());
                    actualizarTotales();
                    break;
                case COLUMNA_OBSERVACIONES:
                    ajuste.setObservaciones(aValue.toString());
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        public boolean isCellEditable(int column) {
            return true;
        }
    }
    final String[] COLUMNAS_PAGOS = "Forma pago, Monto".split(", ");

    private class PagoWrapper implements Vectorizable {

        final int COLUMNA_FORMA_PAGO = 0,
                COLUMNA_MONTO = 1;
        private Pago pago;

        public PagoWrapper(Pago pago) {
            this.pago = pago;
        }

        public PagoWrapper() {
            this.pago = new Pago();
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                pago.getFormaPago(),
                pago.getMonto()
            });
        }

        @Override
        public void setValueAt(int column, Object aValue) {
            switch (column) {
                case COLUMNA_FORMA_PAGO:
                    pago.setFormaPago((FormaPago) aValue);
                    break;
                case COLUMNA_MONTO:
                    pago.setMonto(InputUtils.parseCurrency(aValue).doubleValue());
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        public boolean isCellEditable(int column) {
            return true;
        }
    }
    final String[] COLUMNAS_DESCUENTO_ADELANTOS = "Nro adelanto, Fecha, Monto adelantado, Disponible, A descontar del adelanto".split(", ");

    private class DescuentoAdelantoWrapper implements Vectorizable {

        final int COLUMNA_NRO_ADELANTO = 0,
                COLUMNA_FECHA = 1,
                COLUMNA_MONTO_A_DESCONTAR = 4;
        private DescuentoAdelanto descuentoAdelanto;
        private double disponible = 0;

        public DescuentoAdelantoWrapper(DescuentoAdelanto descuentoAdelanto) {
            this.descuentoAdelanto = descuentoAdelanto;
            this.disponible = descuentoAdelanto.getAdelanto().disponible();
        }

        public DescuentoAdelantoWrapper() {
            this.descuentoAdelanto = new DescuentoAdelanto();
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                descuentoAdelanto.getAdelanto() != null ? descuentoAdelanto.getAdelanto().getId() : "",
                descuentoAdelanto.getAdelanto() != null
                ? OutputUtils.formatShort(descuentoAdelanto.getAdelanto().getFecha().getTime())
                : "",
                descuentoAdelanto.getAdelanto() != null ? descuentoAdelanto.getAdelanto().getMonto() : 0,
                disponible,
                descuentoAdelanto.getMonto()
            });
        }

        @Override
        public void setValueAt(int column, Object aValue) {
            switch (column) {
                case COLUMNA_NRO_ADELANTO:
                    Long nroAdelanto = (Long) aValue;
                    Collection<Adelanto> findByNroAdelanto = Adelanto.findByNroAdelanto(nroAdelanto);
                    if (!findByNroAdelanto.isEmpty()) {
                        descuentoAdelanto.setAdelanto(findByNroAdelanto.iterator().next());
                    }
                    break;
                case COLUMNA_MONTO_A_DESCONTAR:
                    final double montoADescontar = InputUtils.parseBigDecimal(aValue, BigDecimal.ZERO).doubleValue();
                    if (montoADescontar > disponible) {
                        MessageUtils.showError(null, "No se puede descontar mas que el disponibles");
                    } else {
                        descuentoAdelanto.setMonto(montoADescontar);
                        actualizarTotales();
                    }
                    break;
            }
        }

        @Override
        public boolean isCellEditable(int column) {
            return column == COLUMNA_MONTO_A_DESCONTAR || column == COLUMNA_NRO_ADELANTO;
        }
    }

    protected void actualizarTotales() {
        if (totalAPagar() < 0) {
            MessageUtils.showError(this, "El total a pagar no puede ser negativo (menor a cero)");
        }
        if (totalPagado() > (totalAPagar())) {
            MessageUtils.showError(this, "El total pagado no puede ser mayor a el total a pagar");
        }
        jLblALiquidar.setText(OutputUtils.formatCurrency(new BigDecimal(totalALiquidar())));
        jLblAjustes.setText(OutputUtils.formatCurrency(new BigDecimal(totalAjustes())));
        jLblAdelantos.setText(OutputUtils.formatCurrency(new BigDecimal(totalDescontado())));
        jLblJubilacion.setText(OutputUtils.formatCurrency(jubilacion()));
        jLblRetenciones.setText(OutputUtils.formatCurrency(retenciones()));
        jLblAPagar.setText(OutputUtils.formatCurrency(new BigDecimal(totalAPagar())));
    }
    final String[] COLUMNAS_LIQUIDACIONES = "Nro, Desde, Hasta, Horas trabajadas, Horas extras, Precio hora".split(", ");

    private class LiquidacionWrapper extends WrapperNoEditable<Liquidacion> {

        public LiquidacionWrapper(Liquidacion elemento) {
            super(elemento);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                getElemento().getId(),
                OutputUtils.formatShort(getElemento().getDesde()),
                OutputUtils.formatShort(getElemento().getHasta()),
                OutputUtils.formatHorasMinutos(getElemento().minutosTrabajados()),
                getElemento().getHorasExtras(), 
                getElemento().getPrecioHora()
            });
        }
    }

    private class DetalleOrdenDePago implements Detalle<OrdenDePago> {

        public DetalleOrdenDePago() {
        }

        @Override
        public void mostrarDatos(OrdenDePago element) {
            jTxtNroAdelanto.setText(element.getId().toString());
            empleadoSeleccionado = element.getEmpleado();
            jTxtLegajoEmpleado.setText(element.getEmpleado().getLegajo().toString());
            jTxtNombreApellido.setText(element.getEmpleado().getNombreApellido());
            tableAdapterDescuentoAdelanto.setElements(element.getDescuentosAdelantos());
            tableAdapterLiquidaciones.setElements(element.getLiquidaciones());
            tableAdapterPago.setElements(element.getPagos());
            tableAdapterAjustes.setElements(element.getAjustes());
            jTxtJubilacion.setText(OutputUtils.formatCurrency(element.getJubilacion()));
            jTxtRetenciones.setText(OutputUtils.formatCurrency(element.getRetenciones()));
            jLblJubilacion.setText(OutputUtils.formatCurrency(element.getJubilacion()));
            jLblRetenciones.setText(OutputUtils.formatCurrency(element.getRetenciones()));
            actualizarTotales();
        }

        @Override
        public void guardarDatos(OrdenDePago element) {
            element.setDescuentosAdelantos(tableAdapterDescuentoAdelanto.getAllElements());
            element.setEmpleado(empleadoSeleccionado);
            Calendar fecha = Calendar.getInstance();
            if(jXDPFecha.getDate() != null) fecha.setTime(jXDPFecha.getDate());
            element.setFecha(fecha);
            element.setLiquidaciones(tableAdapterLiquidaciones.getAllElements());
            element.setPagos(tableAdapterPago.getAllElements());
            element.setAjustes(tableAdapterAjustes.getAllElements());
            element.setRetenciones(InputUtils.parseCurrency(jTxtRetenciones.getText()).doubleValue());
            element.setJubilacion(InputUtils.parseCurrency(jTxtJubilacion.getText()).doubleValue());
        }

        @Override
        public void edicion() {
            Util.setEnabled(true, jTxtNombreApellido, jTxtLegajoEmpleado, jXTblDescuentosAdelantos, jXTblFormasDePago, jXTblLiquidaciones, jXDPFecha);
        }

        @Override
        public void visualizacion() {
            Util.setEnabled(false, jTxtNombreApellido, jTxtLegajoEmpleado, jXTblDescuentosAdelantos, jXTblFormasDePago, jXTblLiquidaciones, jXDPFecha);
        }

        @Override
        public void limpiar() {
            tableAdapterDescuentoAdelanto.clear();
            tableAdapterLiquidaciones.clear();
            tableAdapterPago.clear();
            tableAdapterAjustes.clear();
            Util.limpiar(jTxtLegajoEmpleado, jTxtNombreApellido, jTxtNroAdelanto);
        }
    }
}
