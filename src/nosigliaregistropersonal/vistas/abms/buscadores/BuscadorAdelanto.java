package nosigliaregistropersonal.vistas.abms.buscadores;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.JPanel;
import nosigliaregistropersonal.modelo.Adelanto;
import nosigliaregistropersonal.vistas.framework.Buscador;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

/**
 *
 * @author #emas
 */
public class BuscadorAdelanto extends Buscador<Adelanto>{

    public BuscadorAdelanto(JPanel oyente) {
        super(oyente);
    }
    
    @Override
    public Collection<Adelanto> buscar(String textBusqueda) {
        if (InputUtils.isInteger(textBusqueda)){
            return Adelanto.findByNroAdelanto(InputUtils.parseLong(textBusqueda));
        }
        return Adelanto.findByEmpleado(textBusqueda);
    }

    @Override
    public String[] columnas() {
        return "Nro adelanto, Fecha, Empleado, Monto adelanto".split(", ");
    }

    @Override
    public Adelanto unWrap(WrapperNoEditable<Adelanto> wrapper) {
        return wrapper.getElemento();
    }

    @Override
    public WrapperNoEditable<Adelanto> wrap(Adelanto element) {
        return new WrapperNoEditable<Adelanto>(element) {
            @Override
            public List<Object> asVector() {
                return Arrays.asList(new Object[]{
                    getElemento().getId(),
                    OutputUtils.formatFechaYHora(getElemento().getFecha().getTime()),
                    getElemento().getEmpleado().getNombreApellido(), 
                    getElemento().getMonto()
                });
            }
        };
    }

}
