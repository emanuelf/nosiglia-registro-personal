package nosigliaregistropersonal.vistas.abms.buscadores;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.JPanel;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.vistas.framework.Buscador;
import nosigliaregistropersonal.vistas.framework.OyenteBusqueda;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

/**
 *
 * @author #emas
 */
public class BuscadorEmpleado extends Buscador<Empleado>{

    public BuscadorEmpleado(JPanel oyente) {
        super(oyente);
    }
    
    @Override
    public String[] columnas() {
        return "Legajo, Nombre y apellido".split(", ");
    }

    @Override
    public Empleado unWrap(WrapperNoEditable<Empleado> wrapper) {
        return wrapper.getElemento();
    }

    @Override
    public WrapperNoEditable<Empleado> wrap(Empleado element) {
        return new WrapperNoEditable<Empleado>(element) {
            @Override
            public List<Object> asVector() {
                return Arrays.asList(new Object[]{
                    getElemento().getLegajo(), 
                    getElemento().getNombreApellido()
                });
            }
        };
    }

    @Override
    public Empleado buscarEnPanel(String textBusqueda) {
        panelBusqueda.executeQuery(textBusqueda);
        mostrarPanelBusquedaEnModal();
        return seleccion;
    }

    @Override
    public Collection<Empleado> buscar(String textBusqueda) {
        return Empleado.findByNombreYApellido(textBusqueda);
    }

}
