package nosigliaregistropersonal.vistas.abms.buscadores;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.JPanel;
import nosigliaregistropersonal.modelo.OrdenDePago;
import nosigliaregistropersonal.vistas.framework.Buscador;
import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

/**
 *
 * @author #emas
 */
public class BuscadorOrdenDePago extends Buscador<OrdenDePago> {

    public BuscadorOrdenDePago(JPanel oyente) {
        super(oyente);
    }

    
    @Override
    public Collection<OrdenDePago> buscar(String textBusqueda) {
        if (InputUtils.isInteger(textBusqueda)) {
            return OrdenDePago.findById(InputUtils.parseLong(textBusqueda));
        }
        return OrdenDePago.findByApyNomEmpleado(textBusqueda);
    }

    @Override
    public String[] columnas() {
        return "Nro orden, Fecha, Empleado, Total".split(", ");
    }

    @Override
    public OrdenDePago unWrap(WrapperNoEditable<OrdenDePago> wrapper) {
        return wrapper.getElemento();
    }

    @Override
    public WrapperNoEditable<OrdenDePago> wrap(OrdenDePago element) {
        return new WrapperNoEditable<OrdenDePago>(element) {
            
            @Override
            public List<Object> asVector() {
                return Arrays.asList(new Object[]{
                    getElemento().getId(),
                    OutputUtils.formatShort(getElemento().getFecha().getTime()), 
                    getElemento().getEmpleado().getNombreApellido(), 
                    getElemento().getTotalAPagar()
                });
            }
        };
    }
}
