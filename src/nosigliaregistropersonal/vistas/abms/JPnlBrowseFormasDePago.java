package nosigliaregistropersonal.vistas.abms;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.util.Arrays;
import java.util.List;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.modelo.FormaPago;
import nosigliaregistropersonal.modelo.persistencia.Service;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;

public class JPnlBrowseFormasDePago extends javax.swing.JPanel {

    JPnlEdicionFormaPago panelEdicion;
    private SingleSelectionTableAdapter<FormaPago, FormaPagoWrapper> tableAdapter;

    public JPnlBrowseFormasDePago() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
            configurarTabla();
            panelEdicion = new JPnlEdicionFormaPago();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jXTblEmpleados = new org.jdesktop.swingx.JXTable();
        jToolBar1 = new javax.swing.JToolBar();
        jBtnAgregar = new javax.swing.JButton();
        jBtnEditarCancelar = new javax.swing.JButton();
        jBtnEliminar = new javax.swing.JButton();

        jXTblEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre", "Codigo"
            }
        ));
        jScrollPane1.setViewportView(jXTblEmpleados);

        jToolBar1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jToolBar1.setRollover(true);
        jToolBar1.setBorderPainted(false);

        jBtnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/agregar.png"))); // NOI18N
        jBtnAgregar.setFocusable(false);
        jBtnAgregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBtnAgregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(jBtnAgregar);

        jBtnEditarCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/editar.png"))); // NOI18N
        jBtnEditarCancelar.setFocusable(false);
        jBtnEditarCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBtnEditarCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEditarCancelar);

        jBtnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/eliminar.png"))); // NOI18N
        jBtnEliminar.setFocusable(false);
        jBtnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBtnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEliminar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 686, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAgregar;
    private javax.swing.JButton jBtnEditarCancelar;
    private javax.swing.JButton jBtnEliminar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXTable jXTblEmpleados;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarAbmActions();
    }

    private void agregarAbmActions() {
        agregarNuevaFormaDePagoAction();
        agregarBorrarFormaDePagoAction();
        agregarEditarFormaDePagoAction();
    }

    private void agregarNuevaFormaDePagoAction() {
        jBtnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int nextCodigo = FormaPago.nextCodigo();
                panelEdicion.setFormaPago(new FormaPago(nextCodigo));
                mostrarPanelEdicion();
                cargarFormasDePago();
            }
        });
    }

    private void mostrarPanelEdicion() {
        PanelsUtil.showPanelOnModalDialog(SwingUtilities.getWindowAncestor(this), panelEdicion, "Edicion Formas de pago");
    }

    private void agregarBorrarFormaDePagoAction() {
        jBtnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (MessageUtils.areYouSure("Estas seguro?")) {
                    FormaPago seleccionado = tableAdapter.getSelectedElement();
                    Service.delete(seleccionado);
                    tableAdapter.removeSelectedElement();
                }
            }
        });
    }

    private void agregarEditarFormaDePagoAction() {
        jBtnEditarCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter.hasSelection()) {
                    panelEdicion.setFormaPago(tableAdapter.getSelectedElement());
                    PanelsUtil.showPanelOnModalDialog(getWindowAncestor(), panelEdicion, "Edicion de formas de pago");
                    cargarFormasDePago();
                } else {
                    MessageUtils.showWarning(JPnlBrowseFormasDePago.this, "Epa... Debes seleccionar algo ;)");
                }
            }
        });
    }

    public Window getWindowAncestor() {
        return SwingUtilities.getWindowAncestor(this);
    }

    private void configurarTableAdapter() {
        CustomTableModel<FormaPagoWrapper> ctm = new CustomTableModel<>(columnas);
        tableAdapter = new SingleSelectionTableAdapter<FormaPago, FormaPagoWrapper>(jXTblEmpleados, ctm) {
            @Override
            public FormaPagoWrapper toWrapper(FormaPago element) {
                return new FormaPagoWrapper(element);
            }

            @Override
            public FormaPago fromWrapper(FormaPagoWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
    }
    final String[] columnas = "Codigo, Nombre".split(", ");

    private void cargarFormasDePago() {
        tableAdapter.setElements(FormaPago.all());
    }

    private void configurarTabla() {
        configurarTableAdapter();
        cargarFormasDePago();
    }

    private static class FormaPagoWrapper extends WrapperNoEditable<FormaPago> {

        final static int COLUMNA_CODIGO = 0,
                COLUMNA_NOMBRE = 1;

        public FormaPagoWrapper(FormaPago formaPago) {
            super(formaPago);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                getElemento().getCodigo(),
                getElemento().getNombre()
            });
        }
    }
}
