package nosigliaregistropersonal.vistas.abms;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.Empleado;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;
import nosigliaregistropersonal.vistas.lib.modeloTabla.renderers.ImporteCellRenderer;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTable;

/**
 *
 * @author Emanuel
 */
public class JPnlBrowseTrabajadores extends JPanel {

    private SingleSelectionTableAdapter<Empleado, EmpleadoWrapper> tableAdapter;
    private JPnlEdicionEmpleado panelEdicion = new JPnlEdicionEmpleado();
    private List<Empleado> borrados =  new ArrayList<>();
    public JPnlBrowseTrabajadores() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
            configurarTabla();
            cargarEmpleados();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXSearchField1 = new JXSearchField();
        jScrollPane1 = new JScrollPane();
        jXTblEmpleados = new JXTable();
        jToolBar1 = new JToolBar();
        jBtnAgregar = new JButton();
        jBtnEditarCancelar = new JButton();
        jBtnEliminar = new JButton();

        jXSearchField1.setToolTipText("");
        jXSearchField1.setPrompt("Nombre o legajo a buscar");
        jXSearchField1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jXSearchField1ActionPerformed(evt);
            }
        });

        jXTblEmpleados.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre y apellido", "Legajo"
            }
        ));
        jScrollPane1.setViewportView(jXTblEmpleados);

        jToolBar1.setOrientation(SwingConstants.VERTICAL);
        jToolBar1.setRollover(true);
        jToolBar1.setBorderPainted(false);

        jBtnAgregar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/agregar.png"))); // NOI18N
        jBtnAgregar.setFocusable(false);
        jBtnAgregar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnAgregar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnAgregar);

        jBtnEditarCancelar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/editar.png"))); // NOI18N
        jBtnEditarCancelar.setFocusable(false);
        jBtnEditarCancelar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEditarCancelar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEditarCancelar);

        jBtnEliminar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/eliminar.png"))); // NOI18N
        jBtnEliminar.setFocusable(false);
        jBtnEliminar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEliminar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEliminar);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(jXSearchField1, GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jXSearchField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                    .addComponent(jToolBar1, GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jXSearchField1ActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jXSearchField1ActionPerformed
        List<Empleado> allElements = tableAdapter.getAllElements();
        final ListIterator<Empleado> empleadosIterator = allElements.listIterator();
        while (empleadosIterator.hasNext()) {
            Empleado empleado = empleadosIterator.next();
            if (!empleado.getNombreApellido().toLowerCase().contains(jXSearchField1.getText().toLowerCase())){
                borrados.add(empleado);
                empleadosIterator.remove();
            }
        }
        tableAdapter.setElements(allElements);
        ListIterator<Empleado> borradosIterator = borrados.listIterator();
        while (borradosIterator.hasNext()) {
            Empleado empleadoBorrado = borradosIterator.next();
            if (!tableAdapter.contains(empleadoBorrado) && empleadoBorrado.getNombreApellido().toLowerCase().contains(jXSearchField1.getText().toLowerCase())){
                tableAdapter.addElement(empleadoBorrado);
            }
        }
    }//GEN-LAST:event_jXSearchField1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnAgregar;
    private JButton jBtnEditarCancelar;
    private JButton jBtnEliminar;
    private JScrollPane jScrollPane1;
    private JToolBar jToolBar1;
    private JXSearchField jXSearchField1;
    private JXTable jXTblEmpleados;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarAbmActions();
    }

    private void agregarAbmActions() {
        agregarNuevoTrabajadorAction();
        agregarBorrarTrabajadorAction();
        agregarEditarTrabajadorAction();
    }

    private void agregarNuevoTrabajadorAction() {
        jBtnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelEdicion.setEmpleado(null);
                mostrarPanelEdicion();
            }
        });
    }

    private void mostrarPanelEdicion() {
        PanelsUtil.showPanelOnModalDialog(SwingUtilities.getWindowAncestor(jXSearchField1), panelEdicion, "Edicion Empleados");
    }

    private void agregarBorrarTrabajadorAction() {
        jBtnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (MessageUtils.areYouSure("Estas seguro?")) {
                    Empleado empleadoSeleccionado = tableAdapter.getSelectedElement();
                    empleadoSeleccionado.delete();
                    tableAdapter.removeSelectedElement();
                }
            }
        });
    }

    private void agregarEditarTrabajadorAction() {
        jBtnEditarCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter.hasSelection()) {
                    panelEdicion.setEmpleado(tableAdapter.getSelectedElement());
                    PanelsUtil.showPanelOnModalDialog(getWindowAncestor(), panelEdicion, "Edicion de empleados");
                } else {
                    MessageUtils.showWarning(JPnlBrowseTrabajadores.this, "Epa... Debes seleccionar algo ;)");
                }
            }
        });
    }

    public Window getWindowAncestor(){
        return SwingUtilities.getWindowAncestor(this);
    }
    private void configurarTableAdapter() {
        CustomTableModel<EmpleadoWrapper> ctm = new CustomTableModel<>(columnas);
        tableAdapter = new SingleSelectionTableAdapter<Empleado, EmpleadoWrapper>(jXTblEmpleados, ctm) {
            @Override
            public EmpleadoWrapper toWrapper(Empleado element) {
                return new EmpleadoWrapper(element);
            }

            @Override
            public Empleado fromWrapper(EmpleadoWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
    }
    final String[] columnas = "Legajo, Apellido y nombre, Precio/hora".split(", ");

    private void cargarEmpleados() {
        tableAdapter.setElements(Empleado.findAll());
    }

    private void configurarTabla() {
        configurarTableAdapter();
        jXTblEmpleados.getColumn(EmpleadoWrapper.COLUMNA_PRECIO_HORA).setCellRenderer( new ImporteCellRenderer());
    }

    static class EmpleadoWrapper extends WrapperNoEditable<Empleado> {

        final static int COLUMNA_LEGAJO = 0,
                COLUMNA_APYNOM = 1,
                COLUMNA_PRECIO_HORA = 2;

        public EmpleadoWrapper(Empleado empleado) {
            super(empleado);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                getElemento().getLegajo(),
                getElemento().getNombreApellido(),
                new BigDecimal(getElemento().getPrecioHora())
            });
        }
    }
}
