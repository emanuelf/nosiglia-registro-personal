package nosigliaregistropersonal.vistas.abms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Beans;
import java.util.Arrays;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import nosigliaregistropersonal.modelo.PeriodoTrabajo;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.PanelsUtil;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.WrapperNoEditable;
import org.jdesktop.swingx.JXTable;

/**
 *
 * @author Emanuel
 */
public class JPnlBrowsePeriodosTrabajo extends javax.swing.JPanel {

    private JPnlEdicionPeriodoTrabajo panelEdicion;
    private SingleSelectionTableAdapter<PeriodoTrabajo, PeriodoTrabajoWrapper> tableAdapter;

    public JPnlBrowsePeriodosTrabajo() {
        initComponents();
        if (!Beans.isDesignTime()) {
            agregarActions();
            configurarTabla();
            panelEdicion = new JPnlEdicionPeriodoTrabajo();
            cargarTabla();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new JToolBar();
        jBtnAgregar = new JButton();
        jBtnEditarCancelar = new JButton();
        jBtnEliminar = new JButton();
        jScrollPane1 = new JScrollPane();
        jXTblPeriodosTrabajo = new JXTable();

        jToolBar1.setOrientation(SwingConstants.VERTICAL);
        jToolBar1.setRollover(true);
        jToolBar1.setBorderPainted(false);

        jBtnAgregar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/agregar.png"))); // NOI18N
        jBtnAgregar.setFocusable(false);
        jBtnAgregar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnAgregar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnAgregar);

        jBtnEditarCancelar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/editar.png"))); // NOI18N
        jBtnEditarCancelar.setFocusable(false);
        jBtnEditarCancelar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEditarCancelar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEditarCancelar);

        jBtnEliminar.setIcon(new ImageIcon(getClass().getResource("/nosigliaregistropersonal/iconos/abms/eliminar.png"))); // NOI18N
        jBtnEliminar.setFocusable(false);
        jBtnEliminar.setHorizontalTextPosition(SwingConstants.CENTER);
        jBtnEliminar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jBtnEliminar);

        jXTblPeriodosTrabajo.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre", "Codigo"
            }
        ));
        jScrollPane1.setViewportView(jXTblPeriodosTrabajo);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton jBtnAgregar;
    private JButton jBtnEditarCancelar;
    private JButton jBtnEliminar;
    private JScrollPane jScrollPane1;
    private JToolBar jToolBar1;
    private JXTable jXTblPeriodosTrabajo;
    // End of variables declaration//GEN-END:variables

    private void agregarActions() {
        agregarNuevoAction();
        agregarEditarAction();
        agregarEliminarAction();
    }

    private void configurarTabla() {
        CustomTableModel<PeriodoTrabajoWrapper> ctm = new CustomTableModel<>(COLUMNAS);
        tableAdapter = new SingleSelectionTableAdapter<PeriodoTrabajo, PeriodoTrabajoWrapper>(jXTblPeriodosTrabajo, ctm) {
            @Override
            public PeriodoTrabajoWrapper toWrapper(PeriodoTrabajo element) {
                return new PeriodoTrabajoWrapper(element);
            }

            @Override
            public PeriodoTrabajo fromWrapper(PeriodoTrabajoWrapper wrapper) {
                return wrapper.getElemento();
            }
        };
        tableAdapter.setElements(PeriodoTrabajo.all());
    }

    private void agregarNuevoAction() {
        jBtnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelEdicion.setPeriodo(new PeriodoTrabajo());
                PanelsUtil.showPanelOnModalDialog(SwingUtilities.getWindowAncestor(jToolBar1),
                        panelEdicion,
                        "Nuevo periodo");
                cargarTabla();
            }
        });
    }

    private void agregarEditarAction() {
        jBtnEditarCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelEdicion.setPeriodo(tableAdapter.getSelectedElement());
                PanelsUtil.showPanelOnModalDialog(SwingUtilities.getWindowAncestor(jToolBar1), panelEdicion, "Edicion de periodo de trabajo");
                cargarTabla();
            }
        });
    }

    private void agregarEliminarAction() {
        jBtnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PeriodoTrabajo selectedElement = tableAdapter.getSelectedElement();
                if (selectedElement.usado()) {
                    MessageUtils.showWarning(null, "NO puede borrar un periodo de trabajo si "
                            + "ya fue usado para una liquidacion");
                } else {
                    selectedElement.delete();
                    tableAdapter.removeSelectedElement();
                }
            }
        });
    }
    final String[] COLUMNAS = "Inicio, Fin, Horas laborales, Descripcion".split(", ");

    private void cargarTabla() {
        tableAdapter.setElements(PeriodoTrabajo.all());
    }

    private class PeriodoTrabajoWrapper extends WrapperNoEditable<PeriodoTrabajo> {

        public PeriodoTrabajoWrapper(PeriodoTrabajo elemento) {
            super(elemento);
        }

        @Override
        public List<Object> asVector() {
            return Arrays.asList(new Object[]{
                OutputUtils.formatShort(getElemento().getInicio().getTime()),
                OutputUtils.formatShort(getElemento().getFin().getTime()),
                getElemento().getHorasLaborales(),
                getElemento().getDescripcion()
            });
        }
    }
}