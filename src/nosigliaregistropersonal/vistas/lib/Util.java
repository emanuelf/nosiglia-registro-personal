package nosigliaregistropersonal.vistas.lib;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;

import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import org.jdesktop.swingx.JXDatePicker;

public class Util {

    private static String saveDir;

    private Util() {
        throw new UnsupportedOperationException("Non instantiable");
    }

    public static void limpiar(JTextComponent... components) {
        for (JTextComponent textComponent : components) {
            textComponent.setText("");
        }
    }

    public static void limpiarJTextComponents(Component[] components) {
        JTextComponent[] textComponents = getTextComponentsFrom(components);
        for (JTextComponent textComponent : textComponents) {
            textComponent.setText("");
        }
    }

    private static JTextComponent[] getTextComponentsFrom(Component[] components) {
        List<JTextComponent> textComponents = new ArrayList<>();
        for (Component component : components) {
            if (component instanceof JTextComponent) {
                textComponents.add((JTextComponent) component);
            }
        }
        return textComponents.toArray(new JTextComponent[textComponents.size()]);
    }

    public static void limpiarComponentes(Component... components) {
        for (Component component : components) {
            if (component instanceof JComboBox) {
                JComboBox<?> combo = ((JComboBox) component);
                if (combo.getItemCount() != 0) {
                    combo.setSelectedIndex(InputUtils.DEFAULT_SELECTION_INDEX);
                }
            }
            if (component instanceof JTable) {
                TableModel model = ((JTable) component).getModel();
                if (model instanceof CustomTableModel) {
                    ((CustomTableModel) model).clear();
                } else {
                    ((DefaultTableModel) model).setRowCount(0);
                }
            }
            if (component instanceof JToggleButton) {
                ((JToggleButton) component).setSelected(false);
            }
            if (component instanceof JXDatePicker){
                ((JXDatePicker) component).setDate(null);
            }
        }
        limpiarJTextComponents(components);
    }

    public static void setEnabled(boolean enabled, Component... components) {
        for (Component component : components) {
            component.setEnabled(enabled);
        }
    }

    public static void setVisible(boolean visible, Component... components) {
        for (Component component : components) {
            component.setVisible(visible);
        }
    }

    public static void setEnabled(boolean enabled, Action... actions) {
        for (Action action : actions) {
            action.setEnabled(enabled);
        }
    }

    public static void setEditable(boolean editable, JTextComponent... component) {
        for (JTextComponent jTextComponent : component) {
            jTextComponent.setEditable(editable);
        }
    }

    public static String getSelectedFileName(String nombreDefecto, String extension) {
        JFileChooser jFileChooser = new JFileChooser(saveDir);
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jFileChooser.setDragEnabled(true);
        jFileChooser.setSelectedFile(new File(nombreDefecto + "." + extension));
        if (jFileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            saveDir = jFileChooser.getSelectedFile().getParent();
            return jFileChooser.getSelectedFile().getAbsolutePath();
        } else {
            return null;
        }
    }
    
    
}
