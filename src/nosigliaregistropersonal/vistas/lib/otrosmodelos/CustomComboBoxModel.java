package nosigliaregistropersonal.vistas.lib.otrosmodelos;

import java.util.Arrays;
import javax.swing.DefaultComboBoxModel;

public class CustomComboBoxModel<E> extends DefaultComboBoxModel<E> {

    public CustomComboBoxModel(Iterable<E> objetos) {
        for (E e : objetos) {
            addElement(e);
        }
    }

    public CustomComboBoxModel(E[] values) {
        this(Arrays.asList(values));
    }
}