package nosigliaregistropersonal.vistas.lib;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTextField;
import nosigliaregistropersonal.util.BigDecimalUtil;


public class OutputUtils {

    private static final NumberFormat PERCENTAGE_FORMATTER = NumberFormat.getPercentInstance();
    private static final NumberFormat CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance();
    private static final DecimalFormat NRO_FACTURA_FORMATTER = new DecimalFormat("00000000");
    private static final String FORMATO_NRO_COMPROBANTE = "00000000";
    private static final String FORMATO_PUNTO_VENTA = "0000";
    private static int askedTasks;
    private static int runnedTasks;
    public static final Color CURRENT_COLOR = null;
    public static final Component NO_COMPONENT = null;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yy HH:mm");
    private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH : mm : ss");

    static {
        PERCENTAGE_FORMATTER.setMaximumFractionDigits(2);
    }

    private OutputUtils() {
        throw new UnsupportedOperationException("Non instantiable");//not even through reflection
    }

    public static String formatNroComprobante(int nro, boolean puntoVenta) {
        if (puntoVenta) {
            NRO_FACTURA_FORMATTER.applyPattern(FORMATO_PUNTO_VENTA);
        } else {
            NRO_FACTURA_FORMATTER.applyPattern(FORMATO_NRO_COMPROBANTE);
        }
        return NRO_FACTURA_FORMATTER.format(nro);
    }

    public static String formatNroComprobante(int nro) {
        NRO_FACTURA_FORMATTER.applyPattern(FORMATO_NRO_COMPROBANTE);
        return NRO_FACTURA_FORMATTER.format(nro);
    }

    public static String formatPercentage(BigDecimal percentage) {
        if (BigDecimalUtil.menorQueCero(percentage) || BigDecimalUtil.mayorQueUno(percentage)) {
            throw new IllegalArgumentException("El porcentaje debe encontrarse entre 0% y 100%");
        }
        return PERCENTAGE_FORMATTER.format(percentage);
    }

    public static synchronized String formatCurrency(BigDecimal currency) {
        return CURRENCY_FORMATTER.format(currency);
    }
    
    public static synchronized String formatCurrency(Double currency) {
        return CURRENCY_FORMATTER.format(currency);
    }

    public static String formatCuitCentro(Integer cuitInteger) {
        if (cuitInteger == null) {
            return "";
        }
        return NRO_FACTURA_FORMATTER.format(cuitInteger);
    }

    public static String formatShort(Date date) {
        return DateFormat.getDateInstance(DateFormat.SHORT).format(date);
    }

    /**
     *
     * @param integer el entero que se desea convertir a String
     * @return the empty String if integer == null, integer.toString() de otra
     * manera
     */
    public static String toString(Integer integer) {
        return toString(integer, "");
    }

    public static String toString(Long longValue) {
        return toString(longValue, "");
    }

    public static String toString(Integer integer, String nullString) {
        return integer == null ? nullString : integer.toString();
    }

    public static String toString(Long integer, String nullString) {
        return integer == null ? nullString : integer.toString();
    }

    public void hide(JLabel jLabel) {
        jLabel.setText(jLabel.getText().replaceAll(".", " "));
    }

    public void hide(JTextField jTextField) {
        jTextField.setText("");
        jTextField.setBorder(null);
        jTextField.setBackground(new Color(238, 238, 238));
    }

    public static String formatFechaYHora(Date aFormatear) {
        if (aFormatear == null) {
            return "";
        }
        return DATE_FORMAT.format(aFormatear);
    }

    public static String formatTime(Date aFormatear){
        if (aFormatear == null) {
            return "";
        }
        return TIME_FORMAT.format(aFormatear);
    }
    
    public static void setCurrency(JTextField currencyField, BigDecimal monto) {
        if (currencyField.isFocusOwner()) {
            currencyField.setText(monto.toPlainString());
        } else {
            currencyField.setText(formatCurrency(monto));
        }
    }
    
    /**
     * String con la representacion de hh:mm de los minutos pasados. 
     * 
     * Ejemplo: 
     * 
     * formatHorasMinutos(65) => "01:05"
     * 
     * @param minutos los minutos a transformar en el formato hh:mm
     * @return un String con el formarto hh:mm
     * 
     */
    public static String formatHorasMinutos(int minutosAFormatear){
        int horas, minutos;
        horas = minutosAFormatear / 60;
        minutos = minutosAFormatear % 60;
        String retorno = "";
        if (horas < 10) {
           retorno = "0";
        }
        retorno = retorno + Integer.toString(horas);
        retorno = retorno + ":";
        if (minutos < 10) {
            retorno = retorno + "0";
        } 
        retorno = retorno + Integer.toString(minutos);
        return retorno;
    }
}