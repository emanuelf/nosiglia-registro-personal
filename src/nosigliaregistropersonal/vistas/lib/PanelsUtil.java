package nosigliaregistropersonal.vistas.lib;

import java.awt.Cursor;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class PanelsUtil {

    public static void disposePanel(JPanel panel) {
        Window window = SwingUtilities.getWindowAncestor(panel);
        window.dispose();
    }

    public static void showPanelOnModalDialog(Window padre, JPanel panel, WindowListener closingWindowListener, String titulo) {
        createDialog(padre, panel, closingWindowListener, titulo);
    }

    private static void createDialog(Window padre, JPanel panel, WindowListener closingWindowListener, String titulo) {
        JDialog dialog = createDialog(padre, panel, titulo);
        dialog.addWindowListener(closingWindowListener);
        dialog.setModal(true);
        dialog.setVisible(true);
    }

    private PanelsUtil() {
    }

    public static JDialog showPanelOnModalDialogConEscapeAction(Window padre, JPanel panel, String titulo) {
        agregarEscapeAction(panel);
        final JDialog dialog = createDialog(padre, panel, titulo);
        dialog.setModal(true);
        dialog.setVisible(true);
        return dialog;
    }

    public static JDialog showPanelOnModalDialog(Window padre, JPanel panel, String titulo) {
        JDialog dialog = createDialog(padre, panel, titulo);
        dialog.setModal(true);
        dialog.setVisible(true);
        return dialog;
    }

    public static void showPanelOnNormalDialog(Window padre, JPanel panel, String titulo) {
        JDialog dialog = createDialog(padre, panel, titulo);
        dialog.setModal(false);
        dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        dialog.setResizable(true);
        dialog.setVisible(true);
    }

    private static JDialog createDialog(Window padre, JPanel panel, String titulo) {
        JDialog dialog = new JDialog(padre);
        dialog.add(panel);
        dialog.pack();
        dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(null);
        dialog.setTitle(titulo);
        return dialog;
    }

    public static void agregarEscapeAction(final JPanel panel) {
        AbstractAction cerrar = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Window windowAncestor = SwingUtilities.getWindowAncestor(panel);
                windowAncestor.setVisible(false);
            }
        };
        panel.getActionMap().put("cerrar", cerrar);
        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, InputUtils.NO_MODIFIERS), "cerrar");
    }

    public static void setWaitCursor(JPanel panel) {
        panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    public static void setDefaultCursor(JPanel panel) {
        panel.setCursor(Cursor.getDefaultCursor());
    }
}