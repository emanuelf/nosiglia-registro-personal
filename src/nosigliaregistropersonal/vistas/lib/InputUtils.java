package nosigliaregistropersonal.vistas.lib;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import nosigliaregistropersonal.util.BigDecimalUtil;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

import nosigliaregistropersonal.vistas.lib.listeners.CurrencyFormattedFieldDecorator;
import nosigliaregistropersonal.vistas.lib.listeners.DecimalOnlyDocumentListener;
import nosigliaregistropersonal.vistas.lib.listeners.DecimalsOnlyIgnoreWhenNotFocussedDocumentListener;
import nosigliaregistropersonal.vistas.lib.listeners.IntegersOnlyDocumentListener;
import nosigliaregistropersonal.vistas.lib.listeners.LimitedLenghtFieldDecorator;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.MultipleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.SingleSelectionTableAdapter;
import nosigliaregistropersonal.vistas.lib.modeloTabla.adapters.TableAdapter;

public class InputUtils {

    public static final Date NO_DATE = null;
    public static final int NO_MODIFIERS = 0;
    public static final int NO_SELECTION_INDEX = -1;
    public static final int DEFAULT_SELECTION_INDEX = 0;
    private static final int DEFAULT_DIGITOS_PORCENTAJE = 4;
    private static final KeyStroke ADD_KEY_STROKE = KeyStroke.
            getKeyStroke(KeyEvent.VK_ADD, KeyEvent.CTRL_DOWN_MASK);
    private static final KeyStroke ADD_KEY_STROKE_2 = KeyStroke.
            getKeyStroke(KeyEvent.VK_PLUS, KeyEvent.CTRL_DOWN_MASK);
    private static final KeyStroke INSERT_KEY_STROKE = KeyStroke.
            getKeyStroke(KeyEvent.VK_INSERT, NO_MODIFIERS);
    private static final KeyStroke SUPRIMIR_KEY_STROKE = KeyStroke.
            getKeyStroke(KeyEvent.VK_DELETE, NO_MODIFIERS);
    private static final KeyStroke REMOVE_KEY_STROKE = KeyStroke.
            getKeyStroke(KeyEvent.VK_MINUS, KeyEvent.CTRL_DOWN_MASK);
    private static final KeyStroke REMOVE_KEY_STROKE_2 = KeyStroke.
            getKeyStroke(KeyEvent.VK_SUBTRACT, KeyEvent.CTRL_DOWN_MASK);
    private static final NumberFormat PERCENTAGE_FORMATTER = NumberFormat.getPercentInstance();
    private static final NumberFormat CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance();

    static {
        PERCENTAGE_FORMATTER.setMaximumFractionDigits(DEFAULT_DIGITOS_PORCENTAJE);
    }

    public static void addPressOnEnterListener(final JButton jButton) {
        jButton.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    jButton.doClick();
                }
            }
        });

    }

    private InputUtils() {
        throw new UnsupportedOperationException("Non instantiable");//not even through reflection
    }

    public static void addLimitedLengthListener(JTextField jTextField, int limite) {
        jTextField.addKeyListener(new LimitedLenghtFieldDecorator(limite));
    }

    /**
     * @param value
     * @return null si el valor es <code> null </code> o si es un string vacío,
     *         su valor numérico en otro caso.
     *
     * @throws NumberFormatException en caso de que el String no contenga un
     *                               número. se puede llamar a <code> isInteger(value)
     *                               </code> para saber si esta invocacion provocaria una excepcion
     */
    public static Integer parseInteger(String text) {
        if (text == null || text.isEmpty()) {
            return null;
        } else {
            return parseInt(text);
        }
    }

    /**
     * @param aValue
     * @return <code> true </code> si es seguro llamar a
     * <code> InputUtils.parseDouble </code> con <code> aValue </code>
     *         como argumento, <code> false </code> de otra manera
     */
    public static boolean isNumeric(Object aValue) {
        try {
            parseDouble(aValue);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * @param aValue
     * @param incluirCero si el cero se considera positivo o no.
     * @return <code> true </code> si es seguro llamar a
     * <code> InputUtils.parseDouble </code> con <code> aValue </code>
     *         como argumento, y el resultado de la llamada sera positivo,
     * <code> false </code> de otra manera. El cero se considera positivo
     *         si <code> incluirCero == true;
     *         </code>
     */
    public static boolean isPositiveNumeric(Object aValue, boolean incluirCero) {
        if (isNumeric(aValue)) {
            if (incluirCero) {
                return Math.signum(parseDouble(aValue)) >= 0;
            } else {
                return Math.signum(parseDouble(aValue)) > 0;
            }
        } else {
            return false;
        }
    }

    /**
     * @param aValue
     * @return <code> true </code> si es seguro llamar a
     * <code> InputUtils.parseInteger </code> con <code> aValue </code>
     *         como argumento, <code> false </code> de otra manera
     */
    public static boolean isInteger(Object aValue) {
        try {
            if (aValue != null && aValue.toString().equals("-")) {
                return true;
            }
            parseInt(aValue);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isPositiveInteger(Object aValue, boolean incluirCero) {
        if (aValue.toString().equals("-")) {
            return false;
        }
        if (isInteger(aValue)) {
            if (incluirCero) {
                return Integer.signum(parseInt(aValue)) >= 0;
            } else {
                return Integer.signum(parseInt(aValue)) == 1;
            }
        } else {
            return false;
        }
    }

    public static boolean parseBoolean(Object aValue) {
        if (aValue == null) {
            return false;
        } else {
            return Boolean.parseBoolean(aValue.toString());
        }
    }

    public static long parseLong(Object aValue) {
        return aValue == null || aValue.toString().isEmpty() ? 0 : Long.parseLong(aValue.toString());
    }

    public static void setPercentageFormatterFactory(JFormattedTextField jFTxtPorcentajeComision) {
        NumberFormatter defaultFormatter = new NumberFormatter(new DecimalFormat("#,##%"));
        NumberFormatter displayFormatter =
                new NumberFormatter(new DecimalFormat("#,##%"));
        NumberFormatter editFormatter = new NumberFormatter(new DecimalFormat("#.##"));
        // set their value classes
        defaultFormatter.setValueClass(BigDecimal.class);
        displayFormatter.setValueClass(BigDecimal.class);
        editFormatter.setValueClass(BigDecimal.class);
        // create and set the DefaultFormatterFactory
        DefaultFormatterFactory percentageFactory =
                new DefaultFormatterFactory(defaultFormatter, displayFormatter, editFormatter);
        jFTxtPorcentajeComision.setFormatterFactory(percentageFactory);

    }

    public static BigDecimal parsePercentage(Object value) {
        return parsePercentage(value == null ? "" : value.toString());
    }

    public static BigDecimal parsePercentage(String text) {
        try {
            return new BigDecimal(PERCENTAGE_FORMATTER.parse(text).toString());
        } catch (ParseException ex) {
            if (text == null || text.isEmpty()) {
                return BigDecimal.ZERO;
            } else {
                return new BigDecimal(text).divide(BigDecimalUtil.CIEN, 4, RoundingMode.HALF_UP);
            }
        }
    }

    public static BigDecimal parseCurrency(String text) {
        try {
            return new BigDecimal(CURRENCY_FORMATTER.parse(text).toString());
        } catch (ParseException ex) {
            return text == null || text.isEmpty() ? BigDecimal.ZERO : new BigDecimal(text);
        }
    }

    public static BigDecimal parseCurrency(Object object) {
        return parseCurrency(object == null ? "" : object.toString());
    }

    /**
     * @param value
     * @return 0 si el valor es <code> null </code> o su representacion en
     *         string esta vacía, su valor numérico en otro caso.
     * @throws NumberFormatException en caso de que el String no contenga un
     *                               número. se puede llamar a <code> isInteger(value)
     *                               </code> para saber si esta invocacion provocaria una excepcion
     */
    public static int parseInt(Object value) throws NumberFormatException {
        return value == null ? 0 : parseInt(value.toString());
    }

    private static int parseInt(String value) throws NumberFormatException {
        return value == null || value.isEmpty() ? 0 : Integer.parseInt(value);
    }

    /**
     * @param value
     * @return 0 si el valor es nulo o su representacion en string esta vacía,
     *         su valor numérico en otro caso.
     * @throws NumberFormatException en caso de que el String no contenga un
     *                               número.
     */
    private static double parseDouble(Object value) {
        return value == null ? 0 : parseDouble(value.toString());
    }

    private static double parseDouble(String string) {
        return string == null || string.isEmpty() ? 0 : Double.parseDouble(string);
    }

    public static void addDecimalListeners(JTextField decimalField, boolean incluirCero) {
        decimalField.getDocument().
                addDocumentListener(new DecimalOnlyDocumentListener(decimalField));
    }

    public static void addIntegerListeners(JTextField integerField, boolean incluirCero) {
        integerField.getDocument().
                addDocumentListener(new IntegersOnlyDocumentListener(integerField, incluirCero));
    }

    public static void addPermitenNegativosIntegerListeners(final JTextField integerField) {
        integerField.getDocument().
                addDocumentListener(new IntegersOnlyDocumentListener(integerField));
        integerField.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
                if (integerField.getText().equals("-")) {
                    integerField.setText("");
                }
            }
        });
    }

    public static void addCurrencyListeners(JTextField currencyField, boolean incluirCero) {
        currencyField.getDocument().
                addDocumentListener(new DecimalsOnlyIgnoreWhenNotFocussedDocumentListener(currencyField, incluirCero));
        currencyField.addFocusListener(new CurrencyFormattedFieldDecorator());
    }

    public static boolean porcentajeValido(BigDecimal aEvaluar) {
        return aEvaluar.max(BigDecimal.ZERO) == aEvaluar
                && aEvaluar.min(BigDecimalUtil.CIEN) == aEvaluar;
    }

    public static void addRemoveKeyStroke(JPanel panel, AbstractAction eliminarAction) {
        String eliminar = "Eliminar";
        panel.getActionMap().put(eliminar, eliminarAction);
        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(REMOVE_KEY_STROKE, eliminar);
        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(REMOVE_KEY_STROKE_2, eliminar);
    }

    public static void addAddKeyStroke(JPanel panel, AbstractAction agregarAction) {
        String agregar = "Agregar";
        panel.getActionMap().put(agregar, agregarAction);
        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(ADD_KEY_STROKE, agregar);
        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(ADD_KEY_STROKE_2, agregar);
    }

    public static <T> DefaultComboBoxModel<T> toDefaultComboBoxModel(Collection<T> collection) {
        return new DefaultComboBoxModel<>(new Vector<>(collection));
    }

    public static <T> DefaultComboBoxModel<T> toDefaultComboBoxModel(T... t) {
        return InputUtils.toDefaultComboBoxModel(Arrays.asList(t));
    }

    public static void restringirRangoFechas(final JXDatePicker jXDateInicio, final JXDatePicker jXDateFin) {
        restringirFin(jXDateInicio, jXDateFin);
        restringirInicio(jXDateInicio, jXDateFin);
    }

    private static void restringirFin(final JXDatePicker jXDateInicio, final JXDatePicker jXDateFin) {
        jXDateInicio.addPropertyChangeListener("date", new PropertyChangeListener() {

            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                Date newValue = (Date) evt.getNewValue();
                jXDateFin.getMonthView().setLowerBound(newValue);
            }
        });
    }

    private static void restringirInicio(final JXDatePicker jXDateInicio, final JXDatePicker jXDateFin) {
        jXDateFin.addPropertyChangeListener("date", new PropertyChangeListener() {

            @Override
            public void propertyChange(final PropertyChangeEvent evt) {
                Date newValue = (Date) evt.getNewValue();
                jXDateInicio.getMonthView().setUpperBound(newValue);
            }
        });
    }

    /**
     *
     * @param t los objetos a ingresar en la lista
     * @return una lista conteniendo los objetos del parametro.
     */
    public static <T> List<T> asArrayList(T... t) {
        return new ArrayList<>(Arrays.asList(t));
    }

    public static <T> void pasar(Collection<T> aCargar, DefaultListModel<T> origen, DefaultListModel<T> destino) {
        for (T aCarga : aCargar) {
            origen.removeElement(aCarga);
            if (!destino.contains(aCarga)) {
                destino.addElement(aCarga);
            }
        }
    }

    public static void selectAll(JList<?> jList) {
        jList.setSelectionInterval(0, jList.getModel().getSize() - 1);
    }

    public static void addTransferFocusOnActionListener(JTextField[] jTxtFields) {
        for (final JTextField jTxtField : jTxtFields) {
            jTxtField.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    jTxtField.transferFocus();
                }
            });
            jTxtField.addFocusListener(new FocusAdapter() {

                @Override
                public void focusGained(FocusEvent e) {
                    jTxtField.selectAll();
                }
            });
        }
    }

    public static void addInsertKeyStroke(JXTable panel, AbstractAction agregarAction) {
        String agregar = "Agregar";
        panel.getActionMap().put(agregar, agregarAction);
        panel.getInputMap(JXTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
                put(INSERT_KEY_STROKE, agregar);
    }

    public static void addSuprimirKeyStroke(JXTable tabla, Action elimininarAction) {
        String eliminar = "Eliminar";
        tabla.getActionMap().put(eliminar, elimininarAction);
        tabla.getInputMap(JXTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
                put(SUPRIMIR_KEY_STROKE, eliminar);
    }

    public static void addSuprimirKeyStroke(TableAdapter<?, ?> tableAdapter) {
        String eliminar = "Eliminar";
        tableAdapter.getTabla().getActionMap().
                put(eliminar, getEliminarElementosTablaAction(tableAdapter));
        tableAdapter.getTabla().getInputMap(JXTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
                put(SUPRIMIR_KEY_STROKE, eliminar);
    }

    public static void seleccionarCelda(JXTable jXTable, int columna, int fila) {
        jXTable.requestFocusInWindow();
        jXTable.changeSelection(fila, columna, false, false);
    }

    public static void quitarComportamientoDelEnterEnTabla(JTable tabla) {
        tabla.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.
                getKeyStroke(KeyEvent.VK_ENTER, 0), "");
    }

    public static BigDecimal parseBigDecimal(Object object, BigDecimal onNull) {
        if (object == null) {
            return onNull;
        }
        return new BigDecimal(object.toString());
    }

    public static AbstractAction addEliminarElementoActionAndKeyStroke(TableAdapter<?, ?> tableAdapter, JButton boton) {
        AbstractAction eliminarAction = getEliminarElementosTablaAction(tableAdapter);
        boton.setAction(eliminarAction);
        InputUtils.addSuprimirKeyStroke(tableAdapter.getTabla(), eliminarAction);
        boton.setToolTipText("Eliminar elemento (suprimir)");
        return eliminarAction;
    }

    public static AbstractAction getEliminarElementosTablaAction(final TableAdapter<?, ?> tableAdapter) {
        return new AbstractAction("", null) {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableAdapter instanceof MultipleSelectionTableAdapter) {
                    MultipleSelectionTableAdapter casteado = (MultipleSelectionTableAdapter) tableAdapter;
                    casteado.removeSelectedElements();
                }
                if (tableAdapter instanceof SingleSelectionTableAdapter) {
                    SingleSelectionTableAdapter casteado = (SingleSelectionTableAdapter) tableAdapter;
                    if (casteado.hasSelection()) {
                        casteado.removeSelectedElement();
                    }
                }
            }
        };
    }

    public static <T> void cargar(Collection<T> aCargar, JList<T> jList) {
        final DefaultListModel<T> model = (DefaultListModel<T>) jList.getModel();
        for (T carga : aCargar) {
            if (!model.contains(carga)) {
                model.addElement(carga);
            }
        }
    }

    public static void limpiar(JList<?> jList) {
        DefaultListModel<?> model = (DefaultListModel) jList.getModel();
        model.removeAllElements();
    }
}
