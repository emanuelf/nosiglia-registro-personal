package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.awt.Component;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

public class LineWrappingCellRenderer implements TableCellRenderer {

    private static final Map<JTable, LineWrappingCellRenderer> WEAK_HASH_MAP = new WeakHashMap<>();
    private final JTextArea jTextArea;
    private int previousRow = -1;

    private LineWrappingCellRenderer() {
        super();
        jTextArea = new JTextArea();
        jTextArea.setLineWrap(true);
        jTextArea.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    }

    private int countLines(int width) {
        int textWidth = jTextArea.getFontMetrics(jTextArea.getFont()).stringWidth(jTextArea.getText()) + 5;
        int lineasCompletas = textWidth / width;
        int lineaParcial = textWidth % width == 0 ? 0 : 1;
        return lineasCompletas + lineaParcial;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            jTextArea.setForeground(table.getSelectionForeground());
            jTextArea.setBackground(table.getSelectionBackground());
        } else {
            jTextArea.setForeground(table.getForeground());
            jTextArea.setBackground(table.getBackground());
        }
        jTextArea.setText((value == null) ? "" : value.toString());
        if (!jTextArea.getText().isEmpty()) {
            int newRowHeigth = table.getRowHeight() * countLines(table.getColumnModel().getColumn(column).getWidth());
            if (row == previousRow) {
                table.setRowHeight(row, Math.max(table.getRowHeight(row), newRowHeigth));
            } else {
                previousRow = row;
                table.setRowHeight(row, newRowHeigth);
            }
        }
        return jTextArea;
    }

    public static LineWrappingCellRenderer getLineWrappingCellRendererFor(JTable jTable) {
        if (!WEAK_HASH_MAP.containsKey(jTable)) {
            WEAK_HASH_MAP.put(jTable, new LineWrappingCellRenderer());
        }
        return WEAK_HASH_MAP.get(jTable);
    }
}