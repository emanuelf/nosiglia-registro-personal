package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.awt.Toolkit;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class FormatCellRenderer extends DefaultTableCellRenderer {

    private final Format FORMATTER;

    public FormatCellRenderer(Format FORMATTER) {
        this.FORMATTER = FORMATTER;
    }

    @Override
    protected void setValue(Object value) {
        try {
            setText(FORMATTER.format(value));
        } catch (IllegalArgumentException ex) {
            setText("");
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public static TableCellRenderer dateCellRenderer() {
        return new FormatCellRenderer(DateFormat.getDateInstance());
    }

    public static TableCellRenderer timeCellRenderer() {
        return new FormatCellRenderer(DateFormat.getTimeInstance());
    }

    public static TableCellRenderer dateCellRenderer(int style) {
        return new FormatCellRenderer(DateFormat.getDateInstance(style));
    }

    public static TableCellRenderer timeCellRenderer(int style) {
        return new FormatCellRenderer(DateFormat.getTimeInstance(style));
    }

    public static TableCellRenderer currencyCellRenderer() {
        return new FormatCellRenderer(NumberFormat.getCurrencyInstance()) {

            {
                setHorizontalAlignment(JLabel.TRAILING);
            }
        };
    }

    public static TableCellRenderer percentageCellRenderer(int decimales) {
        final NumberFormat percentInstance = NumberFormat.getPercentInstance();
        percentInstance.setMaximumFractionDigits(decimales);
        percentInstance.setRoundingMode(RoundingMode.HALF_UP);
        return new FormatCellRenderer(percentInstance);
    }
}
