package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.awt.Component;
import java.math.BigDecimal;
import java.text.Format;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ImporteCellRenderer extends DefaultTableCellRenderer {

    private static final Format CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance();

    public ImporteCellRenderer() {
        setHorizontalAlignment(JLabel.TRAILING);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        final Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (isSelected) {
            tableCellRendererComponent.setBackground(table.getSelectionBackground());
            tableCellRendererComponent.setForeground(table.getSelectionForeground());
        } else {
            tableCellRendererComponent.setBackground(table.getBackground());
            tableCellRendererComponent.setForeground(table.getForeground());
        }
        return tableCellRendererComponent;
    }

    @Override
    protected void setValue(Object value) {
        if (value instanceof Integer) {
            setText((value == null) ? "" : CURRENCY_FORMATTER.format((Integer) value));
        } else {
            setText((value == null) ? "" : CURRENCY_FORMATTER.format((BigDecimal) value));
        }
    }
}