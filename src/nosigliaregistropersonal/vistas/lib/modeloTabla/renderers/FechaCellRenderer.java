package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.text.DateFormat;
import java.text.Format;
import java.util.Date;

import javax.swing.table.DefaultTableCellRenderer;

public class FechaCellRenderer extends DefaultTableCellRenderer {

    private static final Format DATE_FORMATTER = DateFormat.getDateInstance();

    @Override
    protected void setValue(Object value) {
        if (value instanceof Date) {
            Date fecha = (Date) value;
            setText(format(fecha));
        } else {
            if (value instanceof Date) {
                setText(format((Date) value));
            } else {
                if (value != null) {
                    throw new ClassCastException("El objeto es del tipo equivocado: " + value.getClass());
                }
            }
        }
    }

    private static synchronized String format(Date fecha) {
        return DATE_FORMATTER.format(fecha);
    }
}