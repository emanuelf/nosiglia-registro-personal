package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

public class LinkCellRendererUtils {
//
//    private static JFrmSistema frmSistema;
//    private static final String AVISO_GUARDAR_O_CANCELAR = "Debe guardar o cancelar los cambios antes para continuar.";
//
//    private LinkCellRendererUtils() {
//        throw new UnsupportedOperationException("Non instantiable.");
//    }
//
//    public static void addCreditoCellRenderer(final JXTable jXTable, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.getColumn(column).setCellRenderer(new CreditoCellRenderer(SwingUtilities.getWindowAncestor(jXTable)));
//            }
//        });
//    }
//
//    public static void addCreditoCellRenderer(final JXTable jXTable, final Class<?> clase) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.setDefaultRenderer(clase, new CreditoCellRenderer(SwingUtilities.getWindowAncestor(jXTable)));
//            }
//        });
//    }
//
//    public static void addDebitoCellRenderer(final JXTable jXTable, final int column, final boolean soloMostrar) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.getColumn(column).setCellRenderer(new DebitoCellRenderer(jXTable, soloMostrar));
//            }
//        });
//    }
//
//    public static void addDebitoCellRenderer(final JXTable jXTable, final Class<?> clase, final boolean soloMostrar) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.setDefaultRenderer(clase, new DebitoCellRenderer(jXTable, soloMostrar));
//            }
//        });
//    }
//
//    public static void addPedidoCellRenderer(final JXTable jXTable, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.getColumn(column).setCellRenderer(new PedidoCellRenderer(jXTable));
//            }
//        });
//    }
//
//    public static void addPedidoCellRenderer(final JXTable jXTable) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.setDefaultRenderer(Pedido.class, new PedidoCellRenderer(jXTable));
//            }
//        });
//    }
//
//    public static void addOrdenDePagoCellRenderer(final JXTable table, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                table.getColumn(column).setCellRenderer(new OrdenDePagoCellRenderer(table));
//            }
//        });
//    }
//
//    public static void addComprobanteProveedorCellRenderer(final JXTable jXTable, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                jXTable.getColumn(column).setCellRenderer(new ComprobanteProveedorCellRenderer(jXTable));
//            }
//        });
//    }
//
//    public static void addRendicionRepartoCellRenderer(final JXTable tabla, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                tabla.getColumn(column).setCellRenderer(new RendicionRepartoCellRenderer(tabla));
//            }
//        });
//    }
//
//    public static void addRendicionCobranzaCellRenderer(final JXTable tabla, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                tabla.getColumn(column).setCellRenderer(new RendicionCobranzaCellRenderer(tabla));
//            }
//        });
//    }
//
//    public static void addCobranzaCellRenderer(final JXTable tabla, final int column) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                tabla.getColumn(column).setCellRenderer(new CobranzaCellRenderer(tabla));
//            }
//        });
//    }
//
//    public static void addComprobanteClienteCellRenderer(final JXTable tabla) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                tabla.setDefaultRenderer(ComprobanteCliente.class, new ComprobanteClienteCellRenderer(tabla));
//            }
//        });
//    }
//
//    public static void addCajaRendicionCellRenderer(final JXTable tabla) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                tabla.setDefaultRenderer(CajaRendicion.class, new CajaRendicionCellRenderer(tabla));
//            }
//        });
//    }
//
//    private static class CreditoCellRenderer extends DefaultTableRenderer {
//
//        CreditoCellRenderer(final Window window) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(window)));
//        }
//
//        private static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<Credito> {
//
//            private static final JPnlAbmNotaCreditoVisor VISOR_ABM_NOTA_CREDITO = new JPnlAbmNotaCreditoVisor();
//            private static final JPnlAbmRecibosVisor VISOR_ABM_RECIBOS = new JPnlAbmRecibosVisor();
//
//            AbstractHyperlinkActionImpl(Window window) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) window;
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                Credito credito = getTarget();
//                if (credito instanceof NotaDeCredito) {
//                    frmSistema.show(VISOR_ABM_NOTA_CREDITO, "Detalle de nota de crédito " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_NOTA_CREDITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                    } else {
//                        VISOR_ABM_NOTA_CREDITO.cargar((NotaDeCredito) credito);
//                    }
//                }
//                if (credito instanceof Recibo) {
//                    frmSistema.show(VISOR_ABM_RECIBOS, "Detalle de recibo " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_RECIBOS.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                    } else {
//                        VISOR_ABM_RECIBOS.cargar((Recibo) credito);
//                    }
//                }
//            }
//        }
//    }
//
//    private static class DebitoCellRenderer extends DefaultTableRenderer {
//
//        DebitoCellRenderer(final JXTable tabla, final boolean soloMostrar) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(tabla, soloMostrar)));
//        }
//
//        private static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<Debito> {
//
//            private static final JPnlAbmFacturaVisor VISOR_ABM_FACTURA = new JPnlAbmFacturaVisor();
//            private static final JPnlAbmNotaDebitoVisor VISOR_ABM_NOTA_DEBITO = new JPnlAbmNotaDebitoVisor();
//            private final boolean soloMostrar;
//
//            AbstractHyperlinkActionImpl(JXTable tabla, boolean soloMostrar) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(tabla);
//                }
//                this.soloMostrar = soloMostrar;
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                final Debito debito = getTarget();
//                if (debito.getClass().equals(Factura.class)) {
//                    frmSistema.show(VISOR_ABM_FACTURA, "Detalle de factura " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_FACTURA.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                    } else {
//                        if (soloMostrar) {
//                            VISOR_ABM_FACTURA.mostrar((Factura) debito);
//                        } else {
//                            VISOR_ABM_FACTURA.cargar((Factura) debito);
//                        }
//                    }
//                }
//                if (debito instanceof NotaDeDebito) {
//                    frmSistema.show(VISOR_ABM_NOTA_DEBITO, "Detalle de nota de débito " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_NOTA_DEBITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                    } else {
//                        if (soloMostrar) {
//                            VISOR_ABM_NOTA_DEBITO.mostrar((NotaDeDebito) debito);
//                        } else {
//                            VISOR_ABM_NOTA_DEBITO.cargar((NotaDeDebito) debito);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    private static class PedidoCellRenderer extends DefaultTableRenderer {
//
//        PedidoCellRenderer(final JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        private static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<Pedido> {
//
//            private static final JPnlAbmPedidoVisor VISOR_ABM_PEDIDO = new JPnlAbmPedidoVisor();
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(VISOR_ABM_PEDIDO, "Detalle de pedido nro " + getTarget().getNro());
//                if (VISOR_ABM_PEDIDO.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    VISOR_ABM_PEDIDO.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class OrdenDePagoCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmOrdenPagoVisor VISOR__ABM_ORDEN_DE_PAGO = new JPnlAbmOrdenPagoVisor();
//
//        OrdenDePagoCellRenderer(final JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<OrdenDePago> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(VISOR__ABM_ORDEN_DE_PAGO, "Detalle de orden de pago nro " + getTarget().getNro());
//                if (VISOR__ABM_ORDEN_DE_PAGO.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    VISOR__ABM_ORDEN_DE_PAGO.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class ComprobanteProveedorCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmCompraVisor VISOR_ABM_COMPRAS = new JPnlAbmCompraVisor();
//        private static final JPnlAbmNotaCreditoProveedorVisor VISOR_ABM_NOTAS_DE_CREDITO = new JPnlAbmNotaCreditoProveedorVisor();
//        private static final JPnlAbmNotaDebitoProveedorVisor VISOR_ABM_NOTAS_DEBITO = new JPnlAbmNotaDebitoProveedorVisor();
//
//        ComprobanteProveedorCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<ComprobanteProveedor> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (getTarget() instanceof Compra) {
//                    frmSistema.show(VISOR_ABM_COMPRAS, "Detalle de compra " + getTarget().getNroComprobante() + ", proveedor " + getTarget().getProveedor());
//                    if (VISOR_ABM_COMPRAS.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_COMPRAS.cargar((Compra) getTarget());
//                    }
//                }
//                if (getTarget() instanceof NotaDeCreditoProveedor) {
//                    frmSistema.show(VISOR_ABM_NOTAS_DE_CREDITO, "Detalle de nc " + getTarget().getNroComprobante() + ", proveedor " + getTarget().getProveedor());
//                    if (VISOR_ABM_NOTAS_DE_CREDITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_NOTAS_DE_CREDITO.cargar((NotaDeCreditoProveedor) getTarget());
//                    }
//                }
//                if (getTarget() instanceof NotaDebitoProveedor) {
//                    frmSistema.show(VISOR_ABM_NOTAS_DEBITO, "Detalle de nd " + getTarget().getNroComprobante() + ", proveedor " + getTarget().getProveedor());
//                    if (VISOR_ABM_NOTAS_DEBITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_NOTAS_DEBITO.cargar((NotaDebitoProveedor) getTarget());
//                    }
//                }
//            }
//        }
//    }
//
//    private static class RendicionRepartoCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmRendicionRepartoVisor VISOR_ABM_RENDICION_REPARTO = new JPnlAbmRendicionRepartoVisor();
//
//        RendicionRepartoCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<RendicionReparto> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(VISOR_ABM_RENDICION_REPARTO, "Detalle de rendición reparto nro " + getTarget().getNro());
//                if (VISOR_ABM_RENDICION_REPARTO.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    VISOR_ABM_RENDICION_REPARTO.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class RendicionCobranzaCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmRendicionCobranzaVisor VISOR_ABM_RENDICION_COBRANZA = new JPnlAbmRendicionCobranzaVisor();
//
//        RendicionCobranzaCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<RendicionCobranza> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(VISOR_ABM_RENDICION_COBRANZA, "Detalle de rendición de cobranza nro " + getTarget().getNro());
//                if (VISOR_ABM_RENDICION_COBRANZA.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    VISOR_ABM_RENDICION_COBRANZA.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class CajaRendicionCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmCajaRendicion JPACR = new JPnlAbmCajaRendicion();
//
//        CajaRendicionCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<CajaRendicion> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(JPACR, "Detalle de caja nro " + getTarget().getNro());
//                if (JPACR.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    JPACR.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class CobranzaCellRenderer extends DefaultTableRenderer {
//
//        private static final JPnlAbmCobranzaVisor VISOR_ABM_COBRANZA = new JPnlAbmCobranzaVisor();
//
//        CobranzaCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<Cobranza> {
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frmSistema.show(VISOR_ABM_COBRANZA, "Detalle de cobranza nro " + getTarget().getNro());
//                if (VISOR_ABM_COBRANZA.estaEditando()) {
//                    MessageUtils.showWarning(frmSistema, "Debe guardar o cancelar los cambios antes para continuar.");
//                } else {
//                    VISOR_ABM_COBRANZA.cargar(getTarget());
//                }
//            }
//        }
//    }
//
//    private static class JPnlAbmFacturaVisor extends JPnlAbmFactura {
//
//        JPnlAbmFacturaVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//        }
//    }
//
//    private static class JPnlAbmNotaDebitoVisor extends JPnlAbmNotaDebito {
//
//        JPnlAbmNotaDebitoVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//        }
//    }
//
//    private static class JPnlAbmPedidoVisor extends JPnlAbmPedido {
//
//        JPnlAbmPedidoVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//            notifyChanged(getWorkingCopy());
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//            notifyChanged(getWorkingCopy());
//        }
//    }
//
//    private static class JPnlAbmCompraVisor extends JPnlAbmCompras {
//
//        JPnlAbmCompraVisor() {
//            deshabilitarNavegacionPermanentemente();
//            deshabilitarAnularPermanentemente();
//            deshabilitarImpresionPermanentemente();
//        }
//    }
//
//    private static class JPnlAbmNotaCreditoProveedorVisor extends JPnlAbmNotaCreditoProveedor {
//
//        JPnlAbmNotaCreditoProveedorVisor() {
//            deshabilitarNavegacionPermanentemente();
//            deshabilitarAnularPermanentemente();
//            deshabilitarImpresionPermanentemente();
//        }
//    }
//
//    private static class JPnlAbmNotaDebitoProveedorVisor extends JPnlAbmNotaDebitoProveedor {
//
//        JPnlAbmNotaDebitoProveedorVisor() {
//            deshabilitarNavegacionPermanentemente();
//            deshabilitarAnularPermanentemente();
//            deshabilitarImpresionPermanentemente();
//        }
//    }
//    private static final Map<Pedido, AbmPedidoListener> interesados = new HashMap<>();
//
//    public interface AbmPedidoListener {
//
//        public void updated(Pedido pedido);
//    }
//
//    public static void notifyInterest(Collection<Pedido> elementos, AbmPedidoListener abmListener) {
//        for (Iterator<Map.Entry<Pedido, AbmPedidoListener>> it = interesados.entrySet().iterator(); it.hasNext();) {
//            Map.Entry<Pedido, AbmPedidoListener> entry = it.next();
//            if (entry.getValue() == abmListener) {
//                it.remove();
//            }
//        }
//        for (Pedido e : elementos) {
//            interesados.put(e, abmListener);
//        }
//    }
//
//    private static void notifyChanged(Pedido o) {
//        AbmPedidoListener interesado = interesados.get(o);
//        if (interesado != null) {
//            interesado.updated(o);
//        }
//    }
//
//    private static class JPnlAbmNotaCreditoVisor extends JPnlAbmNotaCredito {
//
//        JPnlAbmNotaCreditoVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//        }
//    }
//
//    private static class JPnlAbmRecibosVisor extends JPnlAbmRecibos {
//
//        JPnlAbmRecibosVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//        }
//    }
//
//    private static class JPnlAbmOrdenPagoVisor extends JPnlAbmOrdenPago {
//
//        JPnlAbmOrdenPagoVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//
//        @Override
//        public void guardar() {
//            super.guardar();
//        }
//
//        @Override
//        public void cancelar() {
//            super.cancelar();
//        }
//    }
//
//    private static class JPnlAbmRendicionRepartoVisor extends JPnlAbmRendicionReparto {
//
//        JPnlAbmRendicionRepartoVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//    }
//
//    private static class JPnlAbmRendicionCobranzaVisor extends JPnlAbmRendicionCobranza {
//
//        JPnlAbmRendicionCobranzaVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//    }
//
//    private static class JPnlAbmCobranzaVisor extends JPnlAbmCobranzas {
//
//        JPnlAbmCobranzaVisor() {
//            deshabilitarNavegacionPermanentemente();
//        }
//    }
//
//    private static class ComprobanteClienteCellRenderer extends DefaultTableRenderer {
//
//        ComprobanteClienteCellRenderer(JXTable table) {
//            super(new HyperlinkProvider(new AbstractHyperlinkActionImpl(table)));
//        }
//
//        static class AbstractHyperlinkActionImpl extends AbstractHyperlinkAction<ComprobanteCliente> {
//
//            private static final JPnlAbmFacturaVisor VISOR_ABM_FACTURA = new JPnlAbmFacturaVisor();
//            private static final JPnlAbmNotaCreditoVisor VISOR_ABM_NOTA_DE_CREDITO = new JPnlAbmNotaCreditoVisor();
//            private static final JPnlAbmNotaDebitoVisor VISOR_ABM_NOTA_DE_DEBITO = new JPnlAbmNotaDebitoVisor();
//
//            AbstractHyperlinkActionImpl(JXTable table) {
//                if (frmSistema == null) {
//                    frmSistema = (JFrmSistema) SwingUtilities.getWindowAncestor(table);
//                }
//            }
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (getTarget() instanceof Factura) {
//                    frmSistema.show(VISOR_ABM_FACTURA, "Detalle de factura " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_FACTURA.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_FACTURA.cargar((Factura) getTarget());
//                    }
//                }
//                if (getTarget() instanceof NotaDeCredito) {
//                    frmSistema.show(VISOR_ABM_NOTA_DE_CREDITO, "Detalle de factura" + getTarget().getNroComprobante());
//                    if (VISOR_ABM_NOTA_DE_CREDITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_NOTA_DE_CREDITO.cargar((NotaDeCredito) getTarget());
//                    }
//                }
//                if (getTarget() instanceof NotaDeDebito) {
//                    frmSistema.show(VISOR_ABM_NOTA_DE_DEBITO, "Detalle de nd " + getTarget().getNroComprobante());
//                    if (VISOR_ABM_NOTA_DE_DEBITO.estaEditando()) {
//                        MessageUtils.showWarning(frmSistema, AVISO_GUARDAR_O_CANCELAR);
//                    } else {
//                        VISOR_ABM_NOTA_DE_DEBITO.cargar((NotaDeDebito) getTarget());
//                    }
//                }
//            }
//        }
//    }
//
//    public static void openWebpage(URI uri) {
//        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
//        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
//            try {
//                desktop.browse(uri);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public static void openWebpage(URL url) {
//        try {
//            openWebpage(url.toURI());
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//    }
}
