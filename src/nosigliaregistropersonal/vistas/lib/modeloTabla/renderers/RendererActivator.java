package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import org.jdesktop.swingx.JXTable;

public interface RendererActivator {

    public boolean shouldActivateRenderer(JXTable jTable, int row, int column);
}
