package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public abstract class SumRendererDecorator implements TableCellRenderer {

    private final TableCellRenderer originalRenderer;
    private final RendererActivator rendererActivator;

    public SumRendererDecorator(RendererActivator rendererActivator) {
        this.rendererActivator = rendererActivator;
        originalRenderer = new DefaultTableCellRenderer();
    }

    public SumRendererDecorator(RendererActivator rendererActivator, TableCellRenderer originalRenderer) {
        this.rendererActivator = rendererActivator;
        this.originalRenderer = originalRenderer;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (shouldAgregate(row, column)) {
            return originalRenderer.getTableCellRendererComponent(table, agregate(table, row, column), isSelected, hasFocus, row, column);
        } else {
            return originalRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

    public abstract Object agregate(JTable table, int row, int column);

    public final boolean shouldAgregate(int row, int column) {
        return rendererActivator.shouldActivateRenderer(null, row, column);
    }
}
