package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.swing.table.DefaultTableCellRenderer;

public class PercentageCellRenderer extends DefaultTableCellRenderer {

    private final NumberFormat format;

    public PercentageCellRenderer(int decimales) {
        NumberFormat porcentaje = NumberFormat.getPercentInstance();
        porcentaje.setMaximumFractionDigits(decimales);
        format = porcentaje;
    }

    @Override
    protected void setValue(Object value) {
        BigDecimal valor = null == value ? BigDecimal.ZERO : (BigDecimal) value;
        setText((value == null) ? "" : format.format(valor));
    }
}
