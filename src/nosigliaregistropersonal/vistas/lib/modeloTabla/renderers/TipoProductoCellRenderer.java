package nosigliaregistropersonal.vistas.lib.modeloTabla.renderers;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TipoProductoCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        String realValue = value == null ? "" : Boolean.parseBoolean(value.toString()) ? "Bulto" : "Unidad";
        return super.getTableCellRendererComponent(table, realValue, isSelected, hasFocus, row, column);
    }
}
