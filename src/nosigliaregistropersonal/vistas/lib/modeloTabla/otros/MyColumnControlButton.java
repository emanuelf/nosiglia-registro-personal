package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.List;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.MenuSelectionManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicCheckBoxMenuItemUI;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionContainerFactory;
import org.jdesktop.swingx.table.ColumnControlButton;
import org.jdesktop.swingx.table.ColumnControlButton.DefaultColumnControlPopup;
import org.jdesktop.swingx.table.ColumnControlPopup;

//blatantly stolen from http://www.coding-dude.com/wp/java/custom-columncontrolbutton-java-swing-jxtable/ 
//and modified just a little to meet my needs.
public class MyColumnControlButton extends ColumnControlButton {

    private DefaultColumnControlPopup defaultColumnControlPopup;

    public MyColumnControlButton(JXTable table) {
        super(table);
    }

    /*
     * This modifies the popup menu that hides/shows table
     * columns such that the popup is kept open even after a column is clicked.
     * The popup will close if the ColumnControlButton is pressed again.
     */
    @Override
    protected ColumnControlPopup createColumnControlPopup() {
        return getDefaultColumnControlPopup();
    }

    public synchronized DefaultColumnControlPopup getDefaultColumnControlPopup() {
        if (defaultColumnControlPopup == null) {
            defaultColumnControlPopup = new DefaultColumnControlPopup() {

                @Override
                public void addVisibilityActionItems(List<? extends AbstractActionExt> actions) {

                    ActionContainerFactory factory = new ActionContainerFactory(null);
                    for (Action action : actions) {
                        JMenuItem mi = factory.createMenuItem(action);
                        mi.setUI(new MyCheckBoxMenuItemUI());
                        addItem(mi);
                    }
                }
            };
        }
        return defaultColumnControlPopup;
    }

    //we do a simple override or the code executed when a column selected for
    //showing or hiding
    public static class MyCheckBoxMenuItemUI extends BasicCheckBoxMenuItemUI {

        public static ComponentUI createUI(JComponent c) {
            return new MyCheckBoxMenuItemUI();
        }

        synchronized protected void doClick(MenuSelectionManager msm) {
            menuItem.doClick(0);
        }
    }
}
