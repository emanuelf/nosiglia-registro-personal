package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.Collection;
import java.util.List;

import javax.swing.table.TableModel;

public interface ICustomTableModel<T extends Vectorizable> extends TableModel {

    /**
     * Añade un valor al final del modelo.
     *
     * @param value el valor a agregar al modelo
     * @return true si fue agregado el elemento, false de otra manera.
     */
    boolean addRow(T value);

    /**
     * Añade un valor a la posicion especificada.
     *
     * @param value el valor a agregar al modelo
     * @param index la posicion en que se desea agregar el valor
     * @return true si fue agregado el elemento, false de otra manera.
     * @throws IndexOutOfBoundsException si el indice es mayor a getRowCount()
     */
    boolean addRow(int index, T value);

    /**
     * Añade una coleccion de valores al final del modelo.
     *
     * @param values la coleccion de valores a agregar al modelo
     * @return true si el modelo se modifico debido a esta llamada.
     */
    boolean addRows(Collection<T> values);

    /**
     * Lleva al modelo a su estado inicial, podria aun contener uno o más
     * elementos.
     */
    void clear();

    /**
     * @return La lista de elementos que conforman el modelo.
     */
    List<T> getDatos();

    /**
     * Elimina la fila especificada.
     *
     * @param row la fila a eliminar.
     */
    T removeRow(int row);

    /**
     * Lleva al modelo a su estado inicial y luego añade todos los elementos
     * presentes en datos.
     *
     * @param datos los valores que deben figurar en el modelo
     */
    void setDatos(Collection<T> datos);

    /**
     * Reemplaza el valor presente en index con value.
     *
     * @param index el indice a reemplazar.
     * @param value el valor a insertar en la position <code>index</code>.
     */
    void setRow(int index, T value);

    public void fireTableDataChanged();
}
