package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SortOrder;
import javax.swing.table.TableModel;

public class FirstColumnAllwaysSortedRowSorter extends NullTableRowSorter {

    private static final int ALLWAYS_SORTED_COLUMN = 0;

    public FirstColumnAllwaysSortedRowSorter(TableModel model) {
        super(model);
        setMaxSortKeys(2);
        setSortsOnUpdates(true);
    }

    @Override
    public List<SortKey> getSortKeys() {
        List<SortKey> sortKeys = new ArrayList<>(super.getSortKeys());
        int principalIndex = indexOfSortKeyFor(ALLWAYS_SORTED_COLUMN);
        if (principalIndex == -1) {
            sortKeys.add(0, new SortKey(ALLWAYS_SORTED_COLUMN, SortOrder.ASCENDING));
        } else {
            Collections.swap(sortKeys, 0, principalIndex);
        }
        return Collections.unmodifiableList(sortKeys);
    }

    private int indexOfSortKeyFor(int column) {
        for (int currentIndex = 0; currentIndex < super.getSortKeys().size(); currentIndex++) {
            SortKey sortKey = super.getSortKeys().get(currentIndex);
            if (sortKey.getColumn() == column) {
                return currentIndex;
            }
        }
        return -1;
    }

    @Override
    public void toggleSortOrder(int column) {
        int indexOfSortKeyForColumn = indexOfSortKeyFor(column);
        if (indexOfSortKeyForColumn == -1) {
            super.toggleSortOrder(column);
        } else {
            List<SortKey> sortKeys = new ArrayList<>(super.getSortKeys());
            SortKey original = sortKeys.get(indexOfSortKeyForColumn);
            SortKey reverted = revert(original);
            sortKeys.set(indexOfSortKeyForColumn, reverted);
            setSortKeys(sortKeys);
        }
    }

    private SortKey revert(SortKey sortKey) {
        if (SortOrder.ASCENDING.equals(sortKey.getSortOrder())) {
            return new SortKey(sortKey.getColumn(), SortOrder.DESCENDING);
        } else {
            return new SortKey(sortKey.getColumn(), SortOrder.ASCENDING);
        }
    }
}
