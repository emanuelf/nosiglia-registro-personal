package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.math.BigDecimal;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.SortOrder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class NullTableRowSorter extends TableRowSorter<TableModel> {

    private static final EmptyFixedStringComparator NUMBER_COMPARATOR = new EmptyFixedStringComparator(new StringAsBigDecimalComparator());
    private static final EmptyFixedStringComparator BOOLEAN_COMPARATOR = new EmptyFixedStringComparator(new StringAsBooleanComparator());
    private static final EmptyFixedStringComparator INVERTED_BOOLEAN_COMPARATOR = new EmptyFixedStringComparator(new StringAsBooleanInvertedComparator());
    private static final EmptyFixedStringComparator STRING_COMPARATOR = new EmptyFixedStringComparator(new CollatorComparator());

    public NullTableRowSorter(TableModel model) {
        super(model);
    }

    @Override
    protected boolean useToString(int column) {
        return true;
    }

    @Override
    public void toggleSortOrder(int column) {
        if (column >= 0 && column < getModelWrapper().getColumnCount() && isSortable(column)) {
            List< SortKey> keys = new ArrayList<>(getSortKeys());
            if (!keys.isEmpty()) {
                SortKey sortKey = keys.get(0);
                if (sortKey.getColumn() == column && sortKey.getSortOrder() == SortOrder.DESCENDING) {
                    setSortKeys(null);
                    return;
                }
            }
        }
        super.toggleSortOrder(column);
    }

    /**
     * @return un comparator que compara strings como numeros. El comparador
     * puede tirar NumberFormatException si alguno de los argumentos pasados no
     * representan numeros.
     */
    public Comparator<String> createNumberComparator() {
        return NUMBER_COMPARATOR;
    }

    /**
     * @return un comparator que compara strings como booleans.
     */
    public Comparator<String> createBooleanComparator() {
        return BOOLEAN_COMPARATOR;
    }

    /**
     * @return un comparator que compara strings como booleans, pero devuelve el
     * resultado invertido.
     */
    public Comparator<String> createInverseBooleanComparator() {
        return INVERTED_BOOLEAN_COMPARATOR;
    }

    /**
     * @return un comparator que compara strings de acuerdo al Locale por
     * defecto
     */
    public Comparator<String> createStringComparator() {
        return STRING_COMPARATOR;
    }

    private static class EmptyFixedStringComparator implements Comparator<String> {

        private Comparator<String> normalComparator;

        EmptyFixedStringComparator(Comparator<String> normalComparator) {
            this.normalComparator = normalComparator;
        }

        @Override
        public int compare(String o1, String o2) {
            if (o1.isEmpty() || o2.isEmpty()) {
                return 0;
            } else {
                return normalComparator.compare(o1, o2);
            }
        }
    }

    private static class StringAsBigDecimalComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return new BigDecimal(o1).compareTo(new BigDecimal(o2));
        }
    }

    private static class StringAsBooleanComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return Boolean.compare(Boolean.parseBoolean(o1), Boolean.parseBoolean(o2));
        }
    }

    private static class StringAsBooleanInvertedComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return Boolean.compare(Boolean.parseBoolean(o2), Boolean.parseBoolean(o1));
        }
    }

    private static class CollatorComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return Collator.getInstance().compare(o1, o2);
        }
    }
}
