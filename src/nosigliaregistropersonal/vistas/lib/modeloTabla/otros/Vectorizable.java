package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.List;

/**
 * representa una fila de una tabla en un ICustomTableModel
 */
public interface Vectorizable {

    /**
     * @return una lista con los valores presentes en esta fila en orden de
     * visualizacion
     */
    public List<Object> asVector();

    /**
     * modifica el valor de una columna a aValue si es posible.
     *
     * @param column la columna cuyo valor se desea modificar
     * @param aValue el valor que debe tomar la columna
     */
    public void setValueAt(int column, Object aValue);

    /**
     * @return true si la columna <code> column </code> es editable, false de
     * otra manera
     */
    public boolean isCellEditable(int column);
}
