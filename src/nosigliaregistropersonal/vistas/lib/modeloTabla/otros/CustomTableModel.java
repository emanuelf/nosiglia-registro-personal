package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Un TableModel con una interfaz similar a la de una lista. Esta es la
 * implementacion más basica de {@link ICustomTableModel}
 *
 * @param <T> el tipo de las filas de la tabla.
 */
public class CustomTableModel<T extends Vectorizable> extends AbstractTableModel implements ICustomTableModel<T> {

    private String[] columnas; // el nombre de las columnas de las tablas de las vistas
    private final List<T> datos; //Las instancias de T (Vectorizables en nuestro caso) que se cargan en las tablas de las vistas

    /**
     * usa un ArrayList como lista de Datos
     *
     * @param columnas los labels de la cabecera de la tabla
     */
    public CustomTableModel(String[] columnas) {
        this.columnas = Arrays.copyOf(columnas, columnas.length);
        datos = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return datos.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnas[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (datos.isEmpty()) {
            return Object.class;
        } else {
            Object data = datos.get(0).asVector().get(columnIndex);
            return data == null ? Object.class : data.getClass();
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return datos.get(rowIndex).isCellEditable(columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        T get = datos.get(rowIndex);
        return get.asVector().get(columnIndex);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        datos.get(rowIndex).setValueAt(columnIndex, aValue);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    @Override
    public boolean addRow(T value) {
        datos.add(value);
        fireTableRowsInserted(datos.size() - 1, datos.size() - 1);
        return true;
    }

    @Override
    public boolean addRow(int index, T value) {
        datos.add(index, value);
        fireTableRowsInserted(index, index);
        return true;
    }

    @Override
    public boolean addRows(Collection<T> values) {
        if (datos.isEmpty()) {
            setDatos(values);
            return true;
        } else {
            if (datos.addAll(values)) {
                fireTableDataChanged();
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void setRow(int index, T value) {
        datos.set(index, value);
    }

    @Override
    public T removeRow(int row) {
        T removed = datos.remove(row);
        fireTableRowsDeleted(row, row);
        return removed;
    }

    @Override
    public void clear() {
        if (!datos.isEmpty()) {
            int lastRow = datos.size() - 1;
            datos.clear();
            fireTableRowsDeleted(0, lastRow);
        }
    }

    @Override
    public void setDatos(Collection<T> datos) {
        clear();
        if (!datos.isEmpty()) {
            this.datos.addAll(datos);
            fireTableRowsInserted(0, datos.size() - 1);
        }
    }

    @Override
    public List<T> getDatos() {
        return Collections.unmodifiableList(datos);
    }

    public void remove(T wrapper) {
        datos.remove(wrapper);
    }
}