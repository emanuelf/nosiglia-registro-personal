package nosigliaregistropersonal.vistas.lib.modeloTabla.otros;

import java.util.List;
import java.util.Objects;

public abstract class WrapperNoEditable<E> implements Vectorizable {

    private E elemento;

    public WrapperNoEditable(E elemento) {
        this.elemento = elemento;
    }

    public final E getElemento() {
        return elemento;
    }

    @Override
    public abstract List<Object> asVector();

    @Override
    public final void setValueAt(int column, Object aValue) {
        throw new UnsupportedOperationException("Not editable.");
    }

    @Override
    public final boolean isCellEditable(int column) {
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.elemento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final WrapperNoEditable other = (WrapperNoEditable) obj;
        if (!Objects.equals(this.elemento, other.elemento)) {
            return false;
        }
        return true;
    }
}
