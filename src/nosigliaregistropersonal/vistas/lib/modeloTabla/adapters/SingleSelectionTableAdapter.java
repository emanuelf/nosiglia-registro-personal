package nosigliaregistropersonal.vistas.lib.modeloTabla.adapters;

import javax.swing.ListSelectionModel;

import org.jdesktop.swingx.JXTable;

import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.ICustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;

/**
 * Ayuda en la utilizacion de tablas de selección simple.
 *
 * @see TableAdapter
 */
public abstract class SingleSelectionTableAdapter<E, W extends Vectorizable> extends TableAdapter<E, W> {

    public SingleSelectionTableAdapter(JXTable tabla, ICustomTableModel<W> customTableModel) {
        super(tabla, customTableModel);
        tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    /**
     * @return el elemento seleccionado en la tabla o <code> null </code> si no
     * hay seleccion
     */
    public final E getSelectedElement() {
        if (getTabla().getSelectedRows().length > 1) {
            throw new IllegalArgumentException("Más de una fila seleccionada");
        }
        if (getTabla().getSelectedRow() == -1) {
            return null;
        }
        return getElementAt(getTabla().getSelectedRow());
    }

    /**
     * elimina el elemento seleccionado de la tabla
     *
     * @return el wrapper representando el elemento eliminado o <code> null </code> en caso de que no se haya seleccionado nada
     */
    public final W removeSelectedElement() {
        final int selectedRow = getTabla().getSelectedRow();
        if (selectedRow != -1) {
            return null;
        }
        return removeElementAt(selectedRow);
    }
}