package nosigliaregistropersonal.vistas.lib.modeloTabla.adapters;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.table.ColumnControlButton.DefaultColumnControlPopup;

import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.OutputUtils;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.CustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.ICustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.MyColumnControlButton;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;

/**
 * Ayuda en la creacion y utilizacion de tablas. realiza configuraciones de uso
 * muy extendido en las mismas.
 *
 * @param <E> el tipo de los elementos que se cargaran en la tabla.
 * @param <W> el tipo de representacion, en forma de fila de tabla, que se va a
 * utilizar para cada E.
 */
public abstract class TableAdapter<E, W extends Vectorizable> {

    private static final DefaultCellEditor SIN_CEROS_BIG_DECIMAL_CELL_EDITOR = createBigDecimalCellEditorSinCeros();
    private static final DefaultCellEditor BIG_DECIMAL_CELL_EDITOR = createBigDecimalCellEditorConCeros();
    private static final DefaultCellEditor INTEGER_CELL_EDITOR = createIntegerCellEditor(false);
    private static final int FIRST_SHEET = 0;
    private final JXTable tabla;
    private String defaultExportName = "tabla";

    public TableAdapter(JXTable tabla, ICustomTableModel<W> customTableModel) {
        this.tabla = tabla;
        agregarConfiguracionBasica(tabla, customTableModel);
    }

    public TableAdapter(JXTable tabla, String[] COLUMNAS) {
        this.tabla = tabla;
        ICustomTableModel<W> ctm = new CustomTableModel<>(COLUMNAS);
        agregarConfiguracionBasica(tabla, ctm);
    }

    /**
     * @return el modelo de la tabla utilizada por este table adapter, de tipo
     * ICustomTableModel
     */
    @SuppressWarnings("unchecked")
    protected final ICustomTableModel<W> getCustomTableModel() {
        return (ICustomTableModel<W>) getTabla().getModel();
    }

    /**
     * @return true si la tabla esta vacía, false de otra manera
     */
    public final boolean isEmpty() {
        return getCustomTableModel().getDatos().isEmpty();
    }

    /**
     * @return true si la tabla tiene al menos una fila seleccionada, false de
     * otra manera
     */
    public final boolean hasSelection() {
        return getTabla().getSelectedRow() != -1;
    }

    /**
     * añade un elemento al final de la tabla. Si element == null no lo aniade
     */
    public void addElement(E element) {
        getCustomTableModel().addRow(toWrapper(element));
    }

    /**
     * añade una coleccion de elementos al final de la tabla
     */
    public void addElements(Collection<? extends E> elements) {
        List<W> addedRows = new ArrayList<>(elements.size());
        for (E element : elements) {
            addedRows.add(toWrapper(element));
        }
        getCustomTableModel().addRows(addedRows);
    }

    /**
     * Devuelve la tabla a su estado inicial, y luego añade todos los elementos
     * presentes en la coleccion.
     *
     * @param los elementos que contendra la tabla al finalizar esta llamada.
     */
    public void setElements(Collection<E> elements) {
        removeAllElements();
        addElements(elements);
    }

    /**
     * Devuelve la tabla a su estado inicial
     * @deprecated Usar clear en su lugar
     */
    @Deprecated
    public final void removeAllElements() {
        getCustomTableModel().clear();
    }
    
    public final void clear() {
        removeAllElements();
    }

    /**
     * @return todos los elementos presentes en la tabla en el orden del modelo
     */
    public List<E> getAllElements() {
        List<E> elements = new ArrayList<>();
        for (W wrapper : getCustomTableModel().getDatos()) {
            elements.add(fromWrapper(wrapper));
        }
        return elements;
    }

    /**
     * @return todos los elementos presentes en la tabla en el orden de la vista
     */
    public List<E> getAllElementsViewSorted() {
        List<E> modelOrder = getAllElements();
        List<E> viewOrder = new ArrayList<>(modelOrder); //para poder llamar a set(index, element) sin provocar excepciones.
        for (int currentRow = 0; currentRow < viewOrder.size(); currentRow++) {
            int modelRow = getTabla().convertRowIndexToModel(currentRow);
            viewOrder.set(currentRow, modelOrder.get(modelRow));
        }
        return viewOrder;
    }

    /**
     * @return todos los wrappers presentes en la tabla en el orden de la vista
     */
    public List<W> getAllWrappersViewSorted() {
        List<W> modelOrder = getAllWrappers();
        List<W> viewOrder = new ArrayList<>(modelOrder); //para poder llamar a set(index, element) sin provocar excepciones.
        for (int currentRow = 0; currentRow < viewOrder.size(); currentRow++) {
            int modelRow = getTabla().convertRowIndexToModel(currentRow);
            viewOrder.set(currentRow, modelOrder.get(modelRow));
        }
        return viewOrder;
    }

    /**
     * @param indiceVista el indice, en coordenadas de la vista
     * @return el elemento en el indice indicado
     */
    public final E getElementAt(int indiceVista) {
        int indiceModelo = getTabla().convertRowIndexToModel(indiceVista);
        return fromWrapper(getCustomTableModel().getDatos().get(indiceModelo));
    }

    public final W getWrapperAt(int indiceModelo) {
        return getCustomTableModel().getDatos().get(indiceModelo);
    }

    public void setDefaultExportName(String defaultExportName) {
        this.defaultExportName = defaultExportName;
    }

    /**
     * Elimina de la tabla el elemento en el indice indicado
     *
     * @param indiceVista el indice del elemento a eliminar, en coordenadas de
     * la vista
     *
     * @return el elemento eliminado, o null si no se elimino nada
     */
    public W removeElementAt(int indiceVista) {
        int indiceModelo = getTabla().convertRowIndexToModel(indiceVista);
        return getCustomTableModel().removeRow(indiceModelo);
    }

    /**
     *
     * @param element
     * @return true si element se encuentra en este tableAdapter, false de otra
     * manera
     */
    public boolean contains(E element) {
        return getAllElements().contains(element);
    }

    /**
     * @return la tabla que este adapter esta utilizando
     */
    public JXTable getTabla() {
        return tabla;
    }

    /**
     *
     * @return el tamaño (numero de filas) que tiene este tableAdapter
     */
    public int size() {
        return getCustomTableModel().getRowCount();
    }

    /**
     * @param element el elemento que se desea convertir a Vectorizable
     * @return un wrapper representendo el elemento pasado como parametro.
     */
    public abstract W toWrapper(E element);

    /**
     * @param wrapper un wrapper que representa un elemento en una tabla.
     * @return el elemento que representa el wrapper pasado como parametro.
     */
    public abstract E fromWrapper(W wrapper);

    /**
     * Quienes usen un TableAdapter no deberian llamar a JXTable.addHighlighter
     * sino a este metodo para evitar problemas con el orden de los Highlighters
     * que hace que solo funcione el foreground = yellow del rollover, pero no
     * el background = dark_gray.
     *
     * @param highlighter el resaltador que se desea utilizar en la tabla.
     */
    public void putBeforeRollover(Highlighter highlighter) {
        List<Highlighter> asList = new ArrayList<>(Arrays.asList(tabla.getHighlighters()));
        asList.add(1, highlighter);
        tabla.setHighlighters(asList.toArray(new Highlighter[asList.size()]));
    }

    public static TableCellEditor getIntegerEditor() {
        return INTEGER_CELL_EDITOR;
    }

    public static TableCellEditor getBigDecimalEditor(boolean incluirCero) {
        if (incluirCero) {
            return BIG_DECIMAL_CELL_EDITOR;
        } else {
            return SIN_CEROS_BIG_DECIMAL_CELL_EDITOR;
        }
    }

    private static DefaultCellEditor createIntegerCellEditor(boolean incluirCero) {
        JTextField jTxtNroEntero = new JTextField();
        addFocusGainedListener(jTxtNroEntero);
        InputUtils.addIntegerListeners(jTxtNroEntero, incluirCero);
        return new DefaultCellEditor(jTxtNroEntero);
    }

    private static DefaultCellEditor createBigDecimalCellEditorSinCeros() {
        JTextField jTxtNroDecimalSinCero = new JTextField();
        addFocusGainedListener(jTxtNroDecimalSinCero);
        InputUtils.addDecimalListeners(jTxtNroDecimalSinCero, false);
        return new DefaultCellEditor(jTxtNroDecimalSinCero);
    }

    private static DefaultCellEditor createBigDecimalCellEditorConCeros() {
        final JTextField jTxtNroDecimal = new JTextField();
        addFocusGainedListener(jTxtNroDecimal);
        InputUtils.addDecimalListeners(jTxtNroDecimal, true);
        return new DefaultCellEditor(jTxtNroDecimal);
    }

    public List<W> getAllWrappers() {
        return getCustomTableModel().getDatos();
    }

    private static void addFocusGainedListener(final JTextField jTxtNroDecimal) {
        jTxtNroDecimal.addFocusListener(new FocusAdapter() {

            @Override
            public void focusGained(FocusEvent e) {
                final String text = jTxtNroDecimal.getText();
                if (!text.isEmpty()) {
                    if (text.length() > 1) {
                        char letraAgregada = text.charAt(text.length() - 1);
                        jTxtNroDecimal.setText(Character.toString(letraAgregada));
                    }
                }
            }
        });
    }

    public static ColorHighlighter createNotEditableHighlighter(final JXTable jXTable) {
        final Color color = UIManager.getColor("TextField.inactiveBackground");
        return new ColorHighlighter(new HighlightPredicate() {

            @Override
            public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
                int row = adapter.convertRowIndexToModel(adapter.row);
                int column = adapter.convertColumnIndexToModel(adapter.column);
                return !jXTable.isCellEditable(row, column);
            }
        }, color, OutputUtils.CURRENT_COLOR);
    }

    private void agregarConfiguracionBasica(JXTable tabla, ICustomTableModel<W> customTableModel) {
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        tabla.setModel(customTableModel);
        tabla.setColumnSelectionAllowed(false);
        tabla.setSurrendersFocusOnKeystroke(true);
        //MyColumnControlButton myColumnControlButton = configurarCustomColumnControlButton(tabla);
        //tabla.setColumnControl(myColumnControlButton);
        tabla.setColumnControlVisible(true);
        tabla.addHighlighter(HighlighterFactory.createAlternateStriping());
        //tabla.addHighlighter(createNotEditableHighlighter(tabla));
        tabla.addHighlighter(new ColorHighlighter(HighlightPredicate.ROLLOVER_ROW, Color.DARK_GRAY, Color.YELLOW));
        tabla.setDefaultEditor(Integer.class, getIntegerEditor());
        tabla.setDefaultEditor(Long.class, new DefaultCellEditor(new JTextField()));
        tabla.setDefaultEditor(BigDecimal.class, getBigDecimalEditor(true));
        tabla.setDefaultRenderer(Integer.class, new DefaultTableRenderer());
        tabla.setDefaultRenderer(Long.class, new DefaultTableRenderer());
        tabla.setRowSorter(new TriStateTableRowSorter(customTableModel));
        tabla.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputUtils.NO_MODIFIERS), "selectNextColumnCell");
    }

    public static TableCellEditor getIntegerEditor(boolean incluirCero) {
        return createIntegerCellEditor(incluirCero);
    }

    public void fireTableDataChanged() {
        getCustomTableModel().fireTableDataChanged();
    }
}
