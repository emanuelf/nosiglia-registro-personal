package nosigliaregistropersonal.vistas.lib.modeloTabla.adapters;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ListSelectionModel;

import org.jdesktop.swingx.JXTable;
import nosigliaregistropersonal.vistas.lib.Iconos;
import nosigliaregistropersonal.vistas.lib.MessageUtils;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.ICustomTableModel;
import nosigliaregistropersonal.vistas.lib.modeloTabla.otros.Vectorizable;

/**
 * Ayuda en la utilizacion de tablas de seleccion multiple.
 *
 * @see TableAdapter
 */
public abstract class MultipleSelectionTableAdapter<E, W extends Vectorizable> extends TableAdapter<E, W> {

    private AbstractAction eliminarAction;

    public MultipleSelectionTableAdapter(final JXTable tabla, ICustomTableModel<W> customTableModel) {
        super(tabla, customTableModel);
        agregarConfiguracionBasica(tabla);
    }

    public MultipleSelectionTableAdapter(JXTable tabla, String[] COLUMNAS) {
        super(tabla, COLUMNAS);
        agregarConfiguracionBasica(tabla);
    }

    /**
     * @return una lista con todos los elementos seleccionados, ordenados del menor al mayor indice
     */
    public final List<E> getSelectedElements() {
        List<E> elementosSeleccionados = new ArrayList<>();
        int[] indicesSeleccionados = getTabla().getSelectedRows();
        for (int indice : indicesSeleccionados) {
            E elementoSeleccionado = getElementAt(indice);
            elementosSeleccionados.add(elementoSeleccionado);
        }
        return elementosSeleccionados;
    }

    /**
     * elimina de la tabla todos los elementos seleccionados
     *
     * @return una lista con todos los wrappers de los elementos eliminados, ordenados del menor al
     * mayor indice
     */
    public List<W> removeSelectedElements() {
        List<W> eliminados = new ArrayList<>();
        int[] indicesSeleccionados = getTabla().getSelectedRows();
        for (int i = indicesSeleccionados.length - 1; i >= 0; i--) {
            W eliminado = removeElementAt(indicesSeleccionados[i]);
            eliminados.add(eliminado);
        }
        return eliminados;
    }

    /**
     * @return el ultimo elemento (temporalmente hablando) que selecciono el usuario. Si la tabla no
     * contiene elementos, este metodo retorna null.
     */
    public E getLastSelectedElement() {
        final int leadSelectionIndex = getTabla().getSelectionModel().getLeadSelectionIndex();
        if (size() == 0 || leadSelectionIndex == -1) {
            return null;
        }
        return getElementAt(leadSelectionIndex);
    }

    /**
     * @param nombre el nombre que se va a mostrar para la accion
     * @param icono el icono que se va a mostrar para la accion
     * @return un AbstractAction que elimina los elementos seleccionados al ser llamado, si no hay
     * seleccion muestra un cartel al usuario. Si el table adapter se vacia durante la ejecucion de
     * esta accion, la misma se deshabilita
     */
    public AbstractAction getDefaultEliminarAction(String nombre, Icon icono) {
        if (eliminarAction == null) {
            eliminarAction = new AbstractAction(nombre, icono) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (hasSelection()) {
                        removeSelectedElements();
                    } else {
                        MessageUtils.showInfo(MultipleSelectionTableAdapter.this.getTabla(), "Debe seleccionar una fila.");
                    }
                }
            };
        }
        return eliminarAction;
    }

  
    public List<W> getSelectedWrappers() {
        List<W> wrappersSeleccionados = new ArrayList<>();
        int[] indicesSeleccionados = getTabla().getSelectedRows();
        for (int indice : indicesSeleccionados) {
            W elementoSeleccionado = getWrapperAt(indice);
            wrappersSeleccionados.add(elementoSeleccionado);
        }
        return wrappersSeleccionados;
    }

    public void addWrapper(W toWrapper) {
        getCustomTableModel().addRow(toWrapper);
    }

    private void agregarConfiguracionBasica(final JXTable tabla) {
        tabla.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        tabla.setColumnSelectionAllowed(false);
        agregarDobleClickControlDownMouseListener(tabla);
    }

    private void agregarDobleClickControlDownMouseListener(final JXTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() >= 2 && me.isControlDown()) {
                    Point viewPortPoint = me.getPoint();
                    int rowAtPoint = tabla.rowAtPoint(viewPortPoint);
                    tabla.getSelectionModel().addSelectionInterval(rowAtPoint, rowAtPoint);
                }
            }
        });
    }
}