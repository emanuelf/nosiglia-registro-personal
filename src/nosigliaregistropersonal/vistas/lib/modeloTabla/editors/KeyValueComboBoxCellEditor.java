package nosigliaregistropersonal.vistas.lib.modeloTabla.editors;

import java.awt.Component;
import java.util.Arrays;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public abstract class KeyValueComboBoxCellEditor<Key, Value> implements TableCellEditor {

    private DefaultCellEditor comboBoxCellEditor;

    public KeyValueComboBoxCellEditor(Key[] keys) {
        comboBoxCellEditor = new DefaultCellEditor(new JComboBox<>(Arrays.copyOf(keys, keys.length)));
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return comboBoxCellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @SuppressWarnings("unchecked")
    public Key getKey() {
        return (Key) comboBoxCellEditor.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return comboBoxCellEditor.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return comboBoxCellEditor.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return comboBoxCellEditor.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        comboBoxCellEditor.cancelCellEditing();
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        comboBoxCellEditor.addCellEditorListener(l);
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        comboBoxCellEditor.removeCellEditorListener(l);
    }

    @Override
    public abstract Value getCellEditorValue();
}