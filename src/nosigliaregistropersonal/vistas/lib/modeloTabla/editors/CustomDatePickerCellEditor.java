package nosigliaregistropersonal.vistas.lib.modeloTabla.editors;

import java.text.DateFormat;
import java.text.ParseException;

import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.table.DatePickerCellEditor;

public class CustomDatePickerCellEditor extends DefaultCellEditor implements TableCellEditor {

    private DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT);
    private final JFormattedTextField dateFormatterField;

    public CustomDatePickerCellEditor(JFormattedTextField fieldDate) {
        super(fieldDate);
        this.dateFormatterField = fieldDate;
        dateFormatterField.setFormatterFactory(new InnerCustomDatePickerCellEditor().getEditor().getFormatterFactory());
    }

    @Override
    public Object getCellEditorValue() {
        try {
            return formatter.parse(dateFormatterField.getText());
        } catch (ParseException ex) {
            return null;
        }
    }

    private class InnerCustomDatePickerCellEditor extends DatePickerCellEditor {

        public JFormattedTextField getEditor() {
            return this.datePicker.getEditor();
        }
    }
}
