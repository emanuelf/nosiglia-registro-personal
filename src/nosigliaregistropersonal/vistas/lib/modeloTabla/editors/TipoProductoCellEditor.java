package nosigliaregistropersonal.vistas.lib.modeloTabla.editors;

public class TipoProductoCellEditor extends KeyValueComboBoxCellEditor<String, Boolean> {

    public TipoProductoCellEditor() {
        super(new String[]{"Bulto", "Unidad"});
    }

    @Override
    public Boolean getCellEditorValue() {
        return getKey().equalsIgnoreCase("Bulto");
    }
}
