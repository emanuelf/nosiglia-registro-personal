package nosigliaregistropersonal.vistas.lib;

import java.awt.Component;

import javax.swing.JOptionPane;

public class MessageUtils {

    private MessageUtils() {
        throw new UnsupportedOperationException("non instantiable.");
    }

    public static void showValidationWarning(Component jPanel, String mensaje) {
        JOptionPane.showMessageDialog(jPanel, mensaje, "Advertencia", JOptionPane.WARNING_MESSAGE);
    }

    public static void showError(Component origen, String message) {
        JOptionPane.showMessageDialog(origen, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void showWarning(Component origen, String message) {
        JOptionPane.showMessageDialog(origen, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }

    public static void showInfo(Component origen, String message) {
        JOptionPane.showMessageDialog(origen, message, "Informacion", JOptionPane.INFORMATION_MESSAGE);
    }

    public static boolean areYouSure(String pregunta) {
        return areYouSure("Confirme", pregunta);
    }

    public static boolean areYouSure(String titulo, String pregunta) {
        return JOptionPane.showConfirmDialog(null, pregunta,
                                             titulo, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }

}