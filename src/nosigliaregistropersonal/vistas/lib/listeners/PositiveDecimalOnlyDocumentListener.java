package nosigliaregistropersonal.vistas.lib.listeners;

import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import nosigliaregistropersonal.vistas.lib.InputUtils;


/**
 * Listener que reduce el rango de valores en componentes de texto Swing a
 * valores numericos positivos
 */
public class PositiveDecimalOnlyDocumentListener implements DocumentListener {

    private final JTextField decimalField;
    private boolean imModifying;
    private String cadenaAnterior = "";
    private final boolean incluirCero;

    public PositiveDecimalOnlyDocumentListener(JTextField decimalField, boolean incluirCero) {
        this.decimalField = decimalField;
        this.incluirCero = incluirCero;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        checkDecimalUndoOtherwise(decimalField.getText());
    }

    private void checkDecimalUndoOtherwise(String text) {
        final String toLowerCase = text.toLowerCase();
        if (InputUtils.isPositiveNumeric(text, incluirCero) && !toLowerCase.contains("d") && !text.toLowerCase().contains("f")) {
            cadenaAnterior = text;
        } else {
            deshacerEdicionTardiamente();
        }
    }

    private void deshacerEdicionTardiamente() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                undoLastEdit();
                Toolkit.getDefaultToolkit().beep();
            }
        });
    }

    private void undoLastEdit() {
        imModifying = true;
        decimalField.setText(cadenaAnterior);
        imModifying = false;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (!imModifying) {
            cadenaAnterior = decimalField.getText();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }
}
