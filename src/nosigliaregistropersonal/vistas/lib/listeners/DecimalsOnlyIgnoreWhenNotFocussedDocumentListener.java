package nosigliaregistropersonal.vistas.lib.listeners;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Listener que reduce el rango de valores en componentes de texto Swing a
 * valores numericos positivos, pero solo mientras el componente tiene el foco.
 */
public class DecimalsOnlyIgnoreWhenNotFocussedDocumentListener implements DocumentListener {

    private final JTextField decimalField;
    private final PositiveDecimalOnlyDocumentListener dodl;

    public DecimalsOnlyIgnoreWhenNotFocussedDocumentListener(JTextField decimalField, boolean incluirCero) {
        this.decimalField = decimalField;
        this.dodl = new PositiveDecimalOnlyDocumentListener(decimalField, incluirCero);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        if (decimalField.hasFocus()) {
            dodl.insertUpdate(e);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (decimalField.hasFocus()) {
            dodl.removeUpdate(e);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        if (decimalField.hasFocus()) {
            dodl.changedUpdate(e);
        }
    }
}
