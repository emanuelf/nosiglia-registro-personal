package nosigliaregistropersonal.vistas.lib.listeners;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JTextField;
import nosigliaregistropersonal.vistas.lib.InputUtils;

public class CurrencyFormattedFieldDecorator implements FocusListener {

    private static NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();

    @Override
    public void focusGained(FocusEvent event) {
        JTextField origen = (JTextField) event.getComponent();
        try {
            origen.setText(String.valueOf(currencyFormatter.parse(origen.getText())));
        } catch (ParseException ex) {
            origen.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent event) {
        JTextField origen = (JTextField) event.getComponent();
        BigDecimal monto = InputUtils.parseCurrency(origen.getText());
        if (monto.equals(BigDecimal.ZERO)) {
            origen.setText("");
        } else {
            origen.setText(currencyFormatter.format(monto));
        }
    }
}