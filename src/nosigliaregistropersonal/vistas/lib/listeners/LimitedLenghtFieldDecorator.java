package nosigliaregistropersonal.vistas.lib.listeners;

import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;


public class LimitedLenghtFieldDecorator extends KeyAdapter {

    private final int length;

    public LimitedLenghtFieldDecorator(int length) {
        this.length = length;
    }

    @Override
    public void keyTyped(KeyEvent event) {
        JTextField origen = (JTextField) event.getSource();
        if (origen.getText().length() >= length) {
            Toolkit.getDefaultToolkit().beep();
            event.setKeyChar((char) KeyEvent.VK_CLEAR);
        }
    }
}