package nosigliaregistropersonal.vistas.lib.listeners;

import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import nosigliaregistropersonal.vistas.lib.InputUtils;


/**
 * Listener que reduce el rango de valores en componentes de texto Swing a
 * valores enteros positivos
 */
public class IntegersOnlyDocumentListener implements DocumentListener {

    private final JTextField integerField;
    private boolean imModifying;
    private boolean incluirCero;
    private String cadenaAnterior = "";
    private final boolean permitirNegativos;

    public IntegersOnlyDocumentListener(JTextField integerField, boolean incluirCero) {
        this.integerField = integerField;
        this.incluirCero = incluirCero;
        this.permitirNegativos = false;
        inicializarCadenaAnterior(integerField.getText(), incluirCero);
    }

    public IntegersOnlyDocumentListener(JTextField integerField) {
        this.integerField = integerField;
        this.incluirCero = true;
        this.permitirNegativos = true;
    }

    private void inicializarCadenaAnterior(String text, boolean incluirCero) {
        if (permitirNegativos && InputUtils.isInteger(text) || InputUtils.isPositiveInteger(text, incluirCero)) {
            cadenaAnterior = text;
        } else {
            cadenaAnterior = "";
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        checkIntegerUndoOtherwise(integerField.getText());
    }

    private void checkIntegerUndoOtherwise(String text) {
        if (permitirNegativos && InputUtils.isInteger(text) || InputUtils.isPositiveInteger(text, incluirCero)) {
            cadenaAnterior = text;
        } else {
            deshacerEdicionTardiamente();
        }
    }

    private void deshacerEdicionTardiamente() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                undoLastEdit();
                Toolkit.getDefaultToolkit().beep();
            }
        });
    }

    private void undoLastEdit() {
        imModifying = true;
        integerField.setText(cadenaAnterior);
        imModifying = false;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (!imModifying) {
            cadenaAnterior = integerField.getText();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }
}
