package nosigliaregistropersonal.vistas.lib;

import javax.swing.ImageIcon;

public class Iconos {

    private Iconos() {
    }
        
    //abms
    public static final ImageIcon CANCELAR_ABM = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/cerrarOCancelar.png"));
    public static final ImageIcon AGREGAR_ABM = new ImageIcon( Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/agregar.png"));
    public static final ImageIcon BUSCAR_ABM = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/buscar.png"));
    public static final ImageIcon EDITAR_ABM = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/editar.png"));
    public static final ImageIcon ELIMINAR_ABM = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/eliminar.png"));
    public static final ImageIcon GUARDAR_ABM = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/abms/ok.png"));
    public static final ImageIcon ELIMINAR_16X16 = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/16x16/eliminar.png"));
    public static final ImageIcon AGREGAR_16X16 = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/16x16/agregar.png"));
    public static final ImageIcon EDITAR_16X16 = new ImageIcon(Iconos.class.getResource("/nosigliaregistropersonal/iconos/16x16/editar.png"));


}