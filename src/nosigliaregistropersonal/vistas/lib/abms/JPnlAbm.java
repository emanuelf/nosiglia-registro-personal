package nosigliaregistropersonal.vistas.lib.abms;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.beans.Beans;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import nosigliaregistropersonal.vistas.framework.Buscador;

import nosigliaregistropersonal.vistas.lib.InputUtils;
import nosigliaregistropersonal.vistas.lib.MessageUtils;

public abstract class JPnlAbm<E> extends JPanel {

    private ActionDeshabilitable agregar;
    private ActionDeshabilitable anular;
    private ActionDeshabilitable buscar;
    private ActionDeshabilitable cancelar;
    private ActionDeshabilitable editar;
    private ActionDeshabilitable eliminar;
    private ActionDeshabilitable guardar;
    private ActionDeshabilitable imprimir;
    private ActionDeshabilitable vistaPreliminar;
    private boolean nuevo;
    private E workingCopy;
    private boolean editando;

    protected JPnlAbm() {
        if (!Beans.isDesignTime()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
    }

    private void recargar() {
        workingCopy = getControladorAbm().reload(workingCopy);
        cargar(workingCopy);
    }

    private void habilitarNavegacion() {
        getBotonera().modoInicial();
    }

    /**
     * @return el Buscador, que especifica las distintas formas de buscar una
     * entidad en particular.
     */
    protected abstract Buscador<E> getBuscador();

    /**
     * @return el Controlador, que especifica informacion sobre el ciclo de vida
     * de la entidad. (guardado, eliminacion, etc.)
     */
    protected abstract ControladorAbm<E> getControladorAbm();

    /**
     * @return el Detalle que muestra la informacion detallada de la entidad que
     * maneja el abm
     */
    protected abstract Detalle<E> getDetalle();

    /**
     * @return la botonera que maneja el abm
     */
    protected abstract BotoneraAbm getBotonera();

    public boolean estaEditando() {
        return editando;
    }

    /**
     * muestra el elemento en pantalla permitiendo su visualizacion y edicion
     *
     * @param element el elemento a cargar en pantalla.
     * @throws NullPointerException si element == null;
     */
    public void cargar(E element) {
        nuevo = false;
        if (element == null) {
            deshabilitarTodo();
            agregar.setEnabled(true);
        } else {
            workingCopy = element;
            getDetalle().mostrarDatos(workingCopy);
            modoVisualizacion();
        }
    }

    /**
     * Muestra el elemento en pantalla permitiendo solamente su visualizacion.
     *
     * @param element el elemento a mostrar en pantalla.
     * @throws NullPointerException si element == null;
     */
    public JPnlAbm<E> mostrar(E element) {
        workingCopy = element;
        getDetalle().mostrarDatos(element);
        deshabilitarTodo();
        return this;
    }

    /**
     * Actualiza el contenido en pantalla actualizando los paneles hijos en caso
     * de que hubieran.
     */
    protected void actualizarVista() {
        getDetalle().mostrarDatos(workingCopy);
    }

    protected void modoEdicion() {
        deshabilitarTodo();
        getBotonera().modoEdicion();
        getDetalle().edicion();
        editando = true;
    }

    protected void modoVisualizacion() {
        habilitarTodo();
        getBotonera().modoVisualizacion();
        getDetalle().visualizacion();
        editando = false;
    }

    protected void modoInicial() {
        workingCopy = null;
        deshabilitarTodo();
        habilitarNavegacion();
        getDetalle().visualizacion();
    }

    protected final void modoPosImpresion() {
        deshabilitarTodo();
        habilitarNavegacion();
        anular.setEnabled(true);
        imprimir.setEnabled(true);
        vistaPreliminar.setEnabled(true);
    }

    /**
     * Valida el elemento cargado en pantalla y lo guarda si pasa la validacion.
     * Refresca la pantalla por si hubo efectos secundarios en la llamada a
     * guardar. Si no pasa la validacion, muestra el mensaje de error de la
     * misma al usuario. Reimplementar este metodo si se desea que el boton
     * guardar se comporte de distinta manera.
     */
    protected void guardar() {
        getDetalle().guardarDatos(workingCopy);
        if (isValid(workingCopy)) {
            getControladorAbm().guardar(workingCopy);
            refrescar();
        } else {
            MessageUtils.showValidationWarning(this, "Hay datos por cargar o son erroneos");
        }
    }

    protected boolean isNuevo() {
        return nuevo;
    }

    protected void nuevo() {
        nuevo = true;
        workingCopy = getControladorAbm().crear();
        getDetalle().limpiar();
        modoEdicion();
    }

    private void buscar() {
        E resultado = getBuscador().buscar();
        if (resultado != null) {
            cargar(resultado);
        }
    }

    /**
     * Cancela la edicion actualmente en curso. Deshace todos los cambios
     * realizados por el usuario en el curso de la misma. Reimplementar este
     * metodo en caso de que se quiera que el boton cancelar se comporte de
     * distinta manera.
     */
    protected void cancelar() {
        editando = false;
        if (nuevo) {
            modoInicial();
            getDetalle().limpiar();
            nuevo = false;
        } else {
            recargar();
            modoVisualizacion();
        }
    }

    protected void eliminar() {
        if (MessageUtils.areYouSure("¿Está seguro?")) {
            getControladorAbm().eliminar(workingCopy);
            modoInicial();
            getDetalle().limpiar();
        }
    }

    protected void agregar() {
        nuevo();
        modoEdicion();
    }

    protected void configurarActions() {
        configurarAgregarAction();
        configurarBuscarAction();
        configurarCancelarAction();
        configurarEditarAction();
        configurarEliminarAction();
        configurarGuardarAction();
        configurarImprimirAction();
    }

    protected Object getPropertyValue() {
        return getControladorAbm().getId(workingCopy);
    }

    protected String getPropertyName() {
        return "id";
    }

    @SuppressWarnings("unchecked")
    protected Class<? extends E> getSearchedClass() {
        return (Class<? extends E>) getControladorAbm().crear().getClass();
    }

    protected Map<String, Object> getWhereMap() {
        return Collections.emptyMap();
    }

    private void configurarAgregarAction() {
        agregar = new ActionDeshabilitable("agregar", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregar();
            }
        };
        configurarAction(this, "agregar", agregar, KeyEvent.VK_N);
    }

    private void configurarBuscarAction() {
        buscar = new ActionDeshabilitable("buscar", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                buscar();
            }
        };
        configurarAction(this, "buscar", buscar, KeyEvent.VK_B);
    }

    private void configurarCancelarAction() {
        final String nombreAccion = "cancelar";
        cancelar = new ActionDeshabilitable(nombreAccion, null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        };
        configurarAction(this, nombreAccion, cancelar, InputUtils.NO_MODIFIERS);
    }

    private void configurarEditarAction() {
        editar = new ActionDeshabilitable("editar", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargar(workingCopy);
                modoEdicion();
            }
        };
        configurarAction(this, "editar", editar, KeyEvent.VK_E);
    }

    private void configurarEliminarAction() {
        eliminar = new ActionDeshabilitable("eliminar", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminar();
            }
        };
        configurarAction(this, "eliminar", eliminar, KeyEvent.VK_R);
    }

    private void configurarGuardarAction() {
        guardar = new ActionDeshabilitable("guardar", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardar();
            }
        };
        configurarAction(this, "guardar", guardar, KeyEvent.VK_G);
    }
    
    private void configurarImprimirAction(){
        imprimir = new ActionDeshabilitable("imprimir", null) {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    getControladorAbm().imprimir(workingCopy);
                } catch (PrinterException ex) {
                    Logger.getLogger(JPnlAbm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        configurarAction(this, "imprimir", imprimir, KeyEvent.VK_P);
    }

    private void configurarAction(JPanel jPanel, String nombreAccion, ActionDeshabilitable accion, int keyCode) {
        jPanel.getActionMap().put(nombreAccion, accion);
        setAction(nombreAccion, accion);
        jPanel.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(keyCode, KeyEvent.CTRL_DOWN_MASK), nombreAccion);
    }

    private void habilitarTodo() {
        habilitarNavegacion();
        cancelar.setEnabled(true);
        editar.setEnabled(true);
        eliminar.setEnabled(true);
        guardar.setEnabled(true);
    }

    private void deshabilitarTodo() {
        agregar.setEnabled(false);
        buscar.setEnabled(false);
        cancelar.setEnabled(false);
        editar.setEnabled(false);
        eliminar.setEnabled(false);
        guardar.setEnabled(false);
    }

    /**
     * Refresca los cambios que puedan haber sido realizados como efectos
     * secundarios de la llamada a guardar.
     */
    protected final void refrescar() {
        cargar(workingCopy);
    }

    protected E getWorkingCopy() {
        return workingCopy;
    }


    private void esconderBoton(JButton boton) {
        if (boton != null) {
            boton.setVisible(false);
        }
    }

    protected abstract boolean isValid(E object);

    private void setAction(String nombreAccion, ActionDeshabilitable accion) throws AssertionError {
        final BotoneraAbm botonera = getBotonera();
        switch (nombreAccion) {
            case "agregar":
                botonera.setAgregarAction(accion);
                break;
            case "editar":
                botonera.setEditarAction(accion);
                break;
            case "guardar":
                botonera.setGuardarAction(accion);
                break;
            case "eliminar":
                botonera.setEliminarAction(accion);
                break;
            case "cancelar":
                botonera.setCancelarAction(accion);
                break;
            case "buscar":
                botonera.setBuscarAction(accion);
                break;
            case "imprimir":
                botonera.setImprimirAction(accion);
                break;
            default:
                System.out.println("no existe");
                ;
        }
    }

    private abstract static class ActionDeshabilitable extends AbstractAction {

        private boolean habilitado = true;

        ActionDeshabilitable(String name, Icon icon) {
            super(name, icon);
        }

        @Override
        public void setEnabled(boolean newValue) {
            if (habilitado) {
                super.setEnabled(newValue);
            }
        }

        public void deshabilitarPermanentemente() {
            habilitado = false;
            super.setEnabled(false);
        }
    }
}
