package nosigliaregistropersonal.vistas.lib.abms;

import java.awt.event.ActionListener;

/**
 *
 * @author Emanuel
 */
public class ActionAbm {

    private String actionName;
    private int keyStroke;
    private int modifier;
    private ActionListener al;

    public void setActionPerformed(ActionListener e) {
        this.al = e;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public void setKeyStroke(int keyStroke) {
        this.keyStroke = keyStroke;
    }

    public void setModifier(int modifier) {
        this.modifier = modifier;
    }

    public void setAl(ActionListener al) {
        this.al = al;
    }

    public String getActionName() {
        return actionName;
    }

    public int getKeyStroke() {
        return keyStroke;
    }

    public int getModifier() {
        return modifier;
    }

    public ActionListener getAl() {
        return al;
    }
}
