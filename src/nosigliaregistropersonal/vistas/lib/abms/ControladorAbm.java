package nosigliaregistropersonal.vistas.lib.abms;

import java.awt.print.PrinterException;

public interface ControladorAbm<E> {

    public void anular(E element);

    /**
     * crea un nuevo elemento de tipo E.
     *
     * @return el elemento creado
     */
    public E crear();

    /**
     * elimina el elemento E. Opcional
     *
     * @param element
     */
    public void eliminar(E element);

    /**
     * este metodo debería ser usualmente una simple pasarela hacia
     * {@code Service.saveOrUpdate}.
     *
     * @param element el elemento a guardar
     */
    public void guardar(E element);

    /**
     * Imprime el elemento.
     *
     * @param element el elemento a imprimir
     */
    public void imprimir(E element) throws PrinterException;

    /**
     * Muestra en pantalla una vista preliminar de la impresion del elemento.
     *
     * @param element el elemento a visualizar
     */
    public void vistaPreliminar(E element) throws PrinterException;

    /**
     * Recarga el elemento desde la base de datos, para uso de cancelar en
     * JPnlAbm.
     *
     * @param element el elemento a recargar
     * @return el mismo elemento, leido de la base de datos.
     */
    public E reload(E element);

    public Object getId(E element);
}