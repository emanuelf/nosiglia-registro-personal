package nosigliaregistropersonal.vistas.lib.abms;

import javax.swing.Action;

public interface BotoneraAbm {

    public void agregar();

    public void buscar();

    public void cancelar();

    public void editar();

    public void eliminar();

    public void guardar();

    public void setAgregarAction(Action accion);

    public void setBuscarAction(Action guardarAction);

    public void setCancelarAction(Action guardarAction);

    public void setEditarAction(Action guardarAction);

    public void setEliminarAction(Action guardarAction);

    public void setGuardarAction(Action guardarAction);
    
    public void setImprimirAction(Action imprimirAction);
    
    public void modoInicial();

    public void modoEdicion();

    public void modoVisualizacion();}