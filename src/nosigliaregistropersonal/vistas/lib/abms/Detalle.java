package nosigliaregistropersonal.vistas.lib.abms;

/**
 * Muestra información detallada de un elemento de tipo E al usuario
 *
 * @param <E> el tipo de elemento que muestra este detalle.
 */
public interface Detalle<E> {

    /**
     * muestra los datos de element al usuario.
     *
     * @param element el elemento cuyos datos deben ser mostrados
     */
    public void mostrarDatos(E element);

    /**
     * modifica los datos de element para que coincidan con lo que se esta
     * mostrando al usuario.
     *
     * @param element el elemento cuyos datos deben modificarse para coincidir
     *                con lo mostrado
     */
    public void guardarDatos(E element);

    /**
     * Habilita los componentes para que el usuario pueda editarlos.
     */
    public void edicion();

    /**
     * Deshabilita los componentes para que el usuario no pueda editarlos.
     */
    public void visualizacion();

    /**
     * Vuelve el Detalle a su estado inicial
     */
    public void limpiar();
}
