package nosigliaregistropersonal.modelo.helpers;

import java.sql.Time;
import nosigliaregistropersonal.modelo.Marca;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static nosigliaregistropersonal.modelo.helpers.MarcaHelper.*;
/**
 *
 * @author #emas
 */
public class MarcaHelperTest {

    Marca marca630 = new Marca();
    Marca marca650 = new Marca();
    Marca marca7 = new Marca();
    Marca marca745 = new Marca();
    Marca marca14 = new Marca();
    Marca marca15 = new Marca();
    Marca marca1353 = new Marca();
    Marca marca10 = new Marca();
    Marca marca12 = new Marca();
    
    @Before
    public void setup(){
        marca630.setHora(new Time(6,30,0));
        marca630.setHora(new Time(6, 30, 0));
        marca650.setHora(new Time(6,50,0));
        marca7.setHora(new Time(7,0,0));
        marca745.setHora(new Time(7,45,0));
        marca14.setHora(new Time(14, 0, 0));
        marca15.setHora(new Time(15,0,0));
        marca1353.setHora(new Time(13,53,00));
        marca10.setHora(new Time(10,0,0));
        marca12.setHora(new Time(12,0,0));
    }
    
    @Test
    public void testIsMarcaFueraDeHorario(){
        assertFalse(isMarcaFueraDeHorario(marca7));
        assertFalse(isMarcaFueraDeHorario(marca650));
        
        assertFalse(isMarcaFueraDeHorario(marca14));
        assertFalse(isMarcaFueraDeHorario(marca630));
        assertFalse(isMarcaFueraDeHorario(marca1353));
        assertTrue(isMarcaFueraDeHorario(marca12));

        assertTrue(isMarcaFueraDeHorario(marca745));
        assertTrue(isMarcaFueraDeHorario(marca15));
        assertTrue(isMarcaFueraDeHorario(marca10));
    }
}
