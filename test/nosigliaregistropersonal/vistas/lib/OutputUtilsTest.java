package nosigliaregistropersonal.vistas.lib;

import org.junit.Test;
import static org.junit.Assert.*;
import static nosigliaregistropersonal.vistas.lib.OutputUtils.formatHorasMinutos;
/**
 *
 * @author Emanuel
 */
public class OutputUtilsTest {
    
    public OutputUtilsTest() {
    }

    @Test
    public void testFormatHorasMinutos() {
        int sesenta = 60, ochenta = 80, diez = 10, cero = 0;
        assertEquals(formatHorasMinutos(ochenta), "01:20");
        assertEquals(formatHorasMinutos(sesenta), "01:00");
        assertEquals(formatHorasMinutos(diez), "00:10");
        assertEquals(formatHorasMinutos(cero), "00:00");
    }
}